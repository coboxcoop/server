const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeders/broadcasts')
const NotFound = require('../../../../../errors/not-found')
const { getResource } = require('../../../../../helpers/get-resources')
const { queryByType } = require('../../../../../helpers/query')

const ANNOUNCE = 'command/announce'
const HIDE = 'command/hide'

/* load jsdoc type definitions */
require('../../../../../types')

class BroadcastCommandsController {
  /**
   * CRUD controller for broadcast commands
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
    this.startBroadcast = this._api.get('commands.broadcast.start')
    this.stopBroadcast = this._api.get('commands.broadcast.stop')
  }

  /**
   * GET /api/admin/seeders/:id/commands/broadcasts
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<Command>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    return new Promise((resolve, reject) => {
      collect(seeder.log.read({ query: queryByType([ANNOUNCE, HIDE]) }), (err, commands) => {
        if (err) return reject(err)
        resolve(commands)
      })
    })
  }

  /**
   * POST /api/admin/seeders/:id/commands/announce
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {string} opts.id - the seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFoundError} throw when address does not match a seeder in memory
   * @returns {Command}
   */
  async create (params, opts) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))
    return this.startBroadcast(seeder, params, opts.session.id)
  }

  /**
   * POST /api/admin/seeders/:id/commands/hide
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {string} opts.id - the seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFoundError} throw when address does not match a seeder in memory
   * @returns {Command}
   */
  async destroy (params, opts) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))
    return this.stopBroadcast(seeder, params, opts.session.id)
  }

  /**
   * PATCH /api/admin/seeders/:id/commands/broadcasts
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {string} opts.id - the seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   */
  async update (params, opts) {
    let commands = await this.list(params, opts)
    let command = commands[commands.length - 1]
    if (command === ANNOUNCE) return this.create(params, opts)
    else if (command === HIDE) return this.destroy(params, opts)
    else throw new Error('invalid command')
  }
}

module.exports = BroadcastCommandsController
