const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeders/replicates')
const NotFound = require('../../../../../errors/not-found')
const { queryByType } = require('../../../../../helpers/query')
const { getResource } = require('../../../../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../../../../types')

class ReplicateCommandsController {
  /**
   * CRUD controller for replicate commands
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
    this.startReplicate = this._api.get('commands.replicate.start')
    this.stopReplicate = this._api.get('commands.replicate.stop')
  }

  /**
   * GET /api/admin/seeders/:id/commands/replicates
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<Command>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    return new Promise((resolve, reject) => {
      const query = queryByType(['command/replicate', 'command/unreplicate'])
      const stream = seeder.log.read({ query })
      collect(stream, (err, commands) => {
        if (err) return reject(err)
        resolve(commands)
      })
    })
  }

  /**
   * POST /api/admin/seeders/:id/commands/replicate
   *
   * @param {Object} params - a parameters object
   * @param {string} params.name - the name of the space to seed on the remote seeder
   * @param {string} params.address - the address of the space to seed on the remote seeder
   * @param {Object} opts - an options object
   * @param {string} opts.id - the remote seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Command}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))
    return this.startReplicate(seeder, params, opts.session.id)
  }

  /**
   * POST /api/admin/seeders/:id/commands/unreplicate
   *
   * @param {Object} params - a parameters object
   * @param {string} params.name - the name of the space to seed on the remote seeder
   * @param {string} params.address - the address of the space to seed on the remote seeder
   * @param {Object} opts - an options object
   * @param {string} opts.id - the remote seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Command}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))
    return this.stopReplicate(seeder, params, opts.session.id)
  }
}

module.exports = ReplicateCommandsController
