const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeder/connections')
const SpaceDecorator = require('../../../..//decorators/space')
const NotFound = require('../../../../errors/not-found')
const { getResource, getResources } = require('../../../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../../../types')

class SeederConnectionsController {
  /**
   * CRUD controller for seeder connections
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
  }

  /**
   * POST /api/admin/seeders/:id/connections
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params
   * @param {string} opts.id - address of the seeder
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Resource}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    seeder.swarm()

    if (!opts.decorate) return seeder
    const decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  /**
   * POST /api/admin/seeders/connections
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params, with options
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<Resource>}
   */
  async createAll (params, opts = {}) {
    const identity = opts.session.identity
    const seeders = await getResources(this.seeders, identity.publicKey)

    seeders.map((seeder) => seeder.swarm())

    if (!opts.decorate) return seeders
    return seeders
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * DELETE /api/admin/seeders/:id/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {string} params.id - address of the seeder
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Resource}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    seeder.unswarm()

    if (!opts.decorate) return seeder
    const decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  /**
   * DELETE /api/admin/seeders/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<Resource>}
   */
  async destroyAll (params, opts = {}) {
    const identity = opts.session.identity
    const seeders = await getResources(this.seeders, identity.publicKey)

    seeders.map((seeder) => seeder.unswarm())

    if (!opts.decorate) return seeders
    return seeders
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }
}

module.exports = SeederConnectionsController
