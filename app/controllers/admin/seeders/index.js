const assert = require('assert')
const NotFound = require('../../../errors/not-found')
const SpaceDecorator = require('../../../decorators/space')
const createParams = require('../../../helpers/create-params')
const { getResource, getResources } = require('../../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../../types')

class AdminSeedersController {
  /**
   * CRUD controller for seeders
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
    this.factory = this._api.get('factory.seeders')
    this.openInvite = this._api.get('services.invites.open')
  }

  /**
   * GET /api/admin/seeders
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Resource>}
   */
  async list (params, opts = {}) {
    const seeders = await getResources(this.seeders, opts.session.identity.publicKey)

    if (!opts.decorate) return seeders
    return seeders
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * POST /api/admin/seeders
   *
   * @param {Object} params - a parameters object
   * @param {string} params.name - the name of the seeder
   * @param {string} params.address - the address of the seeder
   * @param {string} params.code - an invite code to the seeder
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Resource}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity

    if (params.code) {
      params = this.openInvite(params.code, identity)
    }

    const seeder = await this.factory.createSeeder({
      ...createParams(params),
      identity
    })

    if (!opts.decorate) return seeder
    const decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  /**
   * DELETE /api/admin/seeders/:id
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Resource}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    await this.factory.deleteSeeder(seeder, identity)

    if (!opts.decorate) return seeder
    const decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }
}

module.exports = AdminSeedersController
