const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeder/invites')
const collect = require('collect-stream')
const NotFound = require('../../../../errors/not-found')
const SpaceDecorator = require('../../../../decorators/space')
const inviteParams = require('../../../../helpers/invite-params')
const { getResource } = require('../../../../helpers/get-resources')
const { queryByType } = require('../../../../helpers/query')
const { hex } = require('../../../../../util')

/* load jsdoc type definitions */
require('../../../../types')

class SeederInvitesController {
  /**
   * CRUD controller for seeder invites
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
    this.createInvite = this._api.get('services.invites.create')
    this.factory = this._api.get('factory.seeders')
    this.openInvite = this._api.get('services.invites.open')
  }

  /**
   * GET /api/admin/seeders/invites/accept
   *
   * @param {Object} params - parsed request query string and params
   * @param {string} params.code - an invite code
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   */
  async accept (params, opts = {}) {
    params = this.openInvite(params.code, opts.session.identity)

    const seeder = await this.factory.createSeeder({
      ...params,
      identity: opts.session.identity
    })

    await seeder.swarm()

    if (!opts.decorate) return seeder
    let decorator = SpaceDecorator(seeder)
    return decorator.toJSON()
  }

  /**
   * GET /api/admin/seeders/:id/invites
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<Invite>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    return new Promise((resolve, reject) => {
      collect(seeder.log.read({ query: queryByType('peer/invite') }), (err, invites) => {
        if (err) return reject(err)
        resolve(invites.map((invite) => ({
          address: hex(seeder.address),
          data: invite.value
        })))
      })
    })
  }

  /**
   * POST /api/admin/seeders/:id/invites
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {string} opts.id - the seeder address
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Invite}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, opts.id)
    assert(seeder, new NotFound('seeder does not exist'))
    return this.createInvite(seeder, inviteParams(seeder, params), opts.session.identity)
  }
}

module.exports = SeederInvitesController
