const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:admin/seeder/peers')
const NotFound = require('../../../../errors/not-found')
const { queryByType } = require('../../../../helpers/query')
const { getResource } = require('../../../../helpers/get-resources')
const { hex } = require('../../../../../util')

/* load jsdoc type definitions */
require('../../../../types')

class SeederPeersController {
  /**
   * CRUD controller for seeder peers
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.seeders = this._api.get('cache.seeders')
  }

  /**
   * GET /api/admin/seeders/:id/peers
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} throw when address does not match a seeder in memory
   * @returns {Array.<PeerAbout>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const seeder = await getResource(this.seeders, identity.publicKey, params.id)
    assert(seeder, new NotFound('seeder does not exist'))

    return new Promise((resolve, reject) => {
      collect(seeder.log.read({ query: queryByType('peer/about') }), (err, peers) => {
        if (err) return reject(err)
        resolve(peers.reduce((acc, peer) => {
          acc[peer.value.author] = peer.value
          return acc
        }, {}))
      })
    })
  }
}

module.exports = SeederPeersController
