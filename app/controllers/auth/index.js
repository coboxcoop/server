const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:authentication')
const Unauthorized = require('../../errors/unauthorized')
const NotFound = require('../../errors/not-found')

/* load jsdoc type definitions */
require('../../types')

class AuthenticationController {
  /**
   * CRUD controller for authentication
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.lock = this._api.get('app.lock')
    this.unlock = this._api.get('app.unlock')
    this.authenticate = this._api.get('services.authenticate')
  }

  /**
   * GET /api/auth
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @throws {NotFound} - when no session is present
   */
  async get (params = {}, opts = {}) {
    const session = opts.session
    assert(session.identity, new NotFound('no session found, please login'))
    return true
  }

  /**
   * POST /api/auth
   *
   * @param {Object} params - a parameters object
   * @param {string} params.password - password for decrypting parent key
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @throws {Forbidden} - when password is incorrect
   */
  async create (params = {}, opts = {}) {
    await this.authenticate(params.password)
    const identity = await this.unlock(params.password)
    opts.session.identity = identity
  }

  /**
   * DELETE /api/auth
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {Error} - unable to close corestore or mount location
   */
  async destroy (params = {}, opts = {}) {
    try {
      assert(opts.session.identity, new Unauthorized('no active session detected, please login'))
    } catch (err) {
      const app = this._api.get('app.express')
      if (!app.get('lock')) await this.lock()
      throw err
    }
    await this.lock()
    opts.session.identity = null
  }
}

module.exports = AuthenticationController
