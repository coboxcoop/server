const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/drive')
const NotFound = require('../../errors/not-found')
const open = require('open')
const path = require('path')
const { queryByFilename } = require('../../helpers/query')
const { getResource } = require('../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../types')

class DriveController {
  /**
   * CRUD controller for drive operations
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.settings = this._api.get('app.settings')
    this.spaces = this._api.get('cache.spaces')
  }

  /**
   * GET /api/spaces/:id/drive
   *
   * @param {Object} params - parsed request body
   * @param {Object} params.id - address of the space
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - address does not match a space in memory
   * @throws {AssertionError} - space does not have an active mount directory
   */
  async open (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('does not exist'))
    assert(space.drive.isMounted(), 'space does not have an active mount directory')
    return open(path.join(this.settings.mount, space.name))
  }

  /**
   * GET /api/spaces/:id/drive/history
   *
   * @param {Object} params - parsed request body
   * @param {Object} params.id - address of the space
   * @param {Object} params.filename - address of the space
   * @param {Object} params.from - the timestamp to query from
   * @param {Object} params.to - to timestamp to query to
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - address does not match a space in memory
   * @returns {Array.<DriveState>} - all changes to the space drive
   */
  async history (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('does not exist'))

    return new Promise((resolve, reject) => {
      const query = queryByFilename(params.filename, params.from, params.to)
      const stream = space.state.read({ query })
      collect(stream, (err, state) => {
        if (err) return reject(err)
        resolve(state)
      })
    })
  }

  /**
   * GET /api/spaces/:id/drive/readdir
   *
   * @param {Object} params - parsed request body
   * @param {Object} params.id - address of the space
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - address does not match a space in memory
   * @returns {Array.<String>} - a list of file names and paths w.r.t. dir parameter
   */
  async readdir (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('does not exist'))
    return space.drive.ls(params.dir)
  }

  /*
   * GET /api/spaces/:id/drive/stat
   *
   * @typedef {Object} Size
   * @property {number} size - the number of bytes used
   *
   * @param {Object} params - parsed request body
   * @param {Object} params.id - address of the space
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - address does not match a space in memory
   * @returns {Size}
   */
  async stat (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('does not exist'))
    const size = await space.drive.size()
    return { size }
  }
}

module.exports = DriveController
