const debug = require('@coboxcoop/logger')('@coboxcoop/server:controllers/events')

/* load jsdoc type definitions */
require('../../types')

class EventsController {
  /**
   * main controller for start/stop/events functionality
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {}

  /**
   * WS /api
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {Object} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @returns {Duplex}
   */
  live (params = {}, opts = {}) {
    return this._api.get('app.events')
  }
}

module.exports = EventsController
