const assert = require('assert')
const debug = require('debug')('@coboxcoop/server:export')

/* load jsdoc type definitions */
require('../../types')

class ExportController {
  /**
   * CRUD controller for export
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.authenticate = this._api.get('services.authenticate')
    this.pack = this._api.get('services.backup.pack')
  }

  /**
   * GET /api/export
   *
   * @param {Object} params - parameters object
   * @param {string} params.password - password confirmation
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   */
  async index (params, opts = {}) {
    assert(params.password, 'password required')
    await this.authenticate(params.password)
    const stream = await this.pack(params)
    return stream
  }
}

module.exports = ExportController
