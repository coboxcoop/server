const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:identities')
const IdentityDecorator = require('../../decorators/identity')
const NotFound = require('../../errors/not-found')
const Unauthorized = require('../../errors/unauthorized')
const { shortHex } = require('../../../util')
const { assign } = Object

/* load jsdoc type definitions */
require('../../types')

class IdentitiesController {
  /**
   * CRUD controller for identities
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.identitydb = this._api.get('db.identities')
    this.identities = this._api.get('cache.identities')
    this.factory = this._api.get('factory.identities')
  }

  /**
   * GET /api/identities
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   */
  list (params, opts = {}) {
    const identities = []

    for (const identity of this.identities.values()) {
      identities.push(identity)
    }

    return identities
      .map(IdentityDecorator)
      .map((d) => d.toJSON())
  }

  /**
   * GET /api/identities/current
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @returns {Identity}
   */
  async current (params, opts = {}) {
    const identity = opts.session.identity
    if (!opts.decorate) return identity
    return IdentityDecorator(identity).toJSON()
  }

  /**
   * GET /api/identities/:id/change
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - no known identity matches the public key provided
   * @returns {Identity}
   */
  async change (params, opts = {}) {
    assert(false, new Unauthorized('feature not yet supported'))
    // TODO: enable informing other elements in the back-end
    // e.g. we need to close and open a new networker, since
    // we're now using a different noise keypair
    const prevIdentity = opts.session.identity
    const nextIdentity = this.identities.get(shortHex(params.id))
    assert(nextIdentity, new NotFound('identity doesnt exist'))
    // TODO: unmount all spaces and stop swarming on these spaces?
    // overwrite session identity object
    opts.session.identity = nextIdentity
    if (!opts.decorate) return nextIdentity
    return IdentityDecorator(nextIdentity).toJSON()
  }

  /**
   * POST /api/identities/:id
   *
   * @param {Object} params - parameters object
   * @param {string} params.name - the nickname for your new identity
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @returns {Identity}
   */
  async create (params, opts = {}) {
    const identity = await this.factory.createIdentity(params)
    if (!opts.decorate) return identity
    const decorator = IdentityDecorator(identity)
    return decorator.toJSON()
  }

  /**
   * PATCH /api/identities/:id
   *
   * @param {Object} params - parameters object
   * @param {string} params.name - the nickname for your new identity
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - no known identity matches the public key provided
   * @returns {Identity}
   */
  async update (params, opts = {}) {
    let identity = this.identities.get(shortHex(opts.id))
    assert(identity, new NotFound('identity doesnt exist'))
    opts.session.identity = await this.factory.updateIdentity(assign(params, { identity }))

    if (!opts.decorate) return identity
    return IdentityDecorator(identity).toJSON()
  }

  /**
   * DELETE /api/identities/:id
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   * @throws {NotFound} - no known identity matches the public key provided
   * @throws {AssertionError} - unable to delete current identity
   * @returns {Identity}
   */
  async destroy (params, opts = {}) {
    let identity = this.identities.get(shortHex(params.id))
    assert(identity, new NotFound('identity doesnt exist'))
    const currentIdentity = opts.session.identity
    assert.same(identity.publicKey, currentIdentity.publicKey, 'unable to delete current identity, switch before continuing')
    identity = await this.factory.deleteIdentity(params)

    if (!opts.decorate) return identity
    return IdentityDecorator(identity).toJSON()
  }
}

module.exports = IdentitiesController
