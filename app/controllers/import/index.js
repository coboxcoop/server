const assert = require('assert')
const debug = require('debug')('@coboxcoop/server:import')
const Forbidden = require('../../errors/forbidden')

/* load jsdoc type definitions */
require('../../types')

class ImportController {
  /**
   * CRUD controller for imports
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.unpack = this._api.get('services.backup.unpack')
  }

  /**
   * GET /api/import
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity  - the current identity
   */
  async create (params, opts = {}) {
    assert(!opts.session.identity, new Forbidden('you must logout to import your old cobox backup file'))
    await this.unpack(params.file, params.password)
    const identity = await this.unlock(params.password)
    opts.session.identity = identity
  }
}

module.exports = ImportController
