const assert = require('assert')
const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/mounts')
const NotFound = require('../../errors/not-found')
const SpaceDecorator = require('../../decorators/space')
const { getResource, getResources } = require('../../helpers/get-resources')
const { assign } = Object

/* load jsdoc type definitions */
require('../../types')

class MountsController {
  /**
   * CRUD controller for mounts
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.spaces = this._api.get('cache.spaces')
  }

  /**
   * GET /api/spaces/mounts
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params
   * @param {string} opts.id - address of the replicator
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Space>}
   */
  async list (params, opts = {}) {
    const spaces = await getResources(this.spaces, opts.session.identity.publicKey)
    const mounted = spaces
      .filter((space) => space.drive.location)

    if (!opts.decorate) return mounted
    return mounted
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
      .reduce((acc, space) => {
        acc[space.address] = space.location
        return acc
      }, {})
  }

  /**
   * POST /api/spaces/:id/mounts
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params
   * @param {string} opts.id - address of the replicator
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, opts.id)
    assert(space, new NotFound('space does not exist'))
    const location = path.join(params.location, space.name)
    await space.drive.mount(assign(params, { location }))

    if (!opts.decorate) return space
    const decorator = SpaceDecorator(space)
    const data = assign(decorator.toJSON(), { location })
    return data
  }

  /**
   * POST /api/spaces/mounts
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params, with options
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Space>}
   */
  async createAll (params, opts = {}) {
    const identity = opts.session.identity
    const spaces = await getResources(this.spaces, identity.publicKey)

    spaces
      .filter((space) => !space.drive.isMounted())
      .map((space) => space.drive.mount({
        location: path.join(params.location, space.name)
      }))

    if (!opts.decorate) return spaces
    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * DELETE /api/spaces/:id/mounts
   *
   * @param {Object} params - parsed request query string and params
   * @param {string} params.id - address of the space
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async destroy (params = {}, opts) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))
    const location = await space.drive.unmount()

    if (!opts.decorate) return space
    const decorator = SpaceDecorator(space)
    return assign(decorator.toJSON(), { location })
  }

  /**
   * DELETE /api/spaces/mounts
   *
   * @param {Object} params - parsed request query string and params
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Space>}
   */
  async destroyAll (params, opts) {
    const identity = opts.session.identity
    const spaces = await getResources(this.spaces, identity.publicKey)

    spaces
      .filter((space) => space.drive.isMounted())
      .map((space) => space.drive.unmount())

    if (!opts.decorate) return spaces
    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }
}

module.exports = MountsController
