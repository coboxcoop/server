const debug = require('@coboxcoop/logger')('@coboxcoop/server/app/controllers/peers')

/* load jsdoc type definitions */
require('../../types')

class PeersController {
  /**
   * system controller for making system info available
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.peerdb = this._api.get('db.peers')
  }

  /**
   * GET /api/peers
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @returns {Object.<PeerConnection>}
   */
  async list () {
    return new Promise((resolve, reject) => {
      const stream = this.peerdb.createReadStream({ keys: false, values: true })
      // peers is an object of peer objects
      const peers = {}
      stream.on('data', (peer) => {
        peers[peer.peerId] = peer
      })

      stream.on('end', (err) => {
        if (err) return reject(err)
        return resolve(peers)
      })
    })
  }
}

module.exports = PeersController
