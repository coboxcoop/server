const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:registration')
const crypto = require('@coboxcoop/crypto')
const Unauthorized = require('../../errors/unauthorized')

/* load jsdoc type definitions */
require('../../types')

class RegistrationController {
  /**
   * registration controller for setting up on-disk encryption
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.unlock = this._api.get('app.unlock')
    this.passwordHashExists = this._api.get('services.authentication.password-exists')
    this.savePasswordHash = this._api.get('services.authentication.save-password')
    this.saveEncryptedParentKey = this._api.get('services.encryption.save-parent-key')
  }

  /**
   * POST /api/register
   *
   * @param {Object} params - a parameters object
   * @param {string} params.password - password protection
   * @param {Object} opts - an options object
   * @param {Object} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @throws {AssertionError}
   */
  async create (params = {}, opts = {}) {
    const exists = await this.passwordHashExists()
    assert(!exists, new Unauthorized('account exists, please login'))
    debug({ msg: '[REGISTRATION] generating a new account' })
    await this.savePasswordHash(params.password)
    await this.saveEncryptedParentKey(crypto.masterKey(), params.password)
    debug({ msg: '[REGISTRATION] account successfully created' })
    const identity = await this.unlock(params.password)
    opts.session.identity = identity
  }
}

module.exports = RegistrationController
