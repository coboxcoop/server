const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:replicator/connections')
const NotFound = require('../../../errors/not-found')
const SpaceDecorator = require('../../../decorators/space')
const { getResource, getResources } = require('../../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../../types')

class ReplicatorConnectionsController {
  /**
   * CRUD controller for replicator connections
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.replicators = this._api.get('cache.replicators')
  }

  /**
   * POST /api/replicators/:id/connections
   *
   * @param {Object} params - parsed request params
   * @param {string} params.id - address of the replicator
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - replicator does not exist
   * @returns {Resource}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const replicator = await getResource(this.replicators, identity.publicKey, params.id)
    assert(replicator, new NotFound('replicator does not exist'))

    replicator.swarm()

    if (!opts.decorate) return replicator
    const decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  /**
   * POST /api/replicators/connections
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params, with options
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Resource>}
   */
  async createAll (params, opts = {}) {
    const identity = opts.session.identity
    const replicators = await getResources(this.replicators, identity.publicKey)

    replicators.map((replicator) => replicator.swarm())

    if (!opts.decorate) return replicators
    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * DELETE /api/replicators/:id/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {string} params.id - address of the replicator
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - replicator does not exist
   * @returns {Resource}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const replicator = await getResource(this.replicators, identity.publicKey, params.id)
    assert(replicator, new NotFound('replicator does not exist'))

    replicator.unswarm()

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  /**
   * DELETE /api/replicators/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Resource>}
   */
  async destroyAll (params, opts = {}) {
    const identity = opts.session.identity
    const replicators = await getResources(this.replicators, identity.publicKey)

    replicators.map((replicator) => replicator.unswarm())

    if (!opts.decorate) return replicators
    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }
}

module.exports = ReplicatorConnectionsController
