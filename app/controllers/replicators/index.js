const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:replicators')
const NotFound = require('../../errors/not-found')
const resourceParams = require('../../helpers/resource-params')
const SpaceDecorator = require('../../decorators/space')
const { getResource, getResources } = require('../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../types')

class ReplicatorsController {
  /**
   * CRUD controller for replicators
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.replicators = this._api.get('cache.replicators')
    this.factory = this._api.get('factory.replicators')
  }

  /**
   * GET /api/replicators
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Resource>}
   */
  async list (params, opts = {}) {
    const replicators = await getResources(this.replicators, opts.session.identity.publicKey)

    if (!opts.decorate) return replicators
    return replicators
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * POST /api/replicators
   *
   * @param {Object} params - a parameters object
   * @param {string} params.name - the name of the seeder
   * @param {string} params.address - the address of the seeder
   * @param {string} params.code - an invite code to the seeder
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {AssertionError}
   * @returns {Resource}
   */
  async create (params, opts = {}) {
    const replicator = await this.factory.createReplicator({
      ...resourceParams(params),
      identity: opts.session.identity
    })

    assert(replicator, 'failed to create')

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }

  /**
   * DELETE /api/replicators/:id
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - replicator does not exist
   * @returns {Resource}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const replicator = await getResource(this.replicators, identity.publicKey, params.id)
    assert(replicator, new NotFound('replicator does not exist'))

    await this.factory.deleteReplicator(replicator, identity)

    if (!opts.decorate) return replicator
    let decorator = SpaceDecorator(replicator)
    return decorator.toJSON()
  }
}

module.exports = ReplicatorsController
