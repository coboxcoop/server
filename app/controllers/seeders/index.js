const debug = require('@coboxcoop/logger')('@coboxcoop/server:seeders')
const { Transform } = require('stream')
const collect = require('collect-stream')
const { pick } = require('../../../util')
const { assign } = Object

/* load jsdoc type definitions */
require('../../types')

const SEEDER_CONNECTED = 'SEEDER_CONNECTED'
const SEEDER_DISCONNECTED = 'SEEDER_DISCONNECTED'

// TODO: UPDATE DISABLED CONTROLLER

class LocalSeedersController {
  /**
   * CRUD controller for local seeders
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.localSeeders = this._api.get('db.local.seeders')
  }

  /**
   * WS /api/seeders/.websocket
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   */
  live (params, opts = {}) {
    return this.localSeeders
      .createLiveStream()
      .pipe(format())
  }

  /**
   * GET /api/seeders
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   */
  async index (params, opts = {}) {
    return new Promise((resolve, reject) => {
      const seeders = this.localSeeders
        .createReadStream()
        .pipe(format())

      collect(seeders, (err, msgs) => {
        if (err) return reject(err)
        return resolve(msgs)
      })
    })
  }

  /**
   * GET /api/seeders/:id
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the seeder address
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   */
  async show (params, opts = {}) {
    const seeder = await this._findSeeder(params)
    if (!opts.decorate) return seeder
    return seeder.content
  }

  // --------------------- Helpers ---------------------- //

  _findSeeder (params) {
    return new Promise((resolve, reject) => {
      this.localSeeders.get(params.id, (err, msg) => {
        if (err) return reject(err)
        if (err && err.notFound) return reject(new Error('seeder not seen on this network'))
        else resolve(msg)
      })
    })
  }
}

function format () {
  return new Transform(assign({ objectMode: true }, {
    transform (msg, _, next) {
      if (msg.sync) return next()

      const isBroadcasting = msg.value.content.broadcasting
      const type = isBroadcasting
        ? SEEDER_CONNECTED
        : SEEDER_DISCONNECTED

      this.push({
        type: type,
        data: {
          address: msg.value.content.address,
          ...pick(msg.value, ['author', 'timestamp'])
        }
      })

      next()
    }
  }))
}

module.exports = LocalSeedersController
