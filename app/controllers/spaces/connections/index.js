const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/connections')
const NotFound = require('../../../errors/not-found')
const SpaceDecorator = require('../../../decorators/space')
const { getResource, getResources } = require('../../../helpers/get-resources')

/* load jsdoc type definitions */
require('../../../types')

class SpaceConnectionsController {
  /**
   * CRUD controller for drive operations
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.spaces = this._api.get('cache.spaces')
  }

  /**
   * POST /api/spaces/:id/connections
   *
   * @param {Object} params - parsed request params
   * @param {string} params.id - address of the seeder
   * @param {Object} opts - parsed request query string and params
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))

    space.swarm()

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  /**
   * POST /api/spaces/connections
   *
   * @param {Object} params - parsed request body
   * @param {Object} opts - parsed request query string and params, with options
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @returns {Array.<Space>}
   */
  async createAll (params, opts = {}) {
    const identity = opts.session.identity
    const spaces = await getResources(this.spaces, identity.publicKey)

    spaces.map((space) => space.swarm())

    if (!opts.decorate) return spaces
    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * DELETE /api/spaces/:id/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {string} params.id - address of the seeder
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))

    space.unswarm()

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  /**
   * DELETE /api/spaces/connections
   *
   * @param {Object} params - parsed request query string and params
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @returns {Array.<Space>}
   */
  async destroyAll (params, opts = {}) {
    const identity = opts.session.identity
    const spaces = await getResources(this.spaces, identity.publicKey)

    spaces.map((space) => space.unswarm())

    if (!opts.decorate) return spaces
    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }
}

module.exports = SpaceConnectionsController
