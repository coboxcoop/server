const debug = require('@coboxcoop/logger')('@coboxcoop/server:controllers/spaces')
const assert = require('assert')
const createParams = require('../../helpers/create-params')
const NotFound = require('../../errors/not-found')
const SpaceDecorator = require('../../decorators/space')
const { getResource, getResources } = require('../../helpers/get-resources')
const { shortHex } = require('../../../util')

/* load jsdoc type definitions */
require('../../types')

class SpacesController {
  /**
   * CRUD controller for spaces
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.spaces = this._api.get('cache.spaces')
    this.factory = this._api.get('factory.spaces')
    this.spacedb = this._api.get('db.spaces')
    this.openInvite = this._api.get('services.invites.open')
  }

  /**
   * GET /api/spaces
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<Space>}
   */
  async list (params, opts = {}) {
    const spaces = await getResources(this.spaces, opts.session.identity.publicKey)

    if (!opts.decorate) return spaces
    return spaces
      .map(SpaceDecorator)
      .map((decorator) => decorator.toJSON())
  }

  /**
   * POST /api/spaces
   *
   * @param {Object} params - a parameters object
   * @param {string} params.name - the name of the space
   * @param {string} params.address - the address of the space
   * @param {string} params.code - an invite code to the space
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {AssertionError} - failed to create space
   * @returns {Space}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity

    if (params.code) {
      params = this.openInvite(params.code, identity)
    }

    const space = await this.factory.createSpace({
      ...createParams(params),
      identity
    })

    assert(space, 'failed to create')

    if (!opts.decorate) return space
    const decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  /**
   * PATCH  /api/spaces/:id
   *
   * @param {Object} params - a parameters object
   * @param {string} params.threshold - the threshold setting
   * @param {string} params.tolerance - the tolerance setting
   * @param {Object} opts - an options object
   * @param {string} opts.id - the id of the space
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async update (params, opts = {}) {
    // TODO: parameter validation
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, opts.id)
    assert(space, new NotFound('space does not exist'))
    const key = `${shortHex(identity.publicKey)}/${shortHex(space.address)}`
    const spaceDetails = await this.spacedb.get(key)
    spaceDetails.settings = params
    await this.spacedb.put(key, spaceDetails)
    return params
  }

  /**
   * DELETE /api/spaces/:id
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the space address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {Object} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async destroy (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))

    await this.factory.deleteSpace(space, identity)

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }
}

module.exports = SpacesController
