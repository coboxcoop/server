const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:space/invites')
const NotFound = require('../../../errors/not-found')
const inviteParams = require('../../../helpers/invite-params')
const SpaceDecorator = require('../../../decorators/space')
const { queryByType } = require('../../../helpers/query')
const { getResource } = require('../../../helpers/get-resources')
const { hex } = require('../../../../util')

/* load jsdoc type definitions */
require('../../../types')

class SpaceInvitesController {
  /**
   * CRUD controller for mounts
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.spaces = this._api.get('cache.spaces')
    this.factory = this._api.get('factory.spaces')
    this.createInvite = this._api.get('services.invites.create')
    this.openInvite = this._api.get('services.invites.open')
  }

  /**
   * GET /api/spaces/invites/accept?code=XXXXXXXXXXXXXXXX
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the space address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Space}
   */
  async accept (params, opts = {}) {
    params = this.openInvite(params.code, opts.session.identity)

    const space = await this.factory.createSpace({
      ...params,
      identity: opts.session.identity
    })

    if (!opts.decorate) return space
    let decorator = SpaceDecorator(space)
    return decorator.toJSON()
  }

  /**
   * GET /api/spaces/:id/invites
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the space address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @returns {Array.<PeerInvite>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))

    return new Promise((resolve, reject) => {
      const stream = space.log.read({ query: queryByType('peer/invite') })
      collect(stream, (err, invites) => {
        if (err) return reject(err)
        resolve(invites.map((invite) => ({
          address: hex(space.address),
          data: invite.value
        })))
      })
    })
  }

  /**
   * POST /api/spaces/:id/invites
   *
   * @param {Object} params - a parameters object
   * @param {Object} opts - an options object
   * @param {string} opts.id - the space address
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Space}
   */
  async create (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, opts.id)
    assert(space, new NotFound('space does not exist'))
    return this.createInvite(space, inviteParams(space, params), opts.session.identity)
  }
}

module.exports = SpaceInvitesController
