const assert = require('assert')
const collect = require('collect-stream')
const debug = require('@coboxcoop/logger')('@coboxcoop/server/app/controllers/spaces/peers')
const NotFound = require('../../../errors/not-found')
const { queryByType } = require('../../../helpers/query')
const { getResource } = require('../../../helpers/get-resources')
const { hex } = require('../../../../util')

/* load jsdoc type definitions */
require('../../../types')

class SpacePeersController {
  /**
   * CRUD controller for space peers
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.spaces = this._api.get('cache.spaces')
  }

  /**
   * GET /api/spaces/:id/peers
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the space address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Object.<PeerAbout>}
   */
  async list (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, new NotFound('space does not exist'))

    return new Promise((resolve, reject) => {
      const stream = space.log.read({ query: queryByType('peer/about') })
      collect(stream, (err, peers) => {
        if (err) return reject(err)
        resolve(peers.reduce((acc, peer) => {
          acc[peer.value.author] = peer.value
          return acc
        }, {}))
      })
    })
  }

  /**
   * GET /api/spaces/:id/last-sync
   *
   * @param {Object} params - a parameters object
   * @param {string} params.id - the space address
   * @param {Object} opts - an options object
   * @param {boolean} opts.decorate - boolean flag to decorate the returned object
   * @param {Object} opts.session - the current session details
   * @param {string} opts.session.id - the session id
   * @param {Object} opts.session.identity - the current identity
   * @throws {NotFound} - space does not exist
   * @returns {Object.<SpacePeerLastSync>}
   */
  async lastSync (params, opts = {}) {
    const identity = opts.session.identity
    const space = await getResource(this.spaces, identity.publicKey, params.id)
    assert(space, 'does not exist')

    return new Promise((resolve, reject) => {
      const stream = space.lastSyncDb.createReadStream()
      const seeders = {}

      stream.on('data', ({ key: peerId, value: lastSyncAt }) => {
        seeders[peerId] = {
          address: hex(space.address),
          peerId,
          lastSyncAt
        }
      })

      stream.on('end', (err) => {
        if (err) return reject(err)
        return resolve(seeders)
      })
    })
  }
}

module.exports = SpacePeersController
