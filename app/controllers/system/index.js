const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server/app/controllers/system')
const path = require('path')
const fs = require('fs')

/* load jsdoc type definitions */
require('../../types')

class SystemController {
  /**
   * system controller for making system info available
   *
   * @param {Map} api - global dependency api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * ready function for loading dependencies once present
   */
  async ready () {
    this.details = this._api.get('app.details')
    this.settings = this._api.get('app.settings')
    this.fetchRoutes = this._api.get('setup.fetch-routes')
  }

  /**
   * GET /api/stop
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @returns {Object}
   */
  async stop (params = {}, opts = {}) {
    const stop = this._api.get('app.stop')
    process.nextTick(stop)
    return { success: true }
  }

  /**
   * GET /api/system
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @returns {Object}
   */
  async show (params, opts = {}) {
    return this.details
  }

  /**
   * GET /api/system/routes
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @returns {Array.<Route>}
   */
  routes (params, opts) {
    return this.fetchRoutes()
  }

  /**
   * GET /api/system/logs
   *
   * @param {Object} params - parameters object
   * @param {Object} opts - options object
   * @param {boolean} opts.decorate - flag to decorate
   * @throws {AssertionError}
   * @returns {Readable}
   */
  async logs (params, opts = {}) {
    const logsPath = path.join(this.settings.logs, 'main.log')
    await fs.promises.stat(logsPath)
    return fs.createReadStream(logsPath)
  }
}

module.exports = SystemController
