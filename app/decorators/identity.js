const { hex } = require('../../util')

/* load jsdoc type definitions */
require('../types')

module.exports = (identity) => new IdentityDecorator(identity)

class IdentityDecorator {
  /**
   * decorate an identity for sending back in a response
   * @param {Identity} identity - an identity object
   */
  constructor (identity) {
    this._identity = identity
  }

  /**
   * strip an identity of its protected fields to send over the API
   * @returns {Identity}
   */
  toJSON () {
    return {
      name: this._identity.name,
      publicKey: this._identity.publicKey
    }
  }
}
