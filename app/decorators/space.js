const { trim, hex } = require('../../util')

/* load jsdoc type definitions */
require('../types')

module.exports = (space) => new SpaceDecorator(space)

class SpaceDecorator {
  /**
   * decorate a space for sending back in a response
   * @param {Space|Superusers|Replicator} space - a space object
   */
  constructor (space) {
    this._space = space
  }

  /**
   * strip a space of its protected fields to send over the API
   * @returns {Resource}
   */
  toJSON (opts = {}) {
    return trim({
      name: this._space.name,
      address: hex(this._space.address),
      discoveryKey: hex(this._space.discoveryKey),
      location: this._space.drive && this._space.drive.location,
      size: this._space.bytesUsed && this._space.bytesUsed(),
      swarming: this._space.isSwarming && this._space.isSwarming(),
      ...this._space.settings
    })
  }
}
