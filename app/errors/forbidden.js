const { AssertionError } = require('assert')

/* load jsdoc type definitions */
require('../types')

class Forbidden extends AssertionError {
  /**
   * @param {String} message - the error message suffix
   */
  constructor (message) {
    super({ message: `forbidden: ${message}` })
    this.forbidden = true
    this.status = 403
  }
}

module.exports = Forbidden
