const { AssertionError } = require('assert')

/* load jsdoc type definitions */
require('../types')

class NotFound extends AssertionError {
  /**
   * @param {String} message - the error message suffix
   */
  constructor (message) {
    super({ message: `not found: ${message}` })
    this.notFound = true
    this.status = 404
  }
}

module.exports = NotFound
