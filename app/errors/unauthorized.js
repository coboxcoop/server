const { AssertionError } = require('assert')

/* load jsdoc type definitions */
require('../types')

class Unauthorized extends AssertionError {
  /**
   * @param {String} message - the error message suffix
   */
  constructor (message) {
    super({ message: `unauthorized: ${message}` })
    this.unauthorized = true
    this.status = 401
  }
}

module.exports = Unauthorized
