const constants = require('@coboxcoop/constants')
const crypto = require('@coboxcoop/crypto')
const sodium = require('sodium-native')
const { hex, shortHex } = require('../../util')
const { getResources } = require('../helpers/get-resources')

/* load jsdoc type definitions */
require('./../types')

class IdentityFactory {
  /**
   * a factory for creating identities, generates a keypair using
   * the key derivation function and saves unless otherwise specified,
   * augments the identity in memory with a list of spaces for cross-referencing
   * @param {Map} api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * fetches dependencies once unlock has been called
   */
  ready () {
    this.db = this._api.get('db.identities')
    this.cache = this._api.get('cache.identities')
    this.spaces = this._api.get('cache.spaces')
    this.replicators = this._api.get('cache.replicators')
    this.seeders = this._api.get('cache.seeders')
    this.spaceFactory = this._api.get('factory.spaces')
    this.replicatorFactory = this._api.get('factory.replicators')
    this.seederFactory = this._api.get('factory.seeders')
    this.deriveKeyPair = this._api.get('setup.encryption-key-pair')
  }

  /**
   * create a new identity
   * @param {object} params - a parameters object
   * @param {string} params.name - the name of the replicator
   * @param {string} params.seed - the address of the replicator
   * @returns {Identity} an identity
   */
  async createIdentity (params = {}, opts = { save: true }) {
    // generate a random seed to derive keys from
    const seed = params.seed
      ? Buffer.from(params.seed, 'hex')
      : crypto.randomBytes(sodium.crypto_kdf_CONTEXTBYTES)
    // deterministcally generate a keypair using the seed
    const keyPair = this.deriveKeyPair(constants.keyIds.identity, seed)
    // save the seed and the name
    if (opts.save) {
      await this.db.put(hex(keyPair.publicKey), {
        name: params.name,
        seed: hex(seed)
      })
    }
    // bundle together
    const identity = {
      publicKey: hex(keyPair.publicKey),
      secretKey: hex(keyPair.secretKey),
      name: params.name
    }
    // cache with list of addresses
    this.cache.set(shortHex(identity.publicKey), identity)
    return identity
  }

  /**
   * update an identity
   *
   * @param {object} params - a parameters object
   * @param {string} params.name - the name to change to
   * @param {Identity} params.identity - the identity to update
   * @returns {Identity} an updated identity
   */
  async updateIdentity (params) {
    const { name, identity } = params
    // overwrite the existing record in level, not a great solution
    const saved = await this.db.get(hex(identity.publicKey))
    saved.name = name
    await this.db.put(hex(identity.publicKey), saved)
    // update the cache
    const cached = this.cache.get(shortHex(identity.publicKey))
    cached.name = name
    this.cache.set(shortHex(identity.publicKey), cached)
    // update the session identity object
    identity.name = name
    return identity
  }

  /**
   * delete an identity
   * @param {object} params - a parameters object
   * @param {Identity} params.identity - the identity to delete
   * @returns {Identity} a deleted identity
   */
  async deleteIdentity (params) {
    const { identity } = params
    const spaces = await getResources(this.spaces, identity.publicKey)
    const replicators = await getResources(this.replicators, identity.publicKey)
    const seeders = await getResources(this.seeders, identity.publicKey)
    await Promise.all([
      ...spaces.map((space) => this.spaceFactory.deleteReplicator(space, identity)),
      ...replicators.map((replicator) => this.replicatorFactory.deleteReplicator(replicator, identity)),
      ...seeders.map((seeder) => this.seederFactory.deleteReplicator(seeder, identity))
    ])
    await this.db.del(hex(identity.publicKey))
    this.cache.delete(shortHex(identity.publicKey))
    return identity
  }
}

module.exports = IdentityFactory
