const assert = require('assert')
const Replicator = require('@coboxcoop/replicator')
const path = require('path')
const { hex, shortHex } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

class ReplicatorFactory {
  /**
   * a factory for creating replicators, passes the relevant parameters
   * down to the Replicator constructor, ensuring the storage location on
   * disk for hypercores is set, as well as corestore being present
   * namereplicators storage under the relevant identity, and stores an
   * index reference under that identity
   * @param {Map} api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * fetches dependencies once unlock has been called
   */
  ready () {
    const settings = this._api.get('app.settings')
    this.rootStorage = settings.storage
    this.db = this._api.get('db.replicators')
    this.cache = this._api.get('cache.replicators')
    this.corestore = this._api.get('dat.store')
  }

  /**
   * create a new replicator
   * @param {object} params - a parameters object
   * @param {string} params.name - the name of the replicator
   * @param {string} params.address - the address of the replicator
   * @param {string} params.encryptionKey - the encryption key for the replicator
   * @param {string} params.identity - the identity that owns the replicator
   * @returns {Replicator} a new replicator
   */
  async createReplicator (params, opts = { save: true }) {
    // ensure a public key was passed
    assert(params.identity && params.identity.publicKey, 'invalid: identity public key required')
    const {
      name,
      address,
      identity
    } = params
    // shorten for saving references
    const shortPublicKey = shortHex(identity.publicKey)
    // the replicator storage location is /appStorage/publicKey/address
    const storage = path.join(this.rootStorage, shortPublicKey)
    // create the replicator and make it ready
    const replicator = Replicator(storage, address, identity, {
      name: name,
      corestore: this.corestore
    })
    await replicator.ready()
    const shortAddress = shortHex(replicator.address)
    const reference = [shortPublicKey, shortAddress].join('/')
    // save a reference in our index including the full address and name
    if (opts.save) {
      await this.db.put(reference, {
        name: replicator.name,
        address: hex(replicator.address)
      })
    }
    this.cache.set(reference, replicator)
    return replicator
  }

  /**
   * delete a replicator
   * @param {object} params - a parameters object
   * @param {string} params.id - the address of the replicator
   * @param {string} params.identity - the identity that owns the replicator
   * @returns {Replicator} a deleted replicator
   */
  async deleteReplicator (replicator, identity) {
    const reference = [shortHex(identity.publicKey), shortHex(replicator.address)].join('/')
    // first it remove from the cache
    this.cache.delete(reference)
    // then remove references from the database
    await this.db.del(reference)
    // finally delete its storage location
    await replicator.destroy()
    // return the destroyed replicator
    return replicator
  }
}

module.exports = ReplicatorFactory
