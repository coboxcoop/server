const assert = require('assert')
const { Superusers } = require('@coboxcoop/superusers')
const { hex, shortHex } = require('../../util')
const path = require('path')

/* load jsdoc type definitions */
require('./../types')

class SeederFactory {
  /**
   * a factory for creating seeders, passes the relevant parameters
   * down to the Superusers constructor, ensuring the storage location on
   * disk for hypercores is set, as well as corestore being present
   * nameseeders storage under the relevant identity, and stores an
   * index reference under that identity
   * @param {Map} api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * fetches dependencies once unlock has been called
   */
  ready () {
    const settings = this._api.get('app.settings')
    this.rootStorage = settings.storage
    this.db = this._api.get('db.seeders')
    this.cache = this._api.get('cache.seeders')
    this.corestore = this._api.get('dat.store')
    this.deriveKeyPair = this._api.get('setup.signing-key-pair')
  }

  /**
   * create a new seeder
   * @param {object} params - a parameters object
   * @param {string} params.name - the name of the seeder
   * @param {string} params.address - the address of the seeder
   * @param {string} params.encryptionKey - the encryption key for the seeder
   * @param {string} params.identity - the identity that owns the seeder
   * @returns {Superusers} a new seeder
   */
  async createSeeder (params, opts = { save: true }) {
    // ensure a public key was passed, and secret key wasnt!
    assert(params.identity && params.identity.publicKey, 'invalid: identity public key required')
    const {
      name,
      address,
      encryptionKey,
      identity
    } = params
    // shorten for saving references
    const shortPublicKey = shortHex(identity.publicKey)
    // the seeder storage location is /appStorage/publicKey/address
    const storage = path.join(this.rootStorage, shortPublicKey)
    // create the seeder and make it ready
    const seeder = new Superusers(storage, address, identity, {
      name: name,
      encryptionKey: encryptionKey,
      corestore: this.corestore,
      deriveKeyPair: this.deriveKeyPair
    })
    await seeder.ready()
    const shortAddress = shortHex(seeder.address)
    const reference = [shortPublicKey, shortAddress].join('/')

    if (opts.save) {
      await Superusers.onCreate(seeder, { identity })
      // save a reference in our index including the full address and name
      await this.db.put(reference, {
        name: seeder.name,
        address: hex(seeder.address)
      })
    }

    this.cache.set(reference, seeder)
    return seeder
  }

  /**
   * delete a seeder
   * @param {object} params - a parameters object
   * @param {string} params.id - the address of the seeder
   * @param {string} params.identity - the identity that owns the seeder
   * @returns {Superusers} a deleted seeder
   */
  async deleteSeeder (seeder, identity) {
    const reference = [shortHex(identity.publicKey), shortHex(seeder.address)].join('/')
    // first it remove from the cache
    this.cache.delete(reference)
    // then remove references from the database
    await this.db.del(reference)
    // finally delete its storage location
    await seeder.destroy()
    // return the destroyed seeder
    return seeder
  }
}

module.exports = SeederFactory
