const assert = require('assert')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:factories/space')
const path = require('path')
const { Space } = require('@coboxcoop/space')
const { hex, shortHex } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

class SpaceFactory {
  /**
   * a factory for creating spaces, passes the relevant parameters
   * down to the Space constructor, ensuring the storage location on
   * disk for hypercores is set, as well as corestore being present
   * namespaces storage under the relevant identity, and stores an
   * index reference under that identity
   * @param {Map} api
   */
  constructor (api) {
    this._api = api
  }

  /**
   * injects dependencies once unlock has been called
   */
  async ready () {
    const settings = this._api.get('app.settings')
    this.rootStorage = settings.storage
    this.db = this._api.get('db.spaces')
    this.cache = this._api.get('cache.spaces')
    this.corestore = this._api.get('dat.store')
    this.network = this._api.get('dat.networker')
    this.deriveKeyPair = this._api.get('setup.signing-key-pair')
  }

  /**
   * create a new space
   * @param {object} params - a parameters object
   * @param {string} params.name - the name of the space
   * @param {string} params.address - the address of the space
   * @param {string} params.encryptionKey - the encryption key for the space
   * @param {string} params.identity - the identity that owns the space
   * @returns {Space} a new space
   */
  async createSpace (params, opts = { save: true }) {
    // ensure a public key was passed
    assert(params.identity && params.identity.publicKey, 'invalid: identity public key required')
    let {
      name,
      address,
      encryptionKey,
      settings,
      identity
    } = params
    if (address && !Buffer.isBuffer(address)) address = Buffer.from(address, 'hex')
    if (encryptionKey && !Buffer.isBuffer(encryptionKey)) encryptionKey = Buffer.from(encryptionKey, 'hex')
    // shorten for caching and saving references
    const shortPublicKey = shortHex(identity.publicKey)
    // the space storage location is /appStorage/publicKey/address
    const storage = path.join(this.rootStorage, shortPublicKey)
    // create the space
    const space = new Space(storage, address, identity, {
      name: name,
      settings: settings,
      encryptionKey: encryptionKey,
      corestore: this.corestore,
      network: this.network,
      deriveKeyPair: this.deriveKeyPair
    })
    // wait till ready
    await space.ready()
    // save a reference in our index including the full address and name
    const shortAddress = shortHex(space.address)
    const reference = [shortPublicKey, shortAddress].join('/')
    if (opts.save) {
      await Space.onCreate(space, { identity })
      await this.db.put(reference, {
        name: space.name,
        address: hex(space.address),
        encryptionKey: hex(encryptionKey)
      })
    }
    this.cache.set(reference, space)
    return space
  }

  /**
   * delete a space
   * @param {object} params - a parameters object
   * @param {string} params.id - the address of the space
   * @param {string} params.identity - the identity that owns the space
   * @returns {Space} a deleted space
   */
  async deleteSpace (space, identity) {
    const reference = [shortHex(identity.publicKey), shortHex(space.address)].join('/')
    // first it remove from the cache
    this.cache.delete(reference)
    // then remove references from the database
    await this.db.del(reference)
    // finally delete its storage location
    await space.destroy()
    // return the destroyed space
    return space
  }
}

module.exports = SpaceFactory
