const crypto = require('@coboxcoop/crypto')
const resourceParams = require('./resource-params')
const { hex } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

module.exports = createParams

/**
 * seeds an address and encryption key if they don't exist
 * for creating new spaces / seeders
 *
 * @param {Object} params
 * @param {string} params.name
 * @param {string} params.address
 * @param {string} params.encryptionkey
 * @returns {Resource}
 */
function createParams (params) {
  params = resourceParams(params)
  if (!params.address && !params.encryptionKey) {
    return Object.assign(params, {
      address: hex(crypto.address()),
      encryptionKey: hex(crypto.encryptionKey())
    })
  }
  return params
}
