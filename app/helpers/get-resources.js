const { shortHex } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

module.exports = {
  getResources,
  getResource
}

/**
 * fetch all resources from the cache for this public key
 * @param {AsyncMap} cache - the cache to fetch from
 * @param {string} publicKey - the public key to match against
 * @param {Object} opts - options
 * @param {string} opts.address - optional address to scope the fetch
 * @returns {Array.<Space|Superusers|Replicator>}
 */
async function getResources (cache, publicKey, opts = {}) {
  const proms = Array
    // fetch all cache keys of format shortHex(publicKey)/shortHex(address)
    .from(cache.keys())
    // separate the public key from the address
    .map((key) => key.split('/'))
    // fetch only those belonging to this identity
    .filter(([shortPublicKey, shortAddress]) => shortHex(publicKey) === shortPublicKey)
    // if address parameter was specified, filter to (in theory) only one that matches
    .filter(([shortPublicKey, shortAddress]) => opts.address ? shortHex(opts.address) === shortAddress : true)
    // reconstruct the key to fetch from the cache
    .map(([shortPublicKey, shortAddress]) => [shortPublicKey, shortAddress].join('/'))
    // use async getter to fetch the resource
    .map((key) => cache.get(key))
  // resolve them all
  return Promise.all(proms)
}

/**
 * fetch the resource from the cache that matches this public key and address
 * @param {AsyncMap} cache - the cache to fetch from
 * @param {string} publicKey - the public key to match against
 * @param {string} address - optional address to scope the fetch
 * @returns {Space|Superusers|Replicator}
 */
async function getResource (cache, publicKey, address) {
  const resources = await getResources(cache, publicKey, { address })
  return resources[0]
}
