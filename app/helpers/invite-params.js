const SpaceDecorator = require('../decorators/space')
const { loadKey } = require('@coboxcoop/keys')
const { pick } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

module.exports = inviteParams

/**
 * @param {Space|Replicator|Superusers} entry
 * @param {Object} params
 * @param {string} params.name
 * @param {string} params.publicKey
 * @param {string} params.address
 * @returns {Object}
 */
function inviteParams (entry, params) {
  var encryptionKey = loadKey(entry.storage, 'encryption_key')
  var entryParams = Object.assign(SpaceDecorator(entry).toJSON(), params)
  var picked = pick(entryParams, ['publicKey', 'address', 'name'])
  return Object.assign({ encryptionKey }, picked)
}
