const { Transform } = require('stream')
const { assign } = Object

/* load jsdoc type definitions */
require('./../types')

module.exports = function jsonify () {
  return new Transform(assign({ objectMode: true }, {
    transform (msg, encoding, next) {
      if (msg.sync) return next()
      this.push(JSON.stringify(msg))
      next()
    }
  }))
}
