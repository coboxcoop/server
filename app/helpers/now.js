/* load jsdoc type definitions */
require('./../types')

module.exports = now

/**
 * returns a datetime of the format: '2021-08-05-11:36:18'
 * @returns {string}
 */
function now () {
  return new Date().toISOString().replace(/T/, '-').replace(/\..+/, '')
}
