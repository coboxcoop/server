const { trim } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

module.exports = {
  queryByTimestamp,
  queryByType,
  queryByFilename
}

/**
 * returns an object to be used by map-filter-reduce
 * see: https://github.com/dominictarr/map-filter-reduce
 *
 * @param {string|Array.<string>} type - the message type(s) we want to fetch
 * @param {number} from - a timestamp
 * @param {number} to - a timestamp
 * @returns {Query}
 */
function queryByType (type, from = 0, to) {
  if (!Array.isArray(type)) type = [type]
  return [{
    $filter: {
      value: trim({
        type: {
          $in: type
        },
        timestamp: {
          $gt: from,
          $lt: to
        }
      })
    }
  }]
}

/**
 * returns an object to be used by map-filter-reduce
 * see: https://github.com/dominictarr/map-filter-reduce
 *
 * @param {number} from - a timestamp
 * @param {number} to - a timestamp
 * @returns {Query}
 */
function queryByTimestamp (from = 0, to) {
  return [{
    $filter: {
      value: trim({
        timestamp: {
          $gt: from,
          $lt: to
        }
      })
    }
  }]
}

/**
 * returns an object to be used by map-filter-reduce
 * see: https://github.com/dominictarr/map-filter-reduce
 *
 * @param {string|undefined} filename - the filename to query for
 * @param {number} from - a timestamp
 * @param {number} to - a timestamp
 * @returns {Query}
 */
function queryByFilename (filename = undefined, from = 0, to) {
  return [{
    $filter: {
      value: trim({
        timestamp: {
          $gt: from,
          $lt: to
        },
        filename: filename
      })
    }
  }]
}
