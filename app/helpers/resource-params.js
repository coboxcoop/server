const { pick } = require('../../util')

/* load jsdoc type definitions */
require('./../types')

module.exports = resourceParams

/**
 * fetch resource (e.g. a space) parameters
 *
 * @param {object} params
 * @param {string} params.name
 * @param {string} params.address
 * @param {string} params.encryptionkey
 * @returns {Resource}
 */
function resourceParams (params) {
  return pick(params, ['name', 'address', 'encryptionKey'])
}
