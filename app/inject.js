const { MemoryStore } = require('express-session')
const Invite = require('@coboxcoop/key-exchange')
const AsyncMap = require('../lib/cache')
const level = require('level')
const sub = require('subleveldown')
const Live = require('level-live-stream')
const path = require('path')
const { pick } = require('../util')

/* load jsdoc type definitions */
require('./types')

const defaultEncoding = { keyEncoding: 'utf-8', valueEncoding: 'json' }
const details = pick(require('../package.json'), ['name', 'description', 'version', 'author', 'license', 'bugs'])

module.exports = inject

/**
 * create a container for our dependencies and cross-reference them
 * now all our classes will receive only what they need
 *
 * @param {Application} app - an instance of the main application (see index.js)
 * @returns {Map}
 */
function inject (app) {
  const api = new Map()
  const maindb = level(path.join(app.settings.storage, 'db'), defaultEncoding)
  const peerdb = sub(maindb, 'peers', defaultEncoding)
  Live.install(peerdb)
  // TODO: use a crypto-encoder and migrate db setup to unlock
  // create our leveldbs for storing indices
  api.set('db.main', maindb)
  api.set('db.sessions', new MemoryStore())
  api.set('db.spaces', sub(maindb, 'spaces', defaultEncoding))
  api.set('db.replicators', sub(maindb, 'replicators', defaultEncoding))
  api.set('db.seeders', sub(maindb, 'seeders', defaultEncoding))
  api.set('db.identities', sub(maindb, 'identity', defaultEncoding))
  api.set('db.peers', peerdb)
  // set corestore and networker to null, these are generated on authentication
  // after the parent key has been decrypted, see: app/services/setup/unlock.js
  api.set('dat.store', null)
  api.set('dat.networker', null)
  // initialize factories, ready _must_ be called before they can be used (again, see unlock.js)
  api.set('factory.spaces', new (require('./factories/space'))(api))
  api.set('factory.replicators', new (require('./factories/replicator'))(api))
  api.set('factory.seeders', new (require('./factories/seeder'))(api))
  api.set('factory.identities', new (require('./factories/identity'))(api))
  // make express and other settings available in the API
  api.set('app.express', app.app)
  api.set('app.router', require('express').Router())
  api.set('app.settings', app.settings)
  api.set('app.details', details)
  api.set('app.events', require('merge-stream')())
  api.set('app.setup', require('./services/setup')(api))
  api.set('app.teardown', require('./services/teardown')(api))
  api.set('app.unlock', require('./services/unlock')(api))
  api.set('app.lock', require('./services/lock')(api))
  // initialize all services used in app setup
  api.set('setup.events', require('./services/setup/events')(api))
  api.set('setup.fetch-routes', require('./services/setup/fetch-routes')(api))
  api.set('setup.corestore', require('./services/setup/corestore')(api))
  api.set('setup.spaces', require('./services/setup/spaces')(api))
  api.set('setup.replicators', require('./services/setup/replicators')(api))
  api.set('setup.seeders', require('./services/setup/seeders')(api))
  api.set('setup.identities', require('./services/setup/identities')(api))
  api.set('setup.auth-controllers', require('./services/setup/auth-controllers')(api))
  api.set('setup.unauth-controllers', require('./services/setup/unauth-controllers')(api))
  // initialize teardown services (opposite of setup!)
  api.set('teardown.corestore', require('./services/teardown/corestore')(api))
  api.set('teardown.spaces', require('./services/teardown/spaces')(api))
  api.set('teardown.replicators', require('./services/teardown/replicators')(api))
  api.set('teardown.seeders', require('./services/teardown/seeders')(api))
  api.set('teardown.mount', require('./services/teardown/mount')(api))
  // initialize backup/import/export service
  api.set('services.backup.pack', require('./services/backup/pack')(api))
  api.set('services.backup.unpack', require('./services/backup/unpack')(api))
  // invite services
  api.set('services.invites.pack', Invite.create)
  api.set('services.invites.unpack', Invite.open)
  api.set('services.invites.create', require('./services/invites/create')(api))
  api.set('services.invites.open', require('./services/invites/open')(api))
  // authentication services
  api.set('services.authenticate', require('./services/authentication/with-password')(api))
  api.set('services.authentication.password-exists', require('./services/authentication/password-hash-exists')(api))
  api.set('services.authentication.save-password', require('./services/authentication/save-password-hash')(api))
  api.set('services.encryption.save-parent-key', require('./services/encryption/save-parent-key')(api))
  api.set('services.encryption.get-parent-key', require('./services/encryption/get-parent-key')(api))
  api.set('services.encryption.derive-key-pair', require('./services/encryption/derive-key-pair')(api))
  // remote command services
  api.set('commands.replicate.start', require('./services/commands/start-replicate')(api))
  api.set('commands.replicate.stop', require('./services/commands/stop-replicate')(api))
  api.set('commands.broadcast.start', require('./services/commands/start-broadcast')(api))
  api.set('commands.broadcast.stop', require('./services/commands/stop-broadcast')(api))
  // initialize our caches
  api.set('cache.identities', new Map())
  api.set('cache.spaces', new AsyncMap(async (id, space) => space.ready()))
  api.set('cache.replicators', new AsyncMap(async (id, replicator) => replicator.ready()))
  api.set('cache.seeders', new AsyncMap(async (id, seeder) => seeder.ready()))
  // build all controllers
  api.set('controllers.admin.seeders', new (require('./controllers/admin/seeders'))(api))
  api.set('controllers.admin.seeders.invites', new (require('./controllers/admin/seeders/invites'))(api))
  api.set('controllers.admin.seeders.connections', new (require('./controllers/admin/seeders/connections'))(api))
  api.set('controllers.admin.seeders.peers', new (require('./controllers/admin/seeders/peers'))(api))
  api.set('controllers.auth', new (require('./controllers/auth'))(api))
  api.set('controllers.commands.broadcast', new (require('./controllers/admin/seeders/commands/broadcasts'))(api))
  api.set('controllers.commands.replicate', new (require('./controllers/admin/seeders/commands/replicates'))(api))
  api.set('controllers.drive', new (require('./controllers/drive'))(api))
  api.set('controllers.export', new (require('./controllers/export'))(api))
  api.set('controllers.identities', new (require('./controllers/identities'))(api))
  api.set('controllers.import', new (require('./controllers/import'))(api))
  // api.set('controllers.local.seeders', new (require('./controllers/seeders'))(api))
  api.set('controllers.events', new (require('./controllers/events'))(api))
  api.set('controllers.mounts', new (require('./controllers/mounts'))(api))
  api.set('controllers.peers', new (require('./controllers/peers'))(api))
  api.set('controllers.registration', new (require('./controllers/registration'))(api))
  api.set('controllers.replicators', new (require('./controllers/replicators'))(api))
  api.set('controllers.replicators.connections', new (require('./controllers/replicators/connections'))(api))
  api.set('controllers.spaces', new (require('./controllers/spaces'))(api))
  api.set('controllers.spaces.invites', new (require('./controllers/spaces/invites'))(api))
  api.set('controllers.spaces.connections', new (require('./controllers/spaces/connections'))(api))
  api.set('controllers.spaces.peers', new (require('./controllers/spaces/peers'))(api))
  api.set('controllers.system', new (require('./controllers/system'))(api))
  // upload helper for handling multipart form data
  api.set('helpers.upload', require('multer')({ dest: app.settings.tmp }))
  return api
}
