const path = require('path')
const fs = require('fs')

module.exports = bootstrapUI

function bootstrapUI (uiPath) {
  return async function sendUI (req, res, next) {
    if (!req.baseUrl.startsWith('/api')) {
      res.statusCode = 200
      return fs.createReadStream(path.join(uiPath, 'index.html')).pipe(res)
    }
    return next()
  }
}
