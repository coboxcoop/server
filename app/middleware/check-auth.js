const debug = require('@coboxcoop/logger')('@coboxcoop/server:check-auth')
const Unauthorized = require('../errors/unauthorized')

module.exports = getCheckAuth

function getCheckAuth (api) {
  return async function checkAuth (req, res, next) {
    try {
      if (!loggedIn(req.session)) {
        const err = new Unauthorized('you are not authorized')
        // if we're not authorized, an edge case exists where a session
        // may exist and the lock may be off, in which case, we want to lock
        // this will only occur when something tampers with the sid, as we're
        // relying on the browser or tmp to reliably save the sid
        const app = api.get('app.express')
        const lock = api.get('app.lock')
        // when an sid has expired, e.g. the server was killed
        // while the sid was persisted either in tmp or in browser
        // this conditional won't resolve and lock wont be called
        if (!app.get('lock')) await lock()
        throw err
      }
      next()
    } catch (err) {
      return res
        .status(err.status)
        .json({ errors: [err.message] })
    }
  }

  function loggedIn (session) {
    return session && session.identity
  }
}
