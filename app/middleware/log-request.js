const { isProduction } = require('../../util')

module.exports = function logRequest () {
  return function (req, res, next) {
    if (isProduction()) {
      req.log.info('request')
    }
    next()
  }
}
