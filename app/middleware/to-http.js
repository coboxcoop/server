const debug = require('@coboxcoop/logger')('@coboxcoop/server/to-http')
const { validationResult } = require('express-validator')
const { isEmpty, isHexString } = require('../../util')
const contentDisposition = require('content-disposition')

module.exports = toHTTP

/**
 * handle a controller and a mapping of an action,
 * returns express middleware that is used to compile
 * the query, params and body objects into a 'params'
 * and an 'opts' object which are passed to the relevant
 * controller.
 *
 * ATTENTION:
 * req.query and req.params are merged into a single opts
 * object, then, the request is a GET or a DELETE we treat
 * req.body as null, and the controller parameters are
 * req.query and req.params. if the request is a POST,
 * PUT or PATCH, the req.body are the controller params
 * while req.query and req.params are the controller opts
 *
 * @param {Object} controller - the controller object
 * @param {string} action - the controller method to call
 * @param {Object} opts - additional options about how to process the request and return the response
 */
function toHTTP (controller, action, opts = {}) {
  return async function (req, res) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      debug({ msg: 'request failed', errors })

      return res
        .status(422)
        .json({
          errors: errors.array()
        })
    }

    try {
      let obj = req.body
      let _opts = Object.assign({}, req.query, req.params)

      // handle names or addresses as params.id
      if (_opts.id) {
        if (isHexString(_opts.id, 64)) _opts.address = _opts.id
        else _opts.name = _opts.id
      }

      // if this is a GET, or a DELETE, req.body should be null
      // if this is a PUT, POST, or PATCH, req.body is an object
      if (isEmpty(obj)) {
        obj = _opts
        _opts = {}
      }

      // pass the session object down in opts
      _opts.session = req.session

      // always decorate when handling requests
      if (req.file) obj.file = req.file
      if (req.files) obj.files = req.files
      _opts.decorate = true

      if (!controller[action]) throw new Error(`${action} not defined on ${controller.name}`)
      const method = controller[action].bind(controller)
      debug({ controller: controller.constructor.name, action, method: method && method.name.substr(6, method.name.length) })
      if (!method) throw new Error('invalid controller method', controller.constructor.name, action, method)

      let data = await method(obj, _opts)

      if (opts.stream) {
        data.on('end', () => res.end())
        if (opts.filename) res.setHeader('Content-Disposition', contentDisposition(opts.filename))
        var s = data.pipe(res)
        s.on('error', (err) => { throw new Error('Error when creating stream', err) })
        return
      }

      if (opts.download) {
        // if downloading a file, controllers must return an object
        // of format: { name, filepath }
        return res
          .status(200)
          .download(data.filepath, data.name)
      }

      return res
        .status(200)
        .json(data)
    } catch (err) {
      const status = err.status || 500
      debug({
        msg: 'request error: ' + err.message,
        status: status,
        code: err.code,
        stack: err.stack
      })

      return res
        .status(status)
        .json({
          errors: [{
            value: err.name,
            msg: err.message
          }]
        })
    }
  }
}
