const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const websocketStream = require('websocket-stream/stream')
const { assign } = Object
const jsonify = require('../helpers/jsonify')

/**
 * handle a controller and a mapping of an action,
 * returns express middleware that is used to compile
 * the query, params and body objects into a 'params'
 * and an 'opts' object which are passed to the relevant
 * controller. sends a readable stream from each
 * controller action back to the client by broadcasting
 * over a web socket
 *
 * @param {Object} controller - the controller object
 * @param {string} action - the controller method to call
 * @param {Object} opts - additional options about how to process the request and return the response
 * @returns {Readable}
 */
module.exports = function toWS (controller, action, opts = {}) {
  return async function (ws, res) {
    let opts = assign({}, res.query, res.params)

    const method = controller[action].bind(controller)
    debug({ controller: controller.constructor.name, action, method: method && method.name.substr(6, method.name.length) })
    if (!method) throw new Error('invalid controller method', controller.constructor.name, action, method)

    return method(opts)
      .pipe(jsonify())
      .pipe(websocketStream(ws, {
        objectMode: true
      }))
  }
}
