const express = require('express')
const toHTTP = require('../../../middleware/to-http')
const adminSeederValidator = require('../../../validators/admin/seeders')
const inviteValidator = require('../../../validators/invites')
const replicateCommandsValidator = require('../../../validators/commands/replicates')

module.exports = adminSeederRouter

function adminSeederRouter (api) {
  const router = express.Router()
  const validators = {
    adminSeeders: adminSeederValidator(api),
    invites: inviteValidator(api),
    replicateCommands: replicateCommandsValidator(api)
  }

  /**
   * @openapi
   * /api/admin/seeders:
   *   get:
   *     description: list admin seeders
   *     summary: list admin seeders
   *     responses:
   *       200:
   *         description: returns a list of admin seeders
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   address:
   *                     type: string
   */
  router.get('/', toHTTP(api.get('controllers.admin.seeders'), 'list'))
  /**
   * @openapi
   * /api/admin/seeders:
   *   post:
   *     description: create a new seeder or join using an invite code
   *     summary: create a new seeder or join using an invite code
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: magma-collective
   *               address:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *               encryptionKey:
   *                 type: string
   *                 example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *               code:
   *                 type: string
   *                 example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *     responses:
   *       200:
   *         description: creates an admin seeder
   *       500:
   *         description: internal server error
   */
  router.post('/', validators.adminSeeders.create, toHTTP(api.get('controllers.admin.seeders'), 'create'))
  /**
   * @openapi
   * /api/admin/seeders/connections:
   *   post:
   *     description: returns all swarming seeders
   *     summary: begin replication on all seeders
   *     responses:
   *       200:
   *         description: success to start swarming
   *       500:
   *         description: failed to start swarming
   */
  router.post('/connections', toHTTP(api.get('controllers.admin.seeders.connections'), 'createAll'))
  /**
   * @openapi
   * /api/admin/seeders/connections:
   *   delete:
   *     description: returns all unswarming seeders
   *     summary: stop replication on all seeders
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       500:
   *         description: failed to stop swarming
   */
  router.delete('/connections', toHTTP(api.get('controllers.admin.seeders.connections'), 'destroyAll'))
  /**
   * @openapi
   * /api/admin/seeders/invites/accept:
   *   get:
   *     description: join a seeder using an invite code
   *     summary: join a seeder using an invite code
   *     responses:
   *       200:
   *         description: returns an admin seeder
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                   example: magma-collective
   *                 address:
   *                   type: string
   *                   example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: code
   *       in: query
   *       description: address of the seeder
   *       example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *       required: true
   */
  router.get('/invites/accept', validators.invites.accept, toHTTP(api.get('controllers.admin.seeders.invites'), 'accept'))
  /**
   * @openapi
   * /api/admin/seeders/{id}:
   *   delete:
   *     description: destroy a seeder
   *     summary: completely destroy a seeder locally, wiping all local data
   *     responses:
   *       200:
   *         description: returns the destroyed seeder
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                   example: magma-collective
   *                 address:
   *                   type: string
   *                   example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.delete('/:id', toHTTP(api.get('controllers.admin.seeders'), 'destroy'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/peers:
   *   get:
   *     description: get all seeder admin peers
   *     summary: get all known admins for this seeder
   *     responses:
   *       200:
   *         description: returns a list of admin identities
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   publicKey:
   *                     type: string
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: failed to start swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.get('/:id/peers', toHTTP(api.get('controllers.admin.seeders.peers'), 'list'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/last-sync:
   *   get:
   *     description: get all last sync times with remote peers
   *     summary: get all last sync times with remote peers
   *     responses:
   *       200:
   *         description: returns an object of peers
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: failed to start swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.get('/:id/last-sync', toHTTP(api.get('controllers.admin.seeders.peers'), 'lastSync'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/connections:
   *   post:
   *     description: returns a swarming seeder
   *     summary: begin replication with remote peers
   *     responses:
   *       200:
   *         description: success to start swarming
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: failed to start swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.post('/:id/connections/', toHTTP(api.get('controllers.admin.seeders.connections'), 'create'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/connections:
   *   delete:
   *     description: returns an unswarming seeder
   *     summary: stop replication with remote peers
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: failed to stop swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.delete('/:id/connections/', toHTTP(api.get('controllers.admin.seeders.connections'), 'destroy'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/invites:
   *   get:
   *     description: get all invites to this seeder
   *     summary: get all known invites for new admins to this seeder
   *     responses:
   *       200:
   *         description: returns a list of admin identities
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   type:
   *                     type: string
   *                     pattern: ^peer/about$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                       publicKey:
   *                         type: string
   *                         example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.get('/:id/invites', toHTTP(api.get('controllers.admin.seeders.invites'), 'list'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/invites:
   *   post:
   *     description: generate an invite code to a seeder
   *     summary: generate an invite code to a seeder
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               publicKey:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *     responses:
   *       200:
   *         description: returns an invite
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                   type:
   *                     type: string
   *                     pattern: ^peer/about$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                       publicKey:
   *                         type: string
   *                         example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *                       code:
   *                         type: string
   *                         example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.post('/:id/invites/', validators.invites.create, toHTTP(api.get('controllers.admin.seeders.invites'), 'create'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/commands/replicates:
   *   get:
   *     description: list all replication commands for this seeder
   *     summary: view a list of all locally known spaces being replicated by this seeder
   *     responses:
   *       200:
   *         description: returns a list of admin identities
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   type:
   *                     type: string
   *                     pattern: ^peer/about$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                       name:
   *                         type: string
   *                         example: magma-collective
   *                       address:
   *                         type: string
   *                         example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.get('/:id/commands/replicates', toHTTP(api.get('controllers.commands.replicate'), 'list'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/invites:
   *   post:
   *     description: send a command to the specified seeder to begin replicating an address
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: magma-collective
   *               address:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *     responses:
   *       200:
   *         description: returns a command
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                   type:
   *                     type: string
   *                     pattern: ^command/replicate$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                   name:
   *                     type: string
   *                     example: magma-collective
   *                   address:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.post('/:id/commands/replicate', validators.replicateCommands.create, toHTTP(api.get('controllers.commands.replicate'), 'create'))
  /**
   * @openapi
   * /api/admin/seeders/{id}/invites:
   *   post:
   *     description: send a command to the specified seeder to stop replicating an address
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: magma-collective
   *               address:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *     responses:
   *       200:
   *         description: returns a command
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                   type:
   *                     type: string
   *                     pattern: ^command/unreplicate$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                   name:
   *                     type: string
   *                     example: magma-collective
   *                   address:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       404:
   *         description: seeder doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the seeder
   *       required: true
   */
  router.post('/:id/commands/unreplicate', validators.replicateCommands.destroy, toHTTP(api.get('controllers.commands.replicate'), 'destroy'))

  // TODO: seeder registration via UDP broadcast is disabled
  // router.patch('/:id/commands/broadcast', toHTTP(api.get('controllers.commands.broadcast'), 'update'))
  // router.post('/:id/commands/announce', toHTTP(api.get('controllers.commands.broadcast'), 'create'))
  // router.post('/:id/commands/hide', toHTTP(api.get('controllers.commands.broadcast'), 'destroy'))

  return router
}
