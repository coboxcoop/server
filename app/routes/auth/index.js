const express = require('express')
const toHTTP = require('../../middleware/to-http')
const authenticationValidator = require('../../validators/auth')

module.exports = authRouter

function authRouter (api) {
  const router = express.Router()
  const validator = authenticationValidator(api)
  /**
   * @openapi
   * /api/auth:
   *   get:
   *     description: check for a live session
   *     summary: check of an existing cobox session
   *     responses:
   *       200:
   *         description: a session exists
   *       404:
   *         description: no open session exists
   *       500:
   *         description: internal server error
   */
  router.get('/', toHTTP(api.get('controllers.auth'), 'get'))
  /**
   * @openapi
   * /api/auth:
   *   post:
   *     description: decrypt your data
   *     summary: login to cobox
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               password:
   *                 type: string
   *     responses:
   *       200:
   *         description: decrypts and loads your data
   *       500:
   *         description: internal server error
   */
  router.post('/', validator.create, toHTTP(api.get('controllers.auth'), 'create'))
  /**
   * @openapi
   * /api/auth:
   *   delete:
   *     description: closes your cobox
   *     summary: logout of cobox
   *     responses:
   *       200:
   *         description: successfully logged out
   *       404:
   *         description: not logged in
   *       500:
   *         description: internal server error
   */
  router.delete('/', toHTTP(api.get('controllers.auth'), 'destroy'))

  return router
}
