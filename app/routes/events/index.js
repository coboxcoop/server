const express = require('express')
const expressWs = require('express-ws')
const toWS = require('../../middleware/to-ws')

module.exports = mainRouter

function mainRouter (api) {
  const router = express.Router()
  expressWs(router)

  router.ws('/', toWS(api.get('controllers.events'), 'live'))

  return router
}
