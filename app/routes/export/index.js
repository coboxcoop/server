const express = require('express')
const toHTTP = require('../../middleware/to-http')

module.exports = exportRouter

function exportRouter (api) {
  const router = express.Router()

  /**
   * @openapi
   * /api/export:
   *   get:
   *     description: export your keys and spaces for backup
   *     summary: export your keys and spaces for backup
   *     responses:
   *       200:
   *         description: returns a list of identities
   *         content:
   *           - multipart/form-data
   */
  router.get('/', toHTTP(api.get('controllers.export'), 'index', { stream: true, filename: 'backup.cobox' }))

  return router
}
