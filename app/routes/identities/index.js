const express = require('express')
const toHTTP = require('../../middleware/to-http')
const identityValidator = require('../../validators/identities')

module.exports = identityRouter

function identityRouter (api) {
  const router = express.Router()
  const validator = identityValidator(api)

  /**
   * @openapi
   * /api/identities:
   *   get:
   *     description: list all identities
   *     summary: list all identities
   *     responses:
   *       200:
   *         description: returns a list of identities
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   publicKey:
   *                     type: string
   */
  router.get('/', toHTTP(api.get('controllers.identities'), 'list'))
  /**
   * @openapi
   * /api/identities:
   *   post:
   *     description: create a new identity
   *     summary: create a new identity
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: spaceland
   *     responses:
   *       200:
   *         description: creates an new identity
   *       403:
   *         description: unauthorized
   *       500:
   *         description: internal server error
   */
  router.post('/', validator.create, toHTTP(api.get('controllers.identities'), 'create'))
  /**
   * @openapi
   * /api/identities/current:
   *   get:
   *     description: get current identity
   *     summary: get current identity
   *     responses:
   *       200:
   *         description: returns an identity
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                 publicKey:
   *                   type: string
   *       403:
   *         description: unauthorized
   *       500:
   *         description: internal server error
   */
  router.get('/current', toHTTP(api.get('controllers.identities'), 'current'))
  /**
   * @openapi
   * /api/identities/{id}/change:
   *   get:
   *     description: switch identity
   *     summary: switch identity
   *     responses:
   *       200:
   *         description: returns an identity
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                 publicKey:
   *                   type: string
   *       403:
   *         description: unauthorized
   *       404:
   *         description: identity doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: public key of the identity
   *       required: true
   */
  router.get('/:id/change', toHTTP(api.get('controllers.identities'), 'change'))
  /**
   * @openapi
   * /api/identities/{id}:
   *   patch:
   *     description: update an identity
   *     summary: update an identity
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: spaceland
   *     responses:
   *       200:
   *         description: updates an identity
   *       403:
   *         description: unauthorized
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: public key of the identity
   *       required: true
   */
  router.patch('/:id', validator.update, toHTTP(api.get('controllers.identities'), 'update'))

  return router
}
