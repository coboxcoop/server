const express = require('express')
const toHTTP = require('../../middleware/to-http')

module.exports = importRouter

function importRouter (api) {
  const router = express.Router()
  const uploader = api.get('helpers.upload')

  router.post('/', uploader.single('export'), toHTTP(api.get('controllers.import'), 'create'))

  return router
}
