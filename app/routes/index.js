const expressWs = require('express-ws')
const getCheckAuth = require('../middleware/check-auth')
const toHTTP = require('../middleware/to-http')

module.exports = routes

/**
 * @param {Map} api - global dependency container
 * @returns Router - the root express router
 */
function routes (api) {
  const router = api.get('app.router')
  const checkAuth = getCheckAuth(api)
  expressWs(router)
  // for some reason, for stop to work, it needs to be declared first
  // unauthorized routes
  router.use('/system', require('./system')(api))
  router.use('/auth', require('./auth')(api))
  router.use('/register', require('./registration')(api))
  // authorized routes
  router.use('/', checkAuth, require('./events')(api))
  router.use('/admin/seeders', checkAuth, require('./admin/seeders')(api))
  router.use('/export', checkAuth, require('./export')(api))
  router.use('/identities', checkAuth, require('./identities')(api))
  router.use('/import', checkAuth, require('./import')(api))
  router.use('/peers', checkAuth, require('./peers')(api))
  router.use('/replicators', checkAuth, require('./replicators')(api))
  router.use('/spaces', checkAuth, require('./spaces')(api))
  // TODO: local seeder registration feature is current disabled
  // router.use('/seeders', checkAuth, setIdentity, require('./seeders')(api, validators))
  return router
}
