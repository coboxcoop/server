const express = require('express')
const toHTTP = require('../../middleware/to-http')

module.exports = peersRouter

function peersRouter (api) {
  const router = express.Router()
  /**
   * @openapi
   * /api/peers:
   *   post:
   *     description: list all peers
   *     summary: list all peers
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               type:
   *                 type: string
   *               peerId:
   *                 type: string
   *               lastSeenAt:
   *                 type: number
   *     responses:
   *       200:
   *         description: successfully setup cobox
   *       500:
   *         description: internal server error
   */
  router.get('/', toHTTP(api.get('controllers.peers'), 'list'))

  return router
}
