const express = require('express')
const toHTTP = require('../../middleware/to-http')
const authenticationValidator = require('../../validators/auth')

module.exports = registrationRouter

function registrationRouter (api) {
  const router = express.Router()
  const validator = authenticationValidator(api)
  /**
   * @openapi
   * /api/register:
   *   post:
   *     description: register
   *     summary: register and login to cobox
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               password:
   *                 type: string
   *     responses:
   *       200:
   *         description: successfully setup cobox
   *       500:
   *         description: internal server error
   */
  router.post('/', validator.create, toHTTP(api.get('controllers.registration'), 'create'))

  return router
}
