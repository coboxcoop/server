const express = require('express')
const toHTTP = require('../../middleware/to-http')
const replicatorValidator = require('../../validators/replicators')

module.exports = replicatorRouter

function replicatorRouter (api) {
  const router = express.Router()
  const validator = replicatorValidator(api)
  /**
   * @openapi
   * /api/replicators:
   *   get:
   *     description: list all replicators
   *     summary: list all replicators
   *     responses:
   *       200:
   *         description: returns a list of spacest you are replicating
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   address:
   *                     type: string
   */
  router.get('/', toHTTP(api.get('controllers.replicators'), 'list'))
  /**
   * @openapi
   * /api/replicators:
   *   post:
   *     description: begin replicating a given address
   *     summary : begin replicating a given address
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: magma-collective
   *               address:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *     responses:
   *       200:
   *         description: creates an replicator
   *       500:
   *         description: internal server error
   */
  router.post('/', validator.create, toHTTP(api.get('controllers.replicators'), 'create'))
  /**
   * @openapi
   * /api/replicators/connections:
   *   post:
   *     description: returns all swarming replicators
   *     summary: begin replication on all addresses
   *     responses:
   *       200:
   *         description: success to start swarming
   *       500:
   *         description: failed to start swarming
   */
  router.post('/connections', toHTTP(api.get('controllers.replicators.connections'), 'createAll'))
  /**
   * @openapi
   * /api/replicators/connections:
   *   delete:
   *     description: returns all unswarming replicators
   *     summary: stop replication on all addresses
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       500:
   *         description: failed to stop swarming
   */
  router.delete('/connections', toHTTP(api.get('controllers.replicators.connections'), 'destroyAll'))
  /**
   * @openapi
   * /api/replicators/{id}:
   *   delete:
   *     description: destroy a replicator
   *     summary: completely destroy a replicator locally, wiping all local data
   *     responses:
   *       200:
   *         description: returns the destroyed replicator
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                   example: cobox-project
   *                 address:
   *                   type: string
   *                   example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       402:
   *         description: forbidden
   *       404:
   *         description: replicator doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the replicator
   *       required: true
   */
  router.delete('/:id', toHTTP(api.get('controllers.replicators'), 'destroy'))
  /**
   * @openapi
   * /api/replicators/{id}/connections:
   *   post:
   *     description: returns a swarming replicator
   *     summary: begin replicating with remote peers
   *     responses:
   *       200:
   *         description: success to start swarming
   *       404:
   *         description: replicator doesn't exist
   *       500:
   *         description: failed to start swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the replicator
   *       required: true
   */
  router.post('/:id/connections', toHTTP(api.get('controllers.replicators.connections'), 'create'))
  /**
   * @openapi
   * /api/replicators/{id}/connections:
   *   delete:
   *     description: returns an unswarming replicator
   *     summary: stop replication with remote peers
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       404:
   *         description: replicator doesn't exist
   *       500:
   *         description: failed to stop swarming
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the replicator
   *       required: true
   */
  router.delete('/:id/connections', toHTTP(api.get('controllers.replicators.connections'), 'destroy'))

  return router
}
