const express = require('express')
const expressWs = require('express-ws')
const toHTTP = require('../../../middleware/to-http')
const toWS = require('../../../middleware/to-ws')

module.exports = seederRouter

function seederRouter (api) {
  const router = express.Router()
  expressWs(router)
  /**
   * @openapi
   * /api/seeders:
   *   get:
   *     description: list seeders on the LAN
   *     summary: list seeders on the LAN
   *     responses:
   *       200:
   *         description: returns a list of seeders
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   publicKey:
   *                     type: string
   *                   address:
   *                     type: string
   *                   timestamp:
   *                     type: number
   */
  router.get('/', toHTTP(api.get('controllers.local.seeders'), 'index'))
  /**
   * @openapi
   * /api/seeders/{id}:
   *   get:
   *     description: list seeders on the LAN
   *     summary: list seeders on the LAN
   *     responses:
   *       200:
   *         description: returns a list of seeders
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   publicKey:
   *                     type: string
   *                   address:
   *                     type: string
   *                   timestamp:
   *                     type: number
   *   parameters:
   *     - name: id
   *       in: path
   *       description: public key of the seeder
   *       required: true
   */
  router.get('/:id', toHTTP(api.get('controllers.local.seeders'), 'show'))
  router.ws('/', toWS(api.get('controllers.local.seeders'), 'live'))

  return router
}
