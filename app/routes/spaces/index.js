const express = require('express')
const toHTTP = require('../../middleware/to-http')
const spaceValidator = require('../../validators/spaces')
const driveValidator = require('../../validators/drive')
const mountValidator = require('../../validators/mount')
const inviteValidator = require('../../validators/invites')

module.exports = spaceRouter

function spaceRouter (api) {
  const router = express.Router()
  const validators = {
    invites: inviteValidator(api),
    spaces: spaceValidator(api),
    drive: driveValidator(api),
    mounts: mountValidator(api)
  }

  /**
   * @openapi
   * /api/spaces:
   *   get:
   *     description: list your spaces
   *     summary: list your spaces
   *     responses:
   *       200:
   *         description: returns a list of spaces
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   address:
   *                     type: string
   */
  router.get('/', toHTTP(api.get('controllers.spaces'), 'list'))
  /**
   * @openapi
   * /api/spaces:
   *   post:
   *     description: create a new space or join using an invite code
   *     summary: create a new space or join using an invite code
   *     requestBody:
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: cobox-project
   *                 required: true
   *               address:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *               encryptionKey:
   *                 type: string
   *                 example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *               code:
   *                 type: string
   *                 example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *     responses:
   *       200:
   *         description: creates an space
   *       500:
   *         description: internal server error
   */
  router.post('/', validators.spaces.create, toHTTP(api.get('controllers.spaces'), 'create'))
  /**
   * @openapi
   * /api/spaces/connections:
   *   post:
   *     description: returns all swarming spaces
   *     summary: begin replication on all spaces
   *     responses:
   *       200:
   *         description: success to start swarming
   *       500:
   *         description: failed to start swarming
   */
  router.post('/connections', toHTTP(api.get('controllers.spaces.connections'), 'createAll'))
  /**
   * @openapi
   * /api/spaces/connections:
   *   delete:
   *     description: returns all unswarming spaces
   *     summary: stop replication on all spaces
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       500:
   *         description: failed to stop swarming
   */
  router.delete('/connections', toHTTP(api.get('controllers.spaces.connections'), 'destroyAll'))
  /**
   * @openapi
   * /api/spaces/mounts:
   *   get:
   *     description: list all mounted spaces
   *     summary: list all mounted spaces
   *     responses:
   *       200:
   *         description: success
   *       500:
   *         description: internal server error
   */
  router.get('/mounts', toHTTP(api.get('controllers.mounts'), 'list'))
  /**
   * @openapi
   * /api/spaces/mounts:
   *   post:
   *     description: mount all spaces
   *     summary: mount all spaces
   *     responses:
   *       200:
   *         description: success
   *       500:
   *         description: internal server error
   */
  router.post('/mounts', validators.mounts.create, toHTTP(api.get('controllers.mounts'), 'createAll'))
  /**
   * @openapi
   * /api/spaces/mounts:
   *   delete:
   *     description: unmount all spaces drives
   *     summary: unmount all spaces drives
   *     responses:
   *       200:
   *         description: successfully mounted filesystems
   *       500:
   *         description: failed to mount filesystems
   */
  router.delete('/mounts', toHTTP(api.get('controllers.mounts'), 'destroyAll'))
  /**
   * @openapi
   * /api/spaces/invites/accept:
   *   get:
   *     description: join a space using an invite code
   *     summary: join a space using an invite code
   *     responses:
   *       200:
   *         description: returns an space
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                   example: cobox-project
   *                 address:
   *                   type: string
   *                   example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: code
   *       in: query
   *       description: invite code for the space
   *       example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *       required: true
   */
  router.get('/invites/accept', validators.invites.accept, toHTTP(api.get('controllers.spaces.invites'), 'accept'))
  /**
   * @openapi
   * /api/spaces/{id}:
   *   patch:
   *     description: update a space's settings
   *     summary: change a space's settings
   *     responses:
   *       200:
   *         description: returns the new settings
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 threshold:
   *                   type: number
   *                   example: 1
   *                 address:
   *                   type: number
   *                   example: 86400000
   *       402:
   *         description: forbidden
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.patch('/:id', toHTTP(api.get('controllers.spaces'), 'update'))
  /**
   * @openapi
   * /api/spaces/{id}:
   *   delete:
   *     description: destroy a space
   *     summary: completely destroy a space locally, wiping all local data
   *     responses:
   *       200:
   *         description: returns the destroyed space
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                   example: cobox-project
   *                 address:
   *                   type: string
   *                   example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *       402:
   *         description: forbidden
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.delete('/:id', toHTTP(api.get('controllers.spaces'), 'destroy'))
  /**
   * @openapi
   * /api/spaces/{id}/peers:
   *   get:
   *     description: get all space peers
   *     summary: get all known space members
   *     responses:
   *       200:
   *         description: returns a list of space members
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   name:
   *                     type: string
   *                   publicKey:
   *                     type: string
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.get('/:id/peers', toHTTP(api.get('controllers.spaces.peers'), 'list'))
  /**
   * @openapi
   * /api/spaces/{id}/last-sync:
   *   get:
   *     description: get all last sync times with remote peers
   *     summary: get all last sync times with remote peers
   *     responses:
   *       200:
   *         description: returns an object of peers
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.get('/:id/last-sync', toHTTP(api.get('controllers.spaces.peers'), 'lastSync'))
  /**
   * @openapi
   * /api/spaces/{id}/connections:
   *   post:
   *     description: returns a swarming space
   *     summary: begin replication with remote peers
   *     responses:
   *       200:
   *         description: success to start swarming
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.post('/:id/connections', toHTTP(api.get('controllers.spaces.connections'), 'create'))
  /**
   * @openapi
   * /api/spaces/{id}/connections:
   *   delete:
   *     description: returns an unswarming space
   *     summary: stop replication with remote peers
   *     responses:
   *       200:
   *         description: success to stop swarming
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.delete('/:id/connections', toHTTP(api.get('controllers.spaces.connections'), 'destroy'))
  /**
   * @openapi
   * /api/spaces/{id}/invites:
   *   get:
   *     description: get all invites to this space
   *     summary: get all invites for new members to this space
   *     responses:
   *       200:
   *         description: returns a list of identities
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   type:
   *                     type: string
   *                     pattern: ^peer/about$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                       publicKey:
   *                         type: string
   *                         example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.get('/:id/invites', toHTTP(api.get('controllers.spaces.invites'), 'list'))
  /**
   * @openapi
   * /api/spaces/{id}/invites:
   *   post:
   *     description: generate an invite code to a space
   *     summary: generate an invite code to a space
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               publicKey:
   *                 type: string
   *                 example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *     responses:
   *       200:
   *         description: returns an invite
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                   type:
   *                     type: string
   *                     pattern: ^peer/about$
   *                   version:
   *                     type: string
   *                     pattern: ^1.0.0$
   *                   author:
   *                     type: string
   *                     example: 6b80a07b87a652ef8d8cee8025bbd3adb1c7738de28fbbfa08a955969972df93
   *                   timestamp:
   *                     type: number
   *                     example: 1626864582706
   *                   content:
   *                     type: object
   *                     properties:
   *                       publicKey:
   *                         type: string
   *                         example: 1166e6251315f59e3028b4459b9d93c9229ce62038ece703baf1900a9fa63352
   *                       code:
   *                         type: string
   *                         example: dc9ec850387bd699ace4d2d6c00f9a36f63fb19bf917c3797cb5595ece38e3bc813ef2c43e21529641d21e14d05ab71ae90aaab925a853592e5f456a8a8034b9615cb79d70a19dd05e08da1a03457c17e9963eb1793e28a9c89bf65d229abb4d4aae0e9270d3921d1645e1f68763aa3ece1f8804c461df6829d23f465f23de457b857280ce298c174a08c65d8d
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.post('/:id/invites', validators.invites.create, toHTTP(api.get('controllers.spaces.invites'), 'create'))
  /**
   * @openapi
   * /api/spaces/{id}/mounts:
   *   post:
   *     description: mount a space drive
   *     summary: mount a space drive
   *     responses:
   *       200:
   *         description: fuse successfuly mounted
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: failed to mount filesystem
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.post('/:id/mounts', validators.mounts.create, toHTTP(api.get('controllers.mounts'), 'create'))
  /**
   * @openapi
   * /api/spaces/{id}/mounts:
   *   delete:
   *     description: unmounts a space drive
   *     summary: unmounts a space drive
   *     responses:
   *       200:
   *         description: successfully unmounted filesystem
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: failed to unmount filesystem
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.delete('/:id/mounts', toHTTP(api.get('controllers.mounts'), 'destroy'))
  /**
   * @openapi
   * /api/spaces/{id}/drive:
   *   get:
   *     description: open a drive using your default file explorer
   *     summary: open a drive using your default file explorer
   *     responses:
   *       200:
   *         description: opens the file explorer
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.get('/:id/drive', toHTTP(api.get('controllers.drive'), 'open'))
  /**
   * @openapi
   * /api/spaces/{id}/drive/history:
   *   get:
   *     description: fetch the history of the drive
   *     summary: fetch the history of the drive
   *     responses:
   *       200:
   *         description: returns the history of the drive
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               items:
   *                 type: object
   *                 properties:
   *                   filename:
   *                     type: string
   *                   id:
   *                     type:string
   *                   links:
   *                     type: array
   *                     items:
   *                       type: string
   *                   metadata:
   *                     type: object
   *                     properties:
   *                       id:
   *                         type: string
   *                       seq:
   *                         type: number
   *                   content:
   *                     type: object
   *                     properties:
   *                       id:
   *                         type: string
   *                       seq:
   *                         type: number
   *       402:
   *         description: forbidden
   *       404:
   *         description: space doesn't exist
   *       500:
   *         description: internal server error
   *   parameters:
   *     - name: id
   *       in: path
   *       description: address of the space
   *       required: true
   */
  router.get('/:id/drive/history', validators.drive.history, toHTTP(api.get('controllers.drive'), 'history'))
  router.get('/:id/drive/stat', toHTTP(api.get('controllers.drive'), 'stat'))
  router.get('/:id/drive/readdir', toHTTP(api.get('controllers.drive'), 'readdir'))

  return router
}
