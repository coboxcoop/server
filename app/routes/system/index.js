const express = require('express')
const toHTTP = require('../../middleware/to-http')
const getCheckAuth = require('../../middleware/check-auth')

module.exports = systemRouter

function systemRouter (api) {
  const router = express.Router()
  const checkAuth = getCheckAuth(api)

  /**
   * @openapi
   * /api/system/stop:
   *   get:
   *     description: stop the server
   *     summary: stop the server
   *     responses:
   *       200:
   *         description: stopped the server
   *       500:
   *         description: internal server error
   */
  router.get('/stop', toHTTP(api.get('controllers.system'), 'stop'))
  /**
   * @openapi
   * /api/system:
   *   get:
   *     description: get system information
   *     summary: get system information
   *     responses:
   *       200:
   *         description: returns an identity
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 name:
   *                   type: string
   *                 description:
   *                   type: string
   *                 version:
   *                   type: string
   *                 author:
   *                   type: string
   *                 license:
   *                   type: string
   *                 bugs:
   *                   type: object
   *       403:
   *         description: unauthorized
   *       404:
   *         description: identity doesn't exist
   *       500:
   *         description: internal server error
   */
  router.get('/', toHTTP(api.get('controllers.system'), 'show'))
  /**
   * @openapi
   * /api/system/stop:
   *   get:
   *     description: stop the server
   *     summary: stop the server
   *     responses:
   *       200:
   *         description: stopped the server
   *       500:
   *         description: internal server error
   */
  // router.get('/stop', toHTTP(api.get('controllers.system'), 'stop'))
  router.get('/logs', checkAuth, toHTTP(api.get('controllers.system'), 'logs', { stream: true, filename: 'cobox.log' }))
  /**
   * @openapi
   * /api/system/routes:
   *   get:
   *     description: get the application's routes
   *     summary: get the application's routes
   *     responses:
   *       200:
   *         description: list the routes
   *       500:
   *         description: internal server error
   */
  router.get('/routes', toHTTP(api.get('controllers.system'), 'routes'))

  return router
}
