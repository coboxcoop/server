const { promises: fs } = require('fs')
const path = require('path')

module.exports = setupPasswordHashExists

function setupPasswordHashExists (api) {
  /*
   * checks to see if a password_hash file exists
   * @returns {boolean}
   */
  return async function passwordHashExists () {
    const { storage } = api.get('app.settings')
    try {
      await fs.access(path.join(storage, 'password_hash'))
      return true
    } catch (err) {
      return false
    }
  }
}
