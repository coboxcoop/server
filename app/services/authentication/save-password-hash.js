const bcrypt = require('bcrypt')
const path = require('path')
const { promises: fs, constants } = require('fs')

// keys / passwords etc should be readonly files, so we ensure using these file permissions
const opts = { mode: constants.S_IRUSR }
// 15 salt rounds approximates to one hash every 3 seconds - this makes brute forcing the password very slow
const saltRounds = 15

module.exports = setupSavePasswordHash

function setupSavePasswordHash (api) {
  /**
   * @param {string} rootStorage - the root storage location of the app
   * @param {string} password - the password for encrypting storage
   * @throws {Error} - unable to save the password hash
   */
  return async function savePasswordHash (password) {
    const { storage } = api.get('app.settings')
    // generate the password hash
    const passwordHash = await bcrypt.hash(password, saltRounds)
    // then save the hash, read only permissions
    try {
      await fs.writeFile(path.join(storage, 'password_hash'), Buffer.from(passwordHash, 'utf-8'), opts)
    } catch (err) {
      throw new Error("failed to save 'password_hash', does one already exist? if not, check your file system permissions")
    }
  }
}
