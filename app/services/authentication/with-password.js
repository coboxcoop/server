const assert = require('assert')
const bcrypt = require('bcrypt')
const path = require('path')
const { promises: fs } = require('fs')

const NotFound = require('../../errors/not-found')
const Unauthorized = require('../../errors/unauthorized')

module.exports = setupAuthenticateWithPassword

function setupAuthenticateWithPassword (api) {
  /**
   * @param {string} passwordAttempt - the password attempt
   * @throws {NotFound} - password missing
   * @throws {NotFound} - password hash does not exist on file system
   * @throws {Forbidden} - incorrect password
   */
  return async function authenticateWithPassword (passwordAttempt) {
    assert.ok(passwordAttempt, new NotFound('invalid: missing password attempt'))
    // load the salt and password_hash from disk
    const { storage } = api.get('app.settings')
    let hash
    try {
      hash = await fs.readFile(path.join(storage, 'password_hash'), 'utf-8')
    } catch (err) {
      throw new NotFound(`'password_hash' does not exist, have you registered?`)
    }
    // now hash the password attempt and see if it matches the hash
    const result = await bcrypt.compare(passwordAttempt, hash)
    if (!result) throw new Unauthorized('incorrect password')
    return true
  }
}
