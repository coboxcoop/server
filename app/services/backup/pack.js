const { promises: fs } = require('fs')
const path = require('path')
const pumpify = require('pumpify')
const tar = require('tar-stream')
const zlib = require('zlib')

const { shortHex, hex } = require('../../../util')
const SpaceDecorator = require('../../decorators/space')
const VERSION = require('../../../version')
const { assign } = Object

module.exports = setupPack

/**
 * pack compiles your parent key, your password,
 * and all your space addresses and encryption keys
 * into a single zlib'ed tarball file called backup.cobox
 *
 * @param {Map} api
 * @returns {function}
 */
function setupPack (api) {
  /**
   * @param {Object} params
   * @param {string} params.password
   * @returns {Readable}
   */
  return async function pack (params) {
    const identitydb = api.get('db.identities')
    const settings = api.get('app.settings')
    const identities = api.get('cache.identities')
    const spaces = api.get('cache.spaces')
    const replicators = api.get('cache.replicators')
    const seeders = api.get('cache.seeders')
    const getParentKey = api.get('services.encryption.get-parent-key')

    const pack = tar.pack()
    // setup an object to tarball
    const data = {
      settings: settings,
      parentKey: hex(getParentKey(params.password)),
      spaces: {},
      replicators: {},
      seeders: {},
      identities: {}
    }
    // get all spaces, load their encryption keys, and bundle
    for await (const [reference, space] of spaces) {
      const encryptionKey = await fs.readFile(path.join(space.storage, 'encryption_key'))
      data.spaces[reference] = {
        name: space.name,
        address: space.address,
        encryptionKey: hex(encryptionKey)
      }
    }
    for await (const [reference, seeder] of seeders) {
      const encryptionKey = await fs.readFile(path.join(seeder.storage, 'encryption_key'))
      data.seeders[reference] = {
        name: seeder.name,
        address: seeder.address,
        encryptionKey: hex(encryptionKey)
      }
    }
    // we dont have encryption keys for replicators, we just want names and addresses
    for await (const [reference, replicator] of replicators) {
      data.replicators[reference] = {
        name: replicator.name,
        address: replicator.address,
      }
    }
    // we only need the identity seed, which is in the identitydb
    for await (const { key: address, value: identity } of identitydb.createReadStream()) {
      data.identities[shortHex(address)] = identity
    }

    await addEntry(pack, { name: 'VERSION' }, VERSION)
    await addEntry(pack, { name: 'backup.cobox.json' }, JSON.stringify(data, null, 2))

    pack.finalize()

    // TODO: PROTECT THE BACKUP FILE USING YOUR PASSWORD
    return pumpify(pack, zlib.createGzip())
  }
}

function addEntry (packer, details, entry) {
  return new Promise((resolve, reject) => {
    packer.entry(details, entry, (err) => {
      if (err) return reject(err)
      resolve(err)
    })
  })
}
