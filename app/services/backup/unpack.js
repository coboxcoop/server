const fs = require('fs')
const concat = require('concat-stream')
const tar = require('tar-stream')
const zlib = require('zlib')
const pumpify = require('pumpify')

module.exports = setupUnpack

/**
 * unpack extracts your parent key, identities
 * and all your space addresses and encryption keys
 * into a single zlib'ed tarball file called backup.cobox
 *
 * ATTENTION: merging of 'accounts' is not possible due to
 * conflicts in key derivation due to having two parent keys
 * therefore at this stage, an unpack action can only be 
 * performed before registration, once an account has been
 * created an a parent key generated, import will be disabled
 *
 * @param {Map} api
 * @returns {function}
 */
function setupUnpack (api) {
  return function unpack (file, password) {
    const saveEncryptedParentKey = api.get('services.encryption.save-parent-key')
    const savePasswordHash = api.get('services.authentication.save-password-hash')
    const identitydb = api.get('db.identities')
    const spacedb = api.get('db.spaces')
    const replicatordb = api.get('db.replicators')
    const seederdb = api.get('db.seeders')

    return new Promise((resolve, reject) => {
      const files = {}
      const extract = tar.extract()
      extract.on('entry', onEntry)
      extract.on('finish', done)
      pumpify(fs.createReadStream(file), zlib.createUnzip(), extract)

      function onEntry (header, stream, next) {
        stream.pipe(concat((contents) => {
          files[header.name] = JSON.parse(contents.toString('utf-8'))
          return next()
        }))
      }

      function done () {
        // TODO: set app storage location
        const details = files['backup.cobox.json']
        const proms = []
        proms.push(savePasswordHash(password))
        proms.push(saveEncryptedParentKey(Buffer.from(details.parentKey, 'hex'), password))
        for (const [shortPublicKey, identity] of Object.entries(details.identities)) {
          proms.push(identitydb.put(shortPublicKey, identity))
        }
        for (const [reference, params] of Object.entries(details.spaces)) {
          proms.push(spacedb.put(reference, params))
        }
        for (const [reference, params] of Object.entries(details.replicators)) {
          proms.push(replicatordb.put(reference, params))
        }
        for (const [reference, params] of Object.entries(details.seeders)) {
          proms.push(seederdb.put(reference, params))
        }
        return Promise
          .all(proms)
          .then(resolve)
          .catch(reject)
      }
    })
  }
}
