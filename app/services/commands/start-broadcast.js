const assert = require('assert')
const { encodings, validators } = require('@coboxcoop/schemas')
const Announce = encodings.command.announce
const isAnnounce = validators.command.announce

/* load jsdoc type definitions */
require('./../../types')

module.exports = setupStartBroadcast

function setupStartBroadcast (api) {
  /**
   * creates a command and publishes to the seeders log
   * when read by the seeder, will implement and start
   * broadcasting UDP packets
   *
   * @param {Seeder} seeder - the seeder to communicate with
   * @param {Object} params - a parameters object
   * @param {Identity} identity - the identity of the admin
   */
  return async function startBroadcast (seeder, params, identity) {
    const command = {
      type: 'command/announce',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey
    }

    assert(isAnnounce(command), 'invalid parameters')

    await seeder.log.publish(command, { valueEncoding: Announce })

    return command
  }
}
