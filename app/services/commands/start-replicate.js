const assert = require('assert')
const { encodings, validators } = require('@coboxcoop/schemas')
const resourceParams = require('../../helpers/resource-params')
const Replicate = encodings.command.replicate
const isReplicate = validators.command.replicate

/* load jsdoc type definitions */
require('./../../types')

module.exports = setupStartReplicate

function setupStartReplicate (api) {
  /**
   * creates a command and publishes to the seeders log
   * when read by the seeder, will save a replicator
   * with the provided name and address, and start replicating
   *
   * @param {Seeder} seeder - the seeder to communicate with
   * @param {Object} params - a parameters object
   * @param {Identity} identity - the identity of the admin
   */
  return async function startReplicate (seeder, params, identity) {
    const content = resourceParams(params)
    assert.ok(identity.publicKey, 'identity.publicKey required')

    let command = {
      type: 'command/replicate',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content
    }

    assert(isReplicate(command), 'invalid parameters')

    await seeder.log.publish(command, { valueEncoding: Replicate })

    return command
  }
}
