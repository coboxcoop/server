const assert = require('assert')
const { encodings, validators } = require('@coboxcoop/schemas')
const Hide = encodings.command.hide
const isHide = validators.command.hide

/* load jsdoc type definitions */
require('./../../types')

module.exports = setupStopBroadcast

function setupStopBroadcast (api) {
  /**
   * creates a command and publishes to the seeders log
   * when read by the seeder, will implement and stop
   * broadcasting UDP packets
   *
   * @param {Seeder} seeder - the seeder to communicate with
   * @param {Object} params - a parameters object
   * @param {Identity} identity - the identity of the admin
   */
  return async function stopBroadcast (seeder, params, identity) {
    const command = {
      type: 'command/hide',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey
    }

    assert(isHide(command), 'invalid parameters')

    await seeder.log.publish(command, { valueEncoding: Hide })

    return command
  }
}
