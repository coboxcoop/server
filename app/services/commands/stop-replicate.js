const assert = require('assert')
const { encodings, validators } = require('@coboxcoop/schemas')
const resourceParams = require('../../helpers/resource-params')
const Unreplicate = encodings.command.unreplicate
const isUnreplicate = validators.command.unreplicate

/* load jsdoc type definitions */
require('./../../types')

module.exports = setupStopReplicate

function setupStopReplicate (api) {
  /**
   * creates a command and publishes to the seeders log
   * when read by the seeder, will stop replicating
   * on a cached replicator with the provided name and address
   *
   * @param {Seeder} seeder - the seeder to communicate with
   * @param {Object} params - a parameters object
   * @param {Identity} identity - the identity of the admin
   */
  return async function stopReplicate (seeder, params, identity) {
    const content = resourceParams(params)

    let command = {
      type: 'command/unreplicate',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content
    }

    assert(isUnreplicate(command), 'invalid parameters')

    await seeder.log.publish(command, { valueEncoding: Unreplicate })

    return command
  }
}
