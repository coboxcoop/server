const sodium = require('sodium-native')

module.exports = setupDeriveKeyPair

/* load jsdoc type definitions */
require('./../../types')

function setupDeriveKeyPair (api) {
  /**
   * caches the password in the function's closure, which
   * can be used to decrypt the parent key in order to
   * derive new keypairs for identities, spaces, etc
   *
   * @param {string} rootStorage - the storage location of the parent key
   * @param {function} keyPairFn - a keypair function which takes a parent key, an id and a context string
   * @param {string} password - the password used to decrypt the parent key
   * @returns {function}
   */
  return function getKeyPairDeriver (keyPairFn, password) {
    const getParentKey = api.get('services.encryption.get-parent-key')
    /**
     * @param {number} id - a numerical identifier of the key
     * @param {string|Buffer} context - additional unique context for the keypair
     * @returns {KeyPair}
     */
    return function deriveKeyPair (id, context) {
      const parentKey = getParentKey(password)
      const keyPair = keyPairFn(parentKey, id, context)
      sodium.sodium_memzero(parentKey)
      return keyPair
    }
  }
}
