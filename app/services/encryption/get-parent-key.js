const sodium = require('sodium-native')
const fs = require('fs')
const path = require('path')

module.exports = setupGetParentKey

function setupGetParentKey (api) {
  /**
   * fetch the parent key by producing a hash of the password
   * we hash it to 32 bytes long (required for encryption),
   * then decrypt the encryption key using the hash, xor'ing
   * the encrypted parent key with its own encryption key
   * then zero the unused buffers
   *
   * possible security vulnerability: we already have a bcrypt password
   * hash saved to disk, here we use a different hash algorithm, but perhaps
   * there might be a way to compare hashes and decipher the password...
   *
   * @param {string} password - the password used to decrypt the parent key
   * @returns {Buffer}
   */
  return function getParentKey (password) {
    // make sure password is a buffer
    if (!Buffer.isBuffer(password)) password = Buffer.from(password)
    const { storage } = api.get('app.settings')
    // password needs to be 32 bytes to be a valid encryption key, so we'll hash it
    const passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, password)
    // now we can load our encrypted parent key from disk
    const encParentKey = fs.readFileSync(path.join(storage, 'encrypted_parent_key'))
    // we allocate a buffer for our loaded parent key
    const parentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    // use a zero nonce
    const nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    // decrypt the key
    sodium.crypto_stream_xor(parentKey, encParentKey, nonce, passwordHash)
    // zero the other buffers
    sodium.sodium_memzero(encParentKey)
    sodium.sodium_memzero(passwordHash)
    // and return the key
    return parentKey
  }
}
