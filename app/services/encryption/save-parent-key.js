const sodium = require('sodium-native')
const { promises: fs } = require('fs')
const { constants } = require('fs')
const path = require('path')

const opts = { mode: constants.S_IRUSR }

module.exports = setupSaveParentKey

function setupSaveParentKey (api) {
  /**
   * encrypt and save the parent key to the specified storage location
   *
   * TODO: currently no way of verifying if the parent key has been decrypted successfully
   * we could hash it and concatenate the hash to the end of the key, and use that for verification
   *
   * @param {string} rootStorage - the root storage of the app for saving the parent key
   * @param {Buffer} parentKey - the unencrypted parent key
   * @param {string} password - the password used to encrypt the parent key
   */
  return async function saveParentKey (parentKey, password) {
    // make sure password is a buffer
    if (!Buffer.isBuffer(password)) password = Buffer.from(password)
    const { storage } = api.get('app.settings')
    // it needs to be 32 bytes to be a valid encryption key, so we'll hash it
    const passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, password)
    // now we need a buffer to populated with our encrypted parent key
    const encParentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    // we'll just use a zero nonce for both encryption and decryption
    const nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    // do the encryption
    sodium.crypto_stream_xor(encParentKey, parentKey, nonce, passwordHash)
    // then write the file to disk
    await fs.writeFile(path.join(storage, 'encrypted_parent_key'), encParentKey, opts)
    // finally, zero all the buffers
    sodium.sodium_memzero(encParentKey)
    sodium.sodium_memzero(parentKey)
    sodium.sodium_memzero(passwordHash)
  }
}
