const PeerInvite = require('@coboxcoop/schemas').encodings.peer.invite
const { hex } = require('../../../util')

/* load jsdoc type definitions */
require('./../../types')

module.exports = setupCreateInvite

function setupCreateInvite (api) {
  /**
   * @param {Space} entry - a space / seeder / replicator
   * @param {Resource} params - parameters to generate the invite code
   * @param {Identity} identity - identity creating the invite
   * @returns {Invite}
   */
  return async function createInvite (resource, params, identity) {
    const pack = api.get('services.invites.pack')
    const code = pack(params)

    const invite = {
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content: {
        publicKey: hex(params.publicKey)
      }
    }

    await resource.log.publish(invite, { valueEncoding: PeerInvite })

    invite.content.code = hex(code)

    return invite
  }
}
