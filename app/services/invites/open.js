const Forbidden = require('../../errors/forbidden')

/* load jsdoc type definitions */
require('./../../types')

module.exports = setup

function setup (api) {
  /**
   *
   * @param {string} code - an invite code
   * @param {Identity} identity - identity creating the invite
   * @throws {Forbidden}
   * @returns {Resource}
   */
  return function openInvite (code, identity) {
    const unpack = api.get('services.invites.unpack')
    try {
      return unpack(Buffer.from(code, 'hex'), identity)
    } catch (err) {
      throw new Forbidden('invalid invite code')
    }
  }
}
