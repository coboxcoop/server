const assert = require('assert')
const Forbidden = require('../errors/forbidden')
/* load jsdoc type definitions */
require('../types')

module.exports = setupLock

/**
 * after deleting a session and 'unauthenticating',
 * we need to close all open spaces gracefully,
 * and delete everything left in our cache, so that
 * we have a fresh starting point if we create
 * a new session
 *
 * @param {Map} api - dependency container
 */
function setupLock (api) {
  return async function lock () {
    const app = api.get('app.express')
    assert(!app.get('lock'), new Forbidden('cobox is already locked'))

    const identities = api.get('cache.identities')
    const teardownSpaces = api.get('teardown.spaces')
    const teardownReplicators = api.get('teardown.replicators')
    const teardownSeeders = api.get('teardown.seeders')
    const teardownCorestore = api.get('teardown.corestore')
    const teardownMount = api.get('teardown.mount')

    // TODO: move setup event store to unlock
    // and destroy the stream on lock

    await Promise.all([
      teardownSpaces(),
      teardownReplicators(),
      teardownSeeders()
    ])

    identities.clear()
    await teardownCorestore()
    await teardownMount()
    api.set('setup.signing-key-pair', null)
    api.set('setup.encryption-key-pair', null)
    app.set('lock', true)
    return true
  }
}
