const fs = require('fs')
const path = require('path')

/* load jsdoc type definitions */
require('../types')

module.exports = setup

/**
 * start up the application, initialize the event store
 * and web server, then call ready on the controllers
 * available WITHOUT authentication, to initialize
 * their dependencies properly
 *
 * @param {Map} api - dependency container
 */
function setup (api) {
  return async function setup () {
    const settings = api.get('app.settings')
    const logfile = path.join(settings.logs, 'main.log')
    try {
      await access(logfile)
    } catch (err) {
      const fd = await open(logfile, 'a')
      await close(fd)
    }
    const setupEventStore = api.get('setup.events')
    const setupUnauthControllers = api.get('setup.unauth-controllers')
    await setupUnauthControllers()
    setupEventStore()
  }
}

function access (filename) {
  return new Promise((resolve, reject) => {
    fs.access(filename, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}

function open (filename, flags) {
  return new Promise((resolve, reject) => {
    fs.open(filename, flags, (err, fd) => {
      if (err) return reject(err)
      resolve(fd)
    })
  })
}

function close (fd) {
  return new Promise((resolve, reject) => {
    fs.close(fd, (err) => {
      if (err) return reject(err)
      resolve()
    })
  })
}
