module.exports = setup

function setup (api) {
  return async function setupAuthenticatedControllers () {
    return Promise.all([
      api.get('controllers.admin.seeders').ready(),
      api.get('controllers.admin.seeders.invites').ready(),
      api.get('controllers.admin.seeders.connections').ready(),
      api.get('controllers.admin.seeders.peers').ready(),
      api.get('controllers.commands.broadcast').ready(),
      api.get('controllers.commands.replicate').ready(),
      api.get('controllers.drive').ready(),
      api.get('controllers.events').ready(),
      api.get('controllers.export').ready(),
      api.get('controllers.identities').ready(),
      api.get('controllers.import').ready(),
      api.get('controllers.mounts').ready(),
      api.get('controllers.peers').ready(),
      api.get('controllers.replicators').ready(),
      api.get('controllers.replicators.connections').ready(),
      api.get('controllers.spaces').ready(),
      api.get('controllers.spaces.invites').ready(),
      api.get('controllers.spaces.connections').ready(),
      api.get('controllers.spaces.peers').ready()
    ])
  }
}
