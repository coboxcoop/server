const path = require('path')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:setup/corestore')
const Corestore = require('corestore')
const CoBoxNetworker = require('@coboxcoop/networker')

module.exports = setup

function setup (api) {
  /**
   * register corestore and networker and make ready
   */
  return async function setupCorestore (parentKey, identity) {
    const { storage } = api.get('app.settings')
    const peerdb = api.get('db.peers')
    // setup hypercore storage using a single corestore
    const corestore = new Corestore(path.join(storage, 'store'), { masterKey: parentKey })
    await corestore.ready()
    // start swarming on the DHT
    // TODO: ensure identity here is buffers
    // we should refactor setupNetworker into a separate function,
    // then when an identity is changed, we close the existing networker
    // and generate a new one, a better way would be to have a hook
    // on the networker to fetch the current identity

    // TODO make sure the identity is the same as the one published to the space log
    const networker = new CoBoxNetworker(corestore, {
      publicKey: Buffer.from(identity.publicKey, 'hex'),
      secretKey: Buffer.from(identity.secretKey, 'hex'),
      name: identity.name
    }, peerdb)

    await networker.listen()
    // cache and make available in controllers
    api.set('dat.store', corestore)
    api.set('dat.networker', networker)

    debug({ msg: 'corestore ready, networker listening' })
  }
}
