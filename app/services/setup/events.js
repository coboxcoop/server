const debug = require('@coboxcoop/logger')('@coboxcoop/server:services/events')
const { Transform } = require('stream')
const SpaceDecorator = require('../../decorators/space')
const { isProduction } = require('../../../util')

/* load jsdoc type definitions */
require('./../../types')

module.exports = setup

/**
 * when new spaces or seeders are added to their cache,
 * they are emitted, here we cache them and open new
 * streams of their log contents which are merged with
 * our event store. the event store is made available
 * over a web socket (see: app/controllers/events.js)
 *
 * @param {Map} api - dependency container
 */
function setup (api) {
  return function eventStore () {
    const eventStream = api.get('app.events')
    const spaces = api.get('cache.spaces')
    const replicators = api.get('cache.replicators')
    const seeders = api.get('cache.seeders')
    const peerdb = api.get('db.peers')

    // pipe out any new peers that arrive in peerdb as type 'peer/connection'
    eventStream.add(peerdb
      .createLiveStream({ old: false })
      .pipe(typify('peer/connection'))
      .pipe(log()))

    // whenever new spaces are added, emit an event and add their sources to the event store
    // TODO: refactor these streams into a single merge stream in the replicator / space / superuser class
    spaces.on('add', (address, space) => {
      eventStream.add(space.createLogStream().pipe(log()))
      eventStream.add(space.createLastSyncStream({ old: false }).pipe(log()))
    })

    replicators.on('add', (address, replicator) => {
      eventStream.add(replicator.createLastSyncStream({ old: false }).pipe(log()))
    })

    // the same goes for seeders
    seeders.on('add', (address, seeder) => {
      eventStream.add(seeder.createLogStream().pipe(log()))
      eventStream.add(seeder.createLastSyncStream({ old: false }).pipe(log()))
    })
  }
}

function log () {
  return new Transform({
    objectMode: true,
    transform: function (entry, _, next) {
      if (!isProduction()) debug(JSON.stringify(entry))
      next(null, entry)
    }
  })
}

function typify (type) {
  return new Transform({
    objectMode: true,
    transform: function (entry, _, next) {
      next(null, {
        type,
        ...entry.value
      })
    }
  })
}
