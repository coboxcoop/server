const { assign } = Object

/* load jsdoc type definitions */
require('./../../types')

module.exports = fetchRoutes

/**
 * recursively decomposes all express routers
 * and builds an array of end-points used by
 * the entire application, is made available
 * over GET /api/system/routes (see: app/controllers/system.js)
 *
 * @param {Map} api - dependency container
 */
function fetchRoutes (api) {
  /**
   * @returns {Array.<Route>}
   */
  return function () {
    const router = api.get('app.router')

    return []
      .concat
      .apply([], router.stack.map((layer) => recurse(layer)))
      .filter(Boolean)
      .map((route) => assign({} , route, {
        path: `/api${route.path}`
      }))

    function recurse (layer) {
      if (!layer.route) {
        const match = layer.regexp.toString().match(/\/\^\\(\/\w+)/)
        if (!match) return
        const path = match[1]
        const stack = layer.handle.stack
        if (!stack) return []
        return [].concat.apply([], stack.map((l) => {
          return recurse(l)
            .map((route) => {
              route.path = path.concat(route.path)
              return route
            })
        }))
      } else {
        var path = layer.route.path
        if (!path) return
        return layer.route.stack.reduce((acc, l) => {
          acc.push({ path, method: l.method })
          return acc
        }, [])
      }
    }
  }
}
