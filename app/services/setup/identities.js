module.exports = setup

function setup (api) {
  /**
   * identities are generated on the fly, if any exist then we
   * reload the keypair deterministically using libsodium's key derivation
   * function, we also attach to each one a list of addresses which we will
   * populate so each identity keeps track of its own spaces, etc
   *
   * @returns {Identity}
   */
  return async function loadIdentities () {
    const proms = []
    const identitydb = api.get('db.identities')
    const factory = api.get('factory.identities')
    const stream = identitydb.createReadStream()
    await factory.ready()

    for await (const entry of stream) {
      proms.push(factory.createIdentity(entry.value, { save: false }))
    }

    await Promise.all(proms)
    return findOrCreateIdentity()
  }

  /**
   * construct a new identity (if none exists),
   * this will only execute when the app is loaded
   * for the first time
   * @returns {Identity}
   */
  async function findOrCreateIdentity () {
    const identities = api.get('cache.identities')
    if (identities.size > 0) return identities.values().next().value
    const factory = api.get('factory.identities')
    return await factory.createIdentity()
  }
}
