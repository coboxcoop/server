module.exports = setup

function setup (api) {
  /**
   * load and cache replicators
   * - have no encryption key
   * - contain at max 3 read-only hypercores (metadata, content, log) per peer
   * - none are writable
   */
  return async function loadReplicators () {
    const proms = []
    const replicatordb = api.get('db.replicators')
    const factory = api.get('factory.replicators')
    const identities = api.get('cache.identities')
    await factory.ready()

    for await (const entry of replicatordb.createReadStream()) {
      const [ shortPublicKey, shortAddress ] = entry.key.split('/')
      const identity = identities.get(shortPublicKey)
      proms.push(factory.createReplicator({ ...entry.value, identity }, { save: false }))
    }

    return Promise.all(proms)
  }
}
