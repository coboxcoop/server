module.exports = setup

function setup (api) {
  /**
   * load and cache seeders
   * - share an encryption key
   * - contain a single hypercore per peer
   * - only one core is writable
   */
  return async function loadSeeders () {
    const proms = []
    const seederdb = api.get('db.seeders')
    const factory = api.get('factory.seeders')
    const identities = api.get('cache.identities')
    await factory.ready()

    for await (const entry of seederdb.createReadStream()) {
      const [ shortPublicKey, shortAddress ] = entry.key.split('/')
      const identity = identities.get(shortPublicKey)
      proms.push(factory.createSeeder({ ...entry.value, identity }, { save: false }))
    }

    return Promise.all(proms)
  }
}
