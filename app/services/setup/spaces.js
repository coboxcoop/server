module.exports = setup

function setup (api) {
  /**
   * load and cache spaces
   * - share an encryption key
   * - contain 3 cores per peer (metadata, content, log)
   * - a hyperdrive plus an additional hypercore
   * - only 3 are writable
   */
  return async function loadSpaces () {
    const proms = []
    const spacedb = api.get('db.spaces')
    const factory = api.get('factory.spaces')
    const identities = api.get('cache.identities')
    await factory.ready()

    for await (const entry of spacedb.createReadStream()) {
      const [ shortPublicKey, shortAddress ] = entry.key.split('/')
      const identity = identities.get(shortPublicKey)
      proms.push(factory.createSpace({ ...entry.value, identity }, { save: false }))
    }

    return Promise.all(proms)
  }
}
