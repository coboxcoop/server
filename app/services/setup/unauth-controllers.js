module.exports = setup

function setup (api) {
  return async function setupUnauthenticatedControllers () {
    return Promise.all([
      api.get('controllers.system').ready(),
      api.get('controllers.registration').ready(),
      api.get('controllers.auth').ready()
    ])
  }
}
