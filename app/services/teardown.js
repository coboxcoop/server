const debug = require('@coboxcoop/logger')('@coboxcoop/server:services/stop')

/* load jsdoc type definitions */
require('../types')

module.exports = setup

/**
 * stop the application gracefully
 * lock the app if its unlocked, then
 * close the top-level maindb,
 * we dont close sublevels (they throw a C assertion error)
 * then stop the web server
 *
 * @param {Map} api - dependency container
 */
function setup (api) {
  return async function teardown () {
    const app = api.get('app.express')
    const lock = api.get('app.lock')
    const maindb = api.get('db.main')
    // lock if unlocked (shutdown mounts, corestore, etc)
    if (!app.get('lock')) await lock()
    // close our primary database and all its sublevels
    await maindb.close()
    debug({ message: 'maindb closed' })
  }
}
