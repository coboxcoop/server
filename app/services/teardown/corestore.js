const { sodium_memzero: zero } = require('sodium-native')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:teardown/corestore')

module.exports = teardown

function teardown (api) {
  /**
   * opposite of setup corestore
   * close corestore and the cobox networker
   */
  return async function corestore () {
    debug({ msg: '[LOCK] closing corestore' })
    try {
      // close the networker
      const network = api.get('dat.networker')
      network.close()
      api.set('dat.networker', null)
      const corestore = api.get('dat.store')
      await corestore.close()
      // zero the parent key buffer in corestore
      zero(corestore.inner._masterKey)
      api.set('dat.store', null)
    } catch (err) {
      debug({
        msg: '[LOCK] failed to close corestore',
        message: err.message,
        stack: err.stack
      })
      throw new Error('failed to close corestore', err.message)
    }
  }
}
