const debug = require('@coboxcoop/logger')('@coboxcoop/server:teardown/mount')
const { promises: fs } = require('fs')

module.exports = teardown

function teardown (api) {
  /**
   * delete the mount location
   */
  return async function mount () {
    const { mount } = api.get('app.settings')
    try {
      await fs.rmdir(mount, {
        recursive: true,
        force: true
      })
      debug({ msg: `deleted ${mount}` })
    } catch (err) {
      debug(err)
      throw new Error('failed to remove mount', err.message)
    }
  }
}
