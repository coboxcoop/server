module.exports = teardown

/**
 * whenever we shutdown the app, we need to amend all
 * existing peer records as going offline, otherwise
 * when we come back on, they'll all appear online,
 * even when they may not actually be
 *
 * @param {Map} api - dependency container
 */
function teardown (api) {
  return async function teardownReplicators () {
    const peerdb = api.get('db.peers')
    const ops = []
    const stream = peerdb.createReadStream()
    // stream the entire database, and change
    // online to false, and set the last seen
    // time to be now, since we just went offline!
    for await (const entry of stream) {
      let peerId = entry.key
      let op = {
        type: 'put',
        key: peerId,
        value: {
          peerId: peerId,
          lastSeenAt: Date.now(),
          online: false
        }
      }
      ops.push(op)
    }
    // save to leveldb
    await peerdb.batch(ops)
    // close the database
    return peerdb.close()
  }
}
