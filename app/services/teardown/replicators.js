module.exports = teardown

function teardown (api) {
  return async function teardownReplicators () {
    const replicators = api.get('cache.replicators')
    const proms = []
    for await (const replicator of replicators.values()) {
      proms.push(replicator.close())
    }
    return new Promise((resolve, reject) => {
      Promise.all(proms).then(() => {
        replicators.clear()
        resolve()
      }).catch(reject)
    })
  }
}
