module.exports = teardown

function teardown (api) {
  return async function teardownSeeders () {
    const seeders = api.get('cache.seeders')
    const proms = []
    for await (const seeder of seeders.values()) {
      proms.push(seeder.close())
    }
    return new Promise((resolve, reject) => {
      Promise.all(proms).then(() => {
        seeders.clear()
        resolve()
      }).catch(reject)
    })
  }
}
