module.exports = teardown

function teardown (api) {
  return async function teardownSpaces () {
    const spaces = api.get('cache.spaces')
    const proms = []
    for await (const space of spaces.values()) {
      proms.push(space.close())
    }
    return new Promise((resolve, reject) => {
      Promise.all(proms).then(() => {
        spaces.clear()
        resolve()
      }).catch(reject)
    })
  }
}

