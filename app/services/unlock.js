const debug = require('@coboxcoop/logger')('@coboxcoop/services/unlock')
const crypto = require('@coboxcoop/crypto')
const { promises: fs } = require('fs')

/* load jsdoc type definitions */
require('./../types')

module.exports = setupUnlock

/**
 * @param {Map} api - dependency container
 */
function setupUnlock (api) {
  /**
   * called once authenticated, unlock fetches and decrypts the parent key
   * using a valid password, pops open corestore using the parent key
   * constructs the key derivation function & initializes space factories
   * all controllers have their dependencies loaded creates a nameless identity if one doesn't exist
   *
   * @param {string} password - the password used for key encryption
   * @returns {Identity}
   */
  return async function unlock (password) {
    const app = api.get('app.express')
    const settings = api.get('app.settings')
    const getParentKey = api.get('services.encryption.get-parent-key')
    const getKeyPairDeriver = api.get('services.encryption.derive-key-pair')
    const setupCorestore = api.get('setup.corestore')
    const loadIdentities = api.get('setup.identities')
    const loadSpaces = api.get('setup.spaces')
    const loadReplicators = api.get('setup.replicators')
    const loadSeeders = api.get('setup.seeders')
    const setupAuthenticatedControllers = api.get('setup.auth-controllers')
    // create the mount directory for our space drives
    await fs.mkdir(settings.mount, { recursive: true })
    // decrypt the parent key
    const parentKey = getParentKey(password)
    // ensure we have a key derivation function available
    api.set('setup.signing-key-pair', getKeyPairDeriver(crypto.keyPair, password))
    api.set('setup.encryption-key-pair', getKeyPairDeriver(crypto.deriveBoxKeyPair, password))
    // now load identities and find/create identity
    const identity = await loadIdentities()
    // create a corestore and networker using the parent key as its master key
    await setupCorestore(parentKey, identity)
    // load all resource references from level and cache objects
    await Promise.all([
      loadSpaces(),
      loadReplicators(),
      loadSeeders()
    ])
    // ensure auth controllers have access to now available dependencies
    await setupAuthenticatedControllers()
    // then unlock the app
    app.set('lock', false)
    return identity
  }
}
