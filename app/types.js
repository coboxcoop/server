/**
 * ATTENTION: jsdoc typedefs DO NOT enforce static types
 * in the code, it is not typescript, it is purely for reference
 * they are not comprehensive, and im not sure ive done it correctly!
 *
 * this is a hack, we should probably use jsdoc's import syntax
 * but thats a lot more code maintenance, this seemed adequate
 *
 * ----------------- GENERAL TYPES ------------------ *
 *
 * @typedef {Object} Settings
 * @property {string} port - the port for the http api
 * @property {string} udpPort - the port for UDP packet broadcast
 * @property {string} storage - the root storage path for databases
 * @property {string} mount - the root path for FUSE mounts
 * @property {string} logs - the root path for FUSE mounts
 *
 * @typedef {Object} Application
 * @property {Map} api - dependency container
 * @property {string} id - a unique id for the app instance
 * @property {Settings} settings - app-wide settings
 * @property {string} endpoint - the api end-point
 * @property {Object} app - an express instance
 *
 * @typedef {Object} Route
 * @property {string} path - the api path, e.g. /api/spaces
 * @property {string} method - the http verb, e.g. get
 *
 * @typedef {Object} AsyncMap
 * @property {number} size - the size of the cache
 *
 * see map-filter-reduce package for more details
 * @typedef {Object} Query
 * @property {Object} $filter - an object to filter by
 * @property {Object} $filter.value - the value to filter by
 * @property {Object} $map - a list of fields to map
 * @property {Object} $reduce - an object specifying reducers
 *
 * ----------------- ERROR TYPES ------------------ *
 *
 * @typedef {Error} Forbidden
 * @property {String} message
 * @property {Boolean=true} forbidden
 * @property {Number=403} status
 *
 * @typedef {Error} NotFound
 * @property {String} message
 * @property {Boolean=true} notFound
 * @property {Number=404} status
 *
 * ----------------- RESOURCE TYPES ------------------ *
 *
 * @typedef {Object} Replicator
 * @property {string} name - required
 * @property {string} address - required
 *
 * @typedef {Object} Log
 * @typedef {Object} Drive
 * @property {string} location
 *
 * @typedef {Replicator} Space
 * @property {string} name - required
 * @property {string} address - required
 * @property {string} [encryptionKey] - optional
 * @property {Log} log - a hypercore with queryable indices
 * @property {Drive} drive - a kappa-drive instance
 *
 * @typedef {Replicator} Superusers
 * @property {string} name - required
 * @property {string} address - required
 * @property {string} [encryptionKey] - optional
 * @property {Log} log - a hypercore with queryable indices
 *
 * @typedef {Object} KeyPair
 * @property {Buffer} publicKey
 * @property {Buffer} secretKey
 *
 * @typedef {Object} Identity
 * @property {string} name - required
 * @property {string} publicKey - required
 * @property {string} [secretKey] - optional
 *
 * ----------------- RESPONSE TYPES ------------------ *
 *
 * @typedef {Object} Resource
 * @property {string} name - required
 * @property {string} address - required
 * @property {string} discoveryKey - required
 * @property {string} [location] - optional
 * @property {number} [size] - optional
 * @property {boolean} [swarming] - optional
 *
 * ----------------- MESSAGE TYPES ------------------ *
 *
 * @typedef {Object} LogMessage
 * @property {string} type
 * @property {string} version
 * @property {Number} timestamp
 * @property {string|Buffer} author
 *
 * @typedef {LogMessage} ReplicateCommand
 * @property {string='command/replicate'}
 * @property {Resource} content
 *
 * @typedef {LogMessage} UnreplicateCommand
 * @property {string='command/unreplicate'}
 * @property {Resource} content
 *
 * @typedef {LogMessage} PeerInvite
 * @property {string='peer/invite'}
 * @property {Identity} content
 */
