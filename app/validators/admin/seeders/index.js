const { oneOf } = require('express-validator')

const {
  encryptionKeyChain,
  publicKeyChain,
  SpaceByNameChain,
  SpaceByAddressChain,
  codeChain
} = require('../../chains')

module.exports = adminSeederValidator

function adminSeederValidator (api) {
  const adminSeeders = api.get('cache.seeders')
  const adminSeederByNameChain = SpaceByNameChain(adminSeeders)
  const adminSeederByAddressChain = SpaceByAddressChain(adminSeeders)

  return {
    create: [
      oneOf([
        [
          adminSeederByNameChain(),
          adminSeederByAddressChain(),
          encryptionKeyChain()
        ],
        [
          publicKeyChain()
        ],
        [
          codeChain()
        ]
      ])
    ]
  }
}
