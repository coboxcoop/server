const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const { body } = require('express-validator')

module.exports = authenticationValidator

function authenticationValidator (api) {
  return {
    create: [
      body('password')
      .exists()
      .withMessage('\'password\' required')
      .bail()
    ]
  }
}
