const assert = require('assert')
const crypto = require('@coboxcoop/crypto')
const { body } = require('express-validator')
const { definitions } = require('@coboxcoop/schemas')

const isRequired = `is required`
const isKey = `must be a 32 byte key`
const isName = `must be below 32 characters`
const isIncorrectFormat = 'must be a 272+ character hex code'

// shared validation chains across the application

function encryptionKeyChain () {
  return body('encryptionKey')
    .not()
    .isEmpty()
    .withMessage(isRequired)
    .bail()
    .custom((encryptionKey) => crypto.isKey(encryptionKey))
    .withMessage(isKey)
    .bail()
}

function publicKeyChain () {
  return body('publicKey')
    .not()
    .isEmpty()
    .withMessage(isRequired)
    .bail()
    .custom((publicKey) => crypto.isKey(publicKey))
    .withMessage(isKey)
    .bail()
}

/**
 * validation chain for a space by name
 *
 * @param {AsyncMap} cache - a cache of all spaces
 */
function SpaceByNameChain (cache) {
  const throwWhenSpacePresent = ThrowWhenEntryPresent(cache)

  return function spaceByNameChain () {
    return body('name')
      .not()
      .isEmpty()
      .withMessage(isRequired)
      .bail()
      .custom((name) => !crypto.isKey(name))
      .withMessage(isName)
      .bail()
  }
}

/**
 * validation chain for a space by address
 *
 * @param {AsyncMap} cache - a cache of all spaces
 */
function SpaceByAddressChain (cache) {
  const throwWhenSpacePresent = ThrowWhenEntryPresent(cache)

  return function spaceByAddressChain () {
    return body('address')
      .not()
      .isEmpty()
      .withMessage(isRequired)
      .bail()
      .custom((address) => crypto.isKey(address))
      .withMessage(isKey)
      .bail()
      .custom(async (address, { req }) => throwWhenSpacePresent({ address }))
  }
}

function codeChain () {
  return body('code')
    .not().isEmpty()
    .withMessage(isRequired)
    .bail()
    .matches(definitions.inviteCode.pattern)
    .withMessage(isIncorrectFormat)
    .bail()
}

function ThrowWhenEntryPresent (cache) {
  return async function throwWhenEntryPresent (params) {
    try {
      var entry = await cache.get(params.id)
      assert(!entry, `already exists`)
    } catch (err) {
      if (err.notFound) return true
      throw err
    }
  }
}

module.exports = {
  encryptionKeyChain,
  publicKeyChain,
  codeChain,
  SpaceByNameChain,
  SpaceByAddressChain
}
