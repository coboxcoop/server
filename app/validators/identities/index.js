const { body } = require('express-validator')
const crypto = require('@coboxcoop/crypto')

module.exports = identityValidator

function identityValidator (api) {
  return {
    create: [
      body('name')
      .not()
      .isEmpty()
      .withMessage('\'name\' required')
      .custom((name) => !crypto.isKey(name))
      .withMessage('\'name\' must be below 32 characters')
    ],
    update: [
      body('name')
      .not()
      .isEmpty()
      .withMessage('\'name\' required')
      .custom((name) => !crypto.isKey(name))
      .withMessage('\'name\' must be below 32 characters')
    ]
  }
}
