const debug = require('@coboxcoop/logger')('@coboxcoop/server')
const crypto = require('@coboxcoop/crypto')
const { body, query } = require('express-validator')
const { definitions } = require('@coboxcoop/schemas')

const { publicKeyChain } = require('../chains')

module.exports = inviteValidator

function inviteValidator (api) {
  return {
    accept: [
      query('code')
      .exists()
      .withMessage('\'code\' required')
      .bail()
      .matches(definitions.inviteCode.pattern)
      .withMessage('\'code\' invalid')
      .bail()
    ],
    create: [
      publicKeyChain()
    ]
  }
}
