const path = require('path')
const { body } = require('express-validator')

module.exports = mountValidator

function mountValidator (api) {
  const settings = api.get('app.settings')
  return {
    create: [
      body('location')
      .customSanitizer((location) => location || settings.mount)
    ]
  }
}
