const {
  SpaceByNameChain,
  SpaceByAddressChain
} = require('../chains')

module.exports = replicatorValidator

function replicatorValidator (api) {
  const replicators = api.get('cache.replicators')
  const replicatorByNameChain = SpaceByNameChain(replicators)
  const replicatorByAddressChain = SpaceByAddressChain(replicators)

  return {
    create: [
      replicatorByNameChain(),
      replicatorByAddressChain()
    ]
  }
}
