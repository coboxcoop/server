const { oneOf } = require('express-validator')

const {
  encryptionKeyChain,
  codeChain,
  SpaceByNameChain,
  SpaceByAddressChain
} = require('../chains')

module.exports = spaceValidator

function spaceValidator (api) {
  const spaces = api.get('cache.spaces')
  const spaceByNameChain = SpaceByNameChain(spaces)
  const spaceByAddressChain = SpaceByAddressChain(spaces)

  return {
    create: oneOf([
      spaceByNameChain(),
      [
        spaceByNameChain(),
        spaceByAddressChain(),
        encryptionKeyChain()
      ],
      [
        codeChain()
      ]
    ])
  }
}
