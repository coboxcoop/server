const fs = require('fs')
const { exec } = require('child_process')
const path = require('path')
const options = require('./lib/options')
const { colourize } = require('../util')

exports.command = 'setup-fuse [options]'
exports.desc = 'configure fuse'
exports.builder = { mount: options.mount }
exports.handler = configureFUSE

const mint = colourize(157)
const red = colourize(196)

function configureFUSE (argv, cb) {
  if (!cb) cb = () => {}
  const user = process.geteuid()
  const space = process.getgid()
  const mountdir = path.resolve(argv.mount)

  var _onSuccess = (msgs) => {
    if (!argv.quiet) console.log(msgs)
  }

  var _onError = (err) => {
    if (!argv.quiet) console.log(err.message)
    if (cb) return cb(err)
  }

  checkLocationExists(mountdir, (err, stat) => {
    if (err) return _onError(err)
    if (stat) {
      _onSuccess(`desired mount point ${mint(mountdir)} already exists..`)
      _onSuccess(`assessing permissions...`)

      checkPermissions(stat, (err, can) => {
        if (!err && can) {
          _onSuccess(`correct permissions set!`)
          return onfinish()
        } else {
          return setPermissions(mountdir, onfinish)
        }
      })
    } else {
      // try and create the parent directory...
      exec(`mkdir -p ${path.dirname(mountdir)}`, (err) => {
        if (err) return _onError(err)
        checkLocationExists(path.dirname(mountdir), (err, stat) => {
          if (err) return _onError(err)
          if (stat) {
            checkPermissions(stat, (err, can) => {
              if (!err && can) {
                return exec(`mkdir -p ${mountdir}`, (err) => {
                  if (err) return onfinish(new Error(`could not create the ${mint(mountdir)} directory.`)) // TODO: pass error code
                  else return onfinish()
                })
              }
              return exec(`sudo mkdir -p ${mountdir}`, (err) => {
                if (err) return onfinish(new Error(`could not create the ${mint(mountdir)} directory.`)) // TODO: pass error code
                return setPermissions(mountdir, onfinish)
              })
            })
          }
        })
      })
    }
  })

  function onfinish (err) {
    if (err) return _onError(err)
    _onSuccess(`mount point set to ${mint(mountdir)}`, `successfully configured FUSE`)
    _onSuccess(`successfully configured FUSE`)
    return cb()
  }

  function checkLocationExists (location, cb) {
    fs.stat(location, (err, stat) => {
      if (err && err.errno !== -2) return cb(new Error(`could not get the status of ${red(location)}.`))
      else cb(null, stat)
    })
  }

  function checkPermissions (stat, cb) {
    return cb(null, stat.uid === user && stat.gid === space && !!(2 & parseInt((stat.mode & parseInt('777', 8)).toString(8)[0])))
  }

  function setPermissions (location, cb) {
    exec(`sudo chown ${user}:${space} ${location}`, (err) => {
      if (err) return cb(new Error(`could not change the permissions on the ${red(location)} directory.`))
      return cb()
    })
  }
}
