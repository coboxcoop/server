const bodyParser = require('body-parser')
const { parse: parseCookie } = require('cookie')
const cookieParser = require('cookie-parser')
const constants = require('@coboxcoop/constants')
const crypto = require('@coboxcoop/crypto')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:index')
const EventEmitter = require('events')
const express = require('express')
const cors = require('cors')
const expressWs = require('express-ws')
const fs = require('fs')
const mkdirp = require('mkdirp')
const os = require('os')
const path = require('path')
const session = require('express-session')
const shutdown = require('http-shutdown')
const swaggerUI = require('swagger-ui-express')
const swaggerJsDoc = require('swagger-jsdoc')
const thunky = require('thunky')
const logger = require('@coboxcoop/logger')

const VERSION = require('./version')
const routes = require('./app/routes')
const inject = require('./app/inject')
const { trim, hex, untilde, isProduction } = require('./util')

/* load jsdoc type definitions */
require('./app/types')

const SESSION_COOKIE_NAME = 'connect.sid'
const DEFAULT_HOSTNAME = 'localhost'

function openAPISpecification () {
  const pkg = require('./package.json')
  const apis = path.join(path.dirname(require.resolve('.')), 'app', 'routes', '**', '*.js')
  return swaggerJsDoc({
    definition: {
      openapi: '3.0.0',
      info: {
        title: pkg.name,
        version: pkg.version
      }
    },
    apis: [apis]
  })
}

module.exports = (opts) => new Application(opts)

class Application extends EventEmitter {
  #id = null
  #settings = {}
  /**
   * initialize cobox server, an express http web server
   * using corestore for hypercore persistence and
   * hyperswarm DHT for peer networking and discovery
   * @param {object} opts - an options object
   * @param {string} opts.port - the port for the http api
   * @param {string} opts.udpPort - the port for UDP packet broadcast
   * @param {string} opts.storage - the root storage path for databases
   * @param {string} opts.mount - the root path for FUSE mounts
   */
  constructor (opts = {}) {
    super()
    // generate an app ID for debugging
    // and cache all core settings
    this.#id = hex(crypto.randomBytes(4))
    this.#settings.hostname = opts.hostname || DEFAULT_HOSTNAME
    this.#settings.port = opts.port || 9112
    this.#settings.storage = (opts.storage && path.join(untilde(opts.storage), VERSION)) || path.join(constants.storage, VERSION)
    this.#settings.mount = (opts.mount && untilde(opts.mount)) || constants.mount
    this.#settings.udpPort = opts.udpPort || 8999
    this.#settings.logs = path.join(this.settings.storage, 'logs', 'server')
    this.#settings.tmp = path.join(this.settings.storage, 'tmp')
    // make sure we've created the app storage directory
    mkdirp.sync(this.settings.storage)
    mkdirp.sync(this.settings.logs)
    mkdirp.sync(this.settings.tmp)
    // setup web app and initialize web sockets
    this.app = express()
    expressWs(this.app)
    // set a locked flag, we'll use this for authentication
    this.app.set('lock', true)
    // setup all dependencies as a getter/setter api
    this.api = inject(this)
    // setup http web server middleware, cookies & autoparse JSON
    this.app.use(this._registerAuthHeaders)
    // setup cors
    const corsOrigin = isProduction() ? null : `http://localhost:${process.env.UI_PORT || 8080}`
    this.app.use(cors(trim({ origin: corsOrigin })))
    this.app.use(cookieParser())
    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.app.use(bodyParser.json())
    // setup express session for authentication
    this.app.use(session({
      name: SESSION_COOKIE_NAME,
      store: this.api.get('db.sessions'),
      secret: hex(this._getSessionSecret(this.settings.storage)),
      cookie: { sameSite: 'lax' },
      resave: false,
      saveUninitialized: false
    }))
    // apply custom middleware
    if (isProduction()) {
      // we're going to log request information in production
      this.app.use(logger.requestLogger('@coboxcoop/server:request'))
      this.app.use(require('./app/middleware/log-request')(this.api))
    }
    // strap the UI to the public directory
    const uiPath = this._getUiPath()
    this.app.use(express.static(uiPath))
    this.app.use('*', require('./app/middleware/bootstrap-ui')(uiPath))
    // setup application routes
    this.app.use('/api', routes(this.api))
    // and swagger using @openapi
    this.app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(openAPISpecification()))
    // wrap a thunky around start and close
    this.start = thunky(this.start.bind(this))
    this.stop = thunky(this.stop.bind(this))
  }

  /**
   * secure getter for app id
   * @returns {string}
   */
  get id () {
    return this.#id
  }

  /**
   * secure getter for app-wide settings
   * @returns {Settings}
   */
  get settings () {
    return this.#settings
  }

  /**
   * getter for webserver end-point
   * @returns {string}
   */
  get endpoint () {
    const { hostname, port } = this.settings
    return ['http://', hostname, ':', port].join('')
  }

  /**
   * separate setup logic from starting web server
   * so api integration tests (using supertest) don't hang
   */
  async setup () {
    const setup = this.api.get('app.setup')
    return setup()
  }

  /**
   * start the app, first setup, then start the web server
   */
  async start () {
    // display OS details in development mode
    debug({ platform: os.platform(), arch: os.arch() })
    // first setup the backend
    await this.setup()
    // then start the webserver
    this.webserver = await this.app.listen(this.settings.port, this.settings.hostname)
    shutdown(this.webserver)
    // setup websocket authentication
    this.webserver.on('upgrade', this._authWebSocket.bind(this))
    // make stop accessible via the API
    this.api.set('app.stop', this.stop.bind(this))
    // finally handle SIGINT
    process.on('SIGINT', async (code) => {
      await this.stop()
    })
    // and any uncaught exceptions, we'll want to log
    process.on('uncaughtException', (err) => {
      debug({ msg: '[FATAL]', error: err })
    })
  }

  /**
   * the reverse of setup
   */
  async teardown () {
    const teardown = this.api.get('app.teardown')
    return teardown()
  }

  /**
   * stop the app, first teardown, then stop the web server
   */
  async stop () {
    await this.teardown()
    // stop the server
    debug({ msg: 'shutting down the server' })
    this.webserver.removeListener('upgrade', this._authWebSocket)
    // for some super wierd reason, we can't promisify this?!
    this.webserver.shutdown((err) => {
      if (err) throw err
      debug({ msg: 'closed: server' })
      this.emit('exit')
    })
  }

  /**
   * fetch the UI path, defaults to the fallback
   * 'warning' UI if the @coboxcoop/ui hasn't been compiled
   * @returns {string}
   */
  _getUiPath () {
    let uiPath = path.join(path.dirname(require.resolve('.')), 'public', 'dist')
    try {
      fs.accessSync(path.join(uiPath, 'index.html'))
    } catch (err) {
      uiPath = path.join(path.dirname(require.resolve('.')), 'public', 'default')
    }
    return uiPath
  }

  /**
   * fetch or create a session secret from a local file
   *
   * @param {string} storage - the app's root storage location
   * @returns {string} secret - the session secret hex string
   */
  _getSessionSecret (storage) {
    try {
      return fs.readFileSync(path.join(storage, 'session_secret'), 'utf-8')
    } catch (err) {
      const secret = crypto.randomBytes(32)
      fs.writeFileSync(path.join(storage, 'session_secret'), secret, 'utf-8')
      return hex(secret)
    }
  }

  /**
   * register CORS authorization headers to response object
   *
   * @param {object} req - the request
   * @param {object} res - the response
   * @param {function} next - next action callback
   */
  _registerAuthHeaders (req, res, next) {
    // res.header('Content-Type', 'application/json;charset=UTF-8')
    res.header('Access-Control-Allow-Credentials', true)
    // res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
  }

  /**
   * handle web socket authentication
   */
  _authWebSocket (req, socket, head) {
    if (!req.header.cookie) return
    const cookie = parseCookie(req.header.cookie)
    if (!cookie) return reject()
    const sid = cookie['connect.sid']
    if (!sid) return reject()
    const store = this.api.get('db.sessions')
    store.get(sid, (err, session) => {
      if (err || !session) return reject()
      req.session = session
    })

    function reject () {
      socket.write([
        'HTTP/1.1 401 Web Socket Protocol Handshake',
        'Upgrade: Websocket',
        'Connection: Upgrade',
        '\r\n'
      ].join('\r\n'))
      socket.destroy()
    }
  }
}
