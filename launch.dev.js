const debug = require('@coboxcoop/logger')('@coboxcoop/server:cli')
const nodemon = require('nodemon')
const { pick, flatten } = require('./util')

const DEV_DEBUG = '*,-express*,-bodyparser*,-babel*'

module.exports = function development (opts, cb) {
  process.env.DEBUG = process.env.DEBUG || DEV_DEBUG
  process.env.NODE_ENV = process.env.NODE_ENV || 'development'

  const script = require.resolve('./main.js')
  const args = flatten(pick(opts, ['port', 'hostname', 'storage', 'dev', 'mount', 'udpPort', 'ui']))
  const env = { ...process.env, FORCE_COLOR: process.env.FORCE_COLOR || '2' }
  const stdio = 'inherit'

  const proc = nodemon({
    script,
    args,
    env,
    stdio
  })

  proc.on('exit', (signal) => {
    process.exit(0)
  })

  return cb(null, opts)
}
