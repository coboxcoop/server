if (require.main === module) launch(require('yargs').options(require('./bin/lib/options')).argv)

function launch (opts, cb = noop) {
  console.log('stacking it up, block by block by...')
  if (opts.dev) return require('./launch.dev.js')(opts, cb)
  else return require('./launch.prod.js')(opts, cb)
}

function noop () {}

module.exports = launch
