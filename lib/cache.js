const debug = require('@coboxcoop/logger')('@coboxcoop/server:lib/cache')
const crypto = require('crypto')
const EventEmitter = require('events')

async function noop () {}

class AsyncMap extends EventEmitter {
  /**
   * @param {function} onget - called on get
   * @param {function} onset - called on set
   */
  constructor (onget, onset) {
    super()
    this._id = crypto.randomBytes(4).toString('hex')
    this.onget = onget || noop
    this.onset = onset || noop
    this._cache = new Map()
  }

  async* [Symbol.asyncIterator] () {
    const keys = this.keys()
    const proms = []
    for (const key of keys) {
      yield [key, await this.get(key)]
    }
  }

  get size () {
    return this._cache.size
  }

  has (key) {
    return this._cache.has(key)
  }

  keys () {
    return this._cache.keys()
  }

  values () {
    return this._cache.values()
  }

  entries () {
    return this._cache.entries()
  }

  clear () {
    for (const key of this._cache.keys()) {
      this.delete(key)
    }
  }

  async get (key) {
    const value = this._cache.get(key)
    if (!value) return undefined
    await this.onget(key, value)
    this.emit('touch', key, value)
    return value
  }

  async set (key, value) {
    let hasKey = this._cache.has(key)
    this._cache.set(key, value)
    await this.onset(key, value)
    if (!hasKey) {
      this.emit('add', key, value)
    } else {
      this.emit('update', key, value)
    }
  }

  delete (key) {
    const value = this._cache.get(key)
    this._cache.delete(key)
    this.emit('delete', key, value)
  }
}

module.exports = AsyncMap
