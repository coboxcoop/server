const memdb = require('level-mem')
const collect = require('collect-stream')
const createBroadcastStream = require('broadcast-stream')
const through = require('through2')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:lib/local-seeders')
const LiveStream = require('level-live-stream')
const { encodings, validators } = require('@coboxcoop/schemas')
const Broadcast = encodings.seeders.broadcast
const isBroadcast = validators.seeders.broadcast

// TODO: this functionality was disabled

module.exports = (opts = {}) => {
  var source = createBroadcastStream(opts.port)
    .pipe(decode())

  return {
    store: SeederStore(source)
  }
}

function decode () {
  return through.obj(function (payload, enc, next) {
    try { var msg = Broadcast.decode(payload) }
    catch (err) { return next() }
    if (!isBroadcast(msg)) return next()
    debug({ msg: `seeder announcing:`, msg }, msg)
    this.push(msg)
    next()
  })
}

function SeederStore (stream, opts = {}) {
  const interval = opts.interval || 1000
  const db = memdb({ valueEncoding: Broadcast })

  stream.pipe(through.obj(function (msg, enc, next) {
    db.put(msg.author, msg, next)
  }))

  setInterval(() => {
    var now = Date.now()
    var seeders = db.createReadStream()

    collect(seeders, (err, msgs) => {
      var ops = msgs
        .map((msg) => msg.value)
        .filter((value) => value.content.broadcasting)
        .filter((value) => (now - interval > value.timestamp))
        .map((value) => {
          value.content.broadcasting = false
          return value
        })
        .map((value) => ({
          type: 'put',
          key: value.author,
          value
        }))

      db.batch(ops)
    })
  }, interval)

  LiveStream.install(db)

  return db
}
