const yargs = require('yargs')
const config = require('@coboxcoop/config')
const debug = require('@coboxcoop/logger')('@coboxcoop/server:main')
const App = require('./index')
const options = require('./bin/lib/options')
const open = require('open')
const { isTruthy } = require('./util')

const COBOXRC = '.coboxrc'

const args = yargs
  .config('config', (configPath) => config.load(configPath || COBOXRC))
  .options(options)
  .argv

if (require.main === module) run(args)

module.exports = run

async function run (opts, cb) {
  const app = App(opts)

  try {
    await app.start()
    app.on('exit', () => process.exit(0))

    if (isTruthy(opts.ui)) open(app.endpoint)

    return app
  } catch (err) {
    debug({ msg: 'failed to start the app properly', err })
    throw err
  }
}
