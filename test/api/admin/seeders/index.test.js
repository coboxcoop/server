const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('crypto')
const supertest = require('supertest')
const randomWords = require('random-words')
const { pick, hex } = require('../../../../util')
const { register, logout, tmp, cleanup } = require('../../../spec-helper')
const { encodings } = require('@coboxcoop/schemas')
const PeerInvite = encodings.peer.invite

const App = require('../../../../index')

function isObject (o) {
  return typeof o === 'object' && o !== null
}

async function getCurrentIdentity (request) {
  const response = await request.get('/api/identities/current')
  return response.body
}

function byName (a, b) {
  return a.name > b.name ? 1 : -1
}

describe('api', (context) => {
  let app
  let request
  let factory

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
    factory = app.api.get('factory.seeders')
    const payload = { password: 'my-super-secret-password' }
    await register(request, payload)
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/admin/seeders - lists admin seeders', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const proms = []
    // force create one under a random non-existent identity
    proms.push(factory.createSeeder({
      identity: { publicKey: hex(crypto.randomBytes(32)) },
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32))
    }))
    // create three under the current identity
    const seeders = []
    for (let i = 0; i < 3; ++i) {
      const seeder = {
        name: randomWords(1).pop(),
        address: hex(crypto.randomBytes(32))
      }
      proms.push(factory.createSeeder({ identity, ...seeder }))
      seeders.push(seeder)
    }
    await Promise.all(proms)
    const response = await request.get('/api/admin/seeders')

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')

    const responseData = response.body.map((entry) => pick(entry, ['name', 'address']))
    assert.same(seeders.sort(byName), responseData.sort(byName), 'returns the correct seeder metadata')
    next()
  })

  context('GET /api/admin/seeders - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/admin/seeders')
    assert.same(response.statusCode, 401, 'is not authorized')
    next()
  })

  context('POST /api/admin/seeders - create a new seeder using name, address and encryption key', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }

    let response = await request
      .post('/api/admin/seeders')
      .send(params)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new seeder's metadata`
    )

    response = await request.get('/api/admin/seeders')
    assert.same(response.body.length, 1, 'persists the seeder')
    next()
  })

  context('POST /api/admin/seeders - create a new seeder using an invite code', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const seeder = { name: 'magma', address: hex(crypto.randomBytes(32)) }
    const inviteParams = Object.assign({}, { encryptionKey: hex(crypto.randomBytes(32)) }, identity, seeder)
    const packInvite = app.api.get('services.invites.pack')
    const code = hex(packInvite(inviteParams))

    let response = await request
      .post('/api/admin/seeders')
      .send({ code })

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(inviteParams, ['name', 'address']),
      `returns the new seeder's metadata`
    )

    response = await request.get('/api/admin/seeders')
    assert.same(response.body.length, 1, 'persists the seeder')
    next()
  })

  context('POST /api/admin/seeders - forbidden without authentication', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }
    await logout(request)
    const response = await request.post('/api/admin/seeders').send(params)
    assert.same(response.statusCode, 401, 'is unauthorized')
    next()
  })

  context('DELETE /api/admin/seeders - deletes an existing seeder by address', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder on the backend
    await factory.createSeeder({
      identity,
      ...params
    })
    // delete by address via api
    let response = await request
      .delete(`/api/admin/seeders/${address}`)
    // returns the seeder info
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new seeder's metadata`
    )
    // no seeders exist
    response = await request.get('/api/admin/seeders')
    assert.same(response.body.length, 0, 'deletes the seeder')
    next()
  })

  context('DELETE /api/admin/seeders/:id - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    await logout(request)
    const response = await request.delete(`/api/admin/seeders/${address}`).send(params)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/admin/seeders/:id/peers - list the peers in a seeder', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder on the backend
    await factory.createSeeder({ identity, ...params })
    // delete by address via api
    let response = await request
      .get(`/api/admin/seeders/${address}/peers`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(isObject(response.body), 'returns an object')
    assert.same(Object.entries(response.body).length, 1, 'returns a single peer')
    const peer = Object.values(response.body)[0]
    assert.same(peer.type, 'peer/about', 'returns a peer/about message')
    assert.same(peer.author, identity.publicKey, 'returns the author')
    next()
  })

  context('GET /api/admin/seeders/:id/peers - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.get(`/api/admin/seeders/${address}/peers`)
    assert.same(response.statusCode, 401, 'is unauthorized')
    next()
  })

  context('GET /api/admin/seeders/:id/invites - lists the invites in a seeder', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder and an invite on the backend
    const seeder = await factory.createSeeder({ identity, ...params })
    // generate a peer id to invite
    const publicKey = hex(crypto.randomBytes(32))
    // publish invite message to hypercore
    await seeder.log.publish({
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content: { publicKey: hex(publicKey) }
    }, { valueEncoding: PeerInvite })

    let response = await request
      .get(`/api/admin/seeders/${address}/invites`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(Array.isArray(response.body), 'returns a list')
    assert.same(response.body.length, 1, 'returns a single invite')
    assert.same(response.body[0].data.type, 'peer/invite', 'returns a peer/invite message')
    assert.same(response.body[0].data.content.publicKey, publicKey, 'returns the public key')
    next()
  })

  context('GET /api/admin/seeders/:id/invites - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.get(`/api/admin/seeders/${address}/invites`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/admin/seeders/:id/invites - creates an invite', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder on the backend
    await factory.createSeeder({ identity, ...params })
    // generate a peer id to invite
    const publicKey = hex(crypto.randomBytes(32))
    const name = randomWords(1).pop()
    let response = await request
      .post(`/api/admin/seeders/${address}/invites`)
      .send({ name, publicKey })

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.body.type, 'peer/invite', 'returns an invite message')
    assert.same(response.body.content.publicKey, publicKey, 'returns the public key of the invitee')
    assert.ok(response.body.content.code, 'returns an invite code')
    next()
  })

  context('POST /api/admin/seeders/:id/invites - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/admin/seeders/${address}/invites`).send({ address })
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/admin/seeders/:id/connections - starts swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder on the backend
    const seeder = await factory.createSeeder({ identity, ...params })
    // stub out the swarm function, we dont want any DHT activity
    seeder.swarm = sinon.stub().resolves()

    let response = await request
      .post(`/api/admin/seeders/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(seeder.swarm.calledOnce, 'starts swarming')
    next()
  })

  context('POST /api/admin/seeders/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/admin/seeders/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('DELETE /api/admin/seeders/:id/connections - stops swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a seeder on the backend
    const seeder = await factory.createSeeder({ identity, ...params })
    // stub out the unswarm function, we dont want any DHT activity
    seeder.unswarm = sinon.stub().resolves()

    let response = await request
      .delete(`/api/admin/seeders/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(seeder.unswarm.calledOnce, 'stops swarming')
    next()
  })

  context('DELETE /api/admin/seeders/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.delete(`/api/admin/seeders/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })
})
