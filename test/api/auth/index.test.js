const { describe } = require('../../tape')
const supertest = require('supertest')
const App = require('../../../index')
const { register, logout, tmp, cleanup } = require('../../spec-helper')

describe('api', (context) => {
  let app
  let request

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('POST /api/auth returns a 200 with correct password', async (assert, next) => {
    assert.ok(app.app.get('lock'), 'lock is on')
    const payload = { password: 'my-super-secret-password' }
    await register(request, payload)
    const response = await request.post('/api/auth').send(payload)
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 200, 'returns a 200')
    assert.ok(response.request.cookies, 'returns a session id in a cookie')
    assert.notok(app.app.get('lock'), 'lock is off')
    next()
  })

  context('POST /api/auth returns a 401 with incorrect password', async (assert, next) => {
    await register(request, { password: 'my-super-secret-password' })
    await logout(request)
    assert.ok(app.app.get('lock'), 'lock is on')
    const response = await request.post('/api/auth').send({ password: 'the wrong password' })
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 401, 'returns a 403')
    assert.ok(app.app.get('lock'), 'lock is on')
    next()
  })

  context('POST /api/auth returns a 404 if not yet registered', async (assert, next) => {
    assert.ok(app.app.get('lock'), 'lock is on')
    const response = await request.post('/api/auth').send({ password: '123456' })
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 404, 'returns a 404')
    assert.ok(app.app.get('lock'), 'lock is on')
    next()
  })

  context('DELETE /api/auth returns a 200 when logged in', async (assert, next) => {
    await register(request, { password: 'my-super-secret-password' })
    assert.notok(app.app.get('lock'), 'lock is off')
    const response = await request.delete('/api/auth')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 200, 'returns a 200')
    assert.ok(app.app.get('lock'), 'lock is on')
    next()
  })

  context('DELETE /api/auth returns a 401 when not logged in', async (assert, next) => {
    const response = await request.delete('/api/auth')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 401, 'returns a 401')
    assert.ok(app.app.get('lock'), 'lock is on')
    next()
  })
})
