// TODO: needs fixing, seems to work through the UI
// const { describe } = require('tape-plus')
// const axios = require('axios')
// const WebSocket = require('ws')
// const randomWords = require('random-words')
// const createParams = require('../../../app/helpers/create-params')
// const { assign } = Object
// const { sleep, tmp, cleanup } = require('../../spec-helper')
// const { hex } = require('../../../util')

// const App = require('../../../index')

// describe('api', (context) => {
//   context('WS /api/events - returns a live stream of events', async (assert, next) => {
//     // create an app instance
//     const app = App({ storage: tmp() })
//     await app.start()
//     await sleep(100)
//     // sign up
//     const response = await axios({
//       method: 'POST',
//       url: app.endpoint + '/api/register',
//       headers: { 'Content-Type': 'application/json' },
//       data: { password: '123456' }
//     })
//     // open a web socket connection
//     const endpoint = ['ws://', app.settings.hostname, ':', app.settings.port, '/api'].join('')
//     const socket = new WebSocket(endpoint)
//     const events = []
//     const messages = []

//     socket.on('headers', setAuthHeader)
//     socket.on('message', (payload) => events.push(JSON.parse(payload.data)))
//     socket.on('close', check)

//     const cache = app.api.get('cache.identities')
//     const identity = Array.from(cache)[0][1]
//     identity.name = randomWords(1).pop()
//     cache.set(identity.publicKey, identity)

//     console.log(app.settings.storage)
//     const spaceFactory = app.api.get('factory.spaces')
//     const params = assign(createParams({ name: randomWords(1).pop() }), { identity })
//     const space = await spaceFactory.createSpace(params)

//     messages.push({
//       type: 'space/about',
//       version: '1.0.0',
//       address: hex(space.address),
//       author: identity.publicKey,
//       content: { name: space.name }
//     })
//     messages.push({
//       type: 'peer/about',
//       version: '1.0.0',
//       address: hex(space.address),
//       author: identity.publicKey,
//       content: { name: identity.name }
//     })

//     await sleep(1000)

//     socket.close()

//     function setAuthHeader (headers) {
//       headers['set-cookie'] = response.headers['set-cookie']
//     }

//     async function check () {
//       assert.ok(messages.length === 2, 'messages to check against are present')
//       assert.ok(events.length === 2, 'returns a populated stream of data')
//       assert.same(messages, events, 'announces the correct events')
//       await app.stop()
//       // await cleanup(app.settings.storage)
//       next()
//     }
//   })

//   context('WS /api/events - forbidden without authentication', async (assert, next) => {
//     // create an app instance
//     const app = App({ storage: tmp() })
//     await app.start()
//     await sleep(100)
//     // open a web socket connection
//     const endpoint = ['ws://', app.settings.hostname, ':', app.settings.port, '/api'].join('')
//     const socket = new WebSocket(endpoint)
//     const events = []

//     socket.onmessage = (payload) => {
//       const event = JSON.parse(payload.data)
//       events.push(event)
//     }
//     socket.onclose = check
//     await sleep(1000)

//     socket.close()

//     async function check () {
//       await app.stop()
//       await cleanup(app.settings.storage)
//       next()
//     }
//   })
// })
