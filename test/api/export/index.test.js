const { describe } = require('../../tape')
const supertest = require('supertest')
const App = require('../../../index')
const { register, logout, tmp, cleanup } = require('../../spec-helper')

describe('api', (context) => {
  let app
  let request
  let params

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
    params = { password: 'my-super-secret-password' }
    await register(request, params)
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/export returns a 200 with correct password', async (assert, next) => {
    const response = await request.get('/api/export').send(params)
    assert.same(response.statusCode, 200, 'returns a 200')
    assert.same(response.headers['content-disposition'], 'attachment; filename="backup.cobox"', 'returns a backup.cobox file')
    next()
  })

  context('GET /api/export returns a 401 with incorrect password', async (assert, next) => {
    const response = await request.get('/api/export').send({ password: 'wrong password' })
    assert.same(response.statusCode, 401, 'returns a 401')
    next()
  })

  context('GET /api/export returns a 401 without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/export').send(params)
    assert.same(response.statusCode, 401, 'returns a 401')
    next()
  })
})
