const { describe } = require('tape-plus')
const supertest = require('supertest')
const randomWords = require('random-words')
const { register, logout, tmp, cleanup } = require('../../spec-helper')

const App = require('../../../index')

describe('api', (context) => {
  let app
  let request
  let factory

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
    factory = app.api.get('factory.identities')
    const payload = { password: 'my-super-secret-password' }
    await register(request, payload)
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/identities - lists all identities', async (assert, next) => {
    const proms = []
    for (let i = 0; i < 3; ++i) {
      proms.push(factory.createIdentity({ name: randomWords(1).pop() }))
    }
    await Promise.all(proms)
    const response = await request.get('/api/identities')
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.body.length, 4, 'returns a list of identities')
    next()
  })

  context('GET /api/identities - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/identities')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/identities - creates a new identity', async (assert, next) => {
    const name = randomWords(1).pop()
    const response = await request.post('/api/identities').send({ name })
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.body.name, name, 'returns a name')
    assert.ok(response.body.publicKey, 'returns a public key')
    assert.notok(response.body.secretKey, 'does not return the secret key')
    next()
  })

  context('POST /api/identities - fails without a name ', async (assert, next) => {
    const response = await request.post('/api/identities')
    assert.same(response.statusCode, 422, 'is unprocessable')
    assert.same(response.body.errors, [{ msg: "'name' required", param: 'name', location: 'body' }])
    next()
  })

  context('POST /api/identities - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/identities')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context(`GET /api/identities/current - returns the current session's identity`, async (assert, next) => {
    const response = await request.get('/api/identities/current')
    assert.same(response.statusCode, 200, 'is ok')
    assert.ok(response.body.publicKey, 'returns a public key')
    next()
  })

  context('GET /api/identities/current - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/identities/current')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  // TODO: this is currently disabled
  // context('GET /api/identities/:id/change - changes the session identity to specified', async (assert, next) => {
  //   const nextId = await factory.createIdentity({ name: randomWords(1).pop() })
  //   let response = await request.get(`/api/identities/${nextId.publicKey}/change`)
  //   assert.same(response.statusCode, 200, 'is ok')
  //   assert.same(nextId.publicKey, response.body.publicKey, 'returns the next identity')
  //   response = await request.get('/api/identities/current')
  //   assert.same(response.statusCode, 200, 'is ok')
  //   assert.same(nextId.publicKey, response.body.publicKey, 'switches the current identity')
  //   next()
  // })

  // context('GET /api/identities/:id/change - forbidden without authentication', async (assert, next) => {
  //   const nextId = await factory.createIdentity({ name: randomWords(1).pop() })
  //   await logout(request)
  //   const response = await request.get(`/api/identities/${nextId.publicKey}/change`)
  //   assert.same(response.statusCode, 401, 'is forbidden')
  //   next()
  // })

  context('PATCH /api/identities/:id - forbidden without authentication', async (assert, next) => {
    const { body: { publicKey } } = await request.get('/api/identities/current')
    const name = randomWords(1).pop()
    let response = await request.patch(`/api/identities/${publicKey}`).send({ name })
    assert.same(response.statusCode, 200, 'is ok')
    assert.ok(response.body.publicKey, 'returns the identity public key')
    assert.same(response.body.name, name, 'returns the new identity name')
    response = await request.get('/api/identities/current')
    assert.same(response.body.name, name, 'changes the identity name')
    next()
  })

  context('PATCH /api/identities - fails without a name ', async (assert, next) => {
    const { body: { publicKey } } = await request.get('/api/identities/current')
    const response = await request.patch(`/api/identities/${publicKey}`)
    assert.same(response.statusCode, 422, 'is unprocessable')
    assert.same(response.body.errors, [{ msg: "'name' required", param: 'name', location: 'body' }])
    next()
  })

  context('PATCH /api/identities/:id - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/identities')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })
})
