const { describe } = require('../../tape')
const { promises: fs } = require('fs')
const cookie = require('cookie')
const path = require('path')
const supertest = require('supertest')
const App = require('../../../index')

const { tmp, cleanup } = require('../../spec-helper')

describe('api', (context) => {
  let app
  let request

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('POST api/register returns a 200 when not yet registered', async (assert, next) => {
    assert.ok(app.app.get('lock'), 'lock is on')
    const payload = { password: 'my-super-secret-password' }
    const response = await request.post('/api/register').send(payload)
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 200, 'returns a 200')
    assert.ok(await fs.readFile(path.join(app.settings.storage, 'password_hash')), 'password_hash file is persisted to storage')
    assert.ok(await fs.readFile(path.join(app.settings.storage, 'encrypted_parent_key')), 'encrypted_parent_key file is persisted to storage')
    const sessionCookie = cookie.parse(response.header['set-cookie'][0])
    assert.ok(sessionCookie['connect.sid'], 'returns a session id in a cookie')
    assert.notok(app.app.get('lock'), 'lock is off')
    next()
  })

  context('POST api/register returns a 401 if already registered', async (assert, next) => {
    const payload = { password: 'my-super-secret-password' }
    await request.post('/api/register').send(payload)
    const response = await request.post('/api/register').send(payload)
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 401, 'returns a 401')
    next()
  })
})
