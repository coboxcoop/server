const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('crypto')
const supertest = require('supertest')
const randomWords = require('random-words')
const { pick, hex } = require('../../../util')
const { register, logout, tmp, cleanup } = require('../../spec-helper')

const App = require('../../../index')

async function getCurrentIdentity (request) {
  const response = await request.get('/api/identities/current')
  return response.body
}

function byName (a, b) {
  return a.name > b.name ? 1 : -1
}

describe('api', (context) => {
  let app
  let request
  let factory

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
    factory = app.api.get('factory.replicators')
    const payload = { password: 'my-super-secret-password' }
    await register(request, payload)
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/replicators - lists replicators', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const proms = []
    // force create one under a random non-existent identity
    proms.push(factory.createReplicator({
      identity: { publicKey: hex(crypto.randomBytes(32)) },
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32))
    }))
    // create three under the current identity
    const replicators = []
    for (let i = 0; i < 3; ++i) {
      const replicator = {
        name: randomWords(1).pop(),
        address: hex(crypto.randomBytes(32))
      }
      proms.push(factory.createReplicator({ identity, ...replicator }))
      replicators.push(replicator)
    }
    await Promise.all(proms)
    const response = await request.get('/api/replicators')
    const responseData = response.body.map((entry) => pick(entry, ['name', 'address']))
    assert.same(replicators.sort(byName), responseData.sort(byName), 'returns the correct replicator metadata')
    next()
  })

  context('GET /api/replicators - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/replicators')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/replicators - create a new replicator using name and address', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32))
    }

    let response = await request
      .post('/api/replicators')
      .send(params)

    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new replicator's metadata`
    )

    response = await request.get('/api/replicators')
    assert.same(response.body.length, 1, 'persists the replicator')
    next()
  })

  context('POST /api/replicators - forbidden without authentication', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }
    await logout(request)
    const response = await request.post('/api/replicators').send(params)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('DELETE /api/replicators - deletes an existing replicator by address', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a replicator on the backend
    await factory.createReplicator({
      identity,
      ...params
    })
    // delete by address via api
    let response = await request
      .delete(`/api/replicators/${address}`)
    // returns the replicator info
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new replicator's metadata`
    )
    // no replicators exist
    response = await request.get('/api/replicators')
    assert.same(response.body.length, 0, 'deletes the replicator')
    next()
  })

  context('DELETE /api/replicators/:id - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    await logout(request)
    const response = await request.delete(`/api/replicators/${address}`).send(params)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/replicators/:id/connections - starts swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    const space = await factory.createReplicator({ identity, ...params })
    // stub out the swarm function, we dont want any DHT activity
    space.swarm = sinon.stub().resolves()

    let response = await request
      .post(`/api/replicators/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(space.swarm.calledOnce, 'starts swarming')
    next()
  })

  context('POST /api/replicators/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/replicators/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('DELETE /api/replicators/:id/connections - stops swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    const space = await factory.createReplicator({ identity, ...params })
    // stub out the unswarm function, we dont want any DHT activity
    space.unswarm = sinon.stub().resolves()

    let response = await request
      .delete(`/api/replicators/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(space.unswarm.calledOnce, 'stops swarming')
    next()
  })

  context('DELETE /api/replicators/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.delete(`/api/replicators/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })
})
