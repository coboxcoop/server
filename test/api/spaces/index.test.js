const { describe } = require('tape-plus')
const sinon = require('sinon')
const crypto = require('crypto')
const supertest = require('supertest')
const randomWords = require('random-words')
const { pick, hex } = require('../../../util')
const { register, logout, tmp, cleanup } = require('../../spec-helper')
const { encodings } = require('@coboxcoop/schemas')
const PeerInvite = encodings.peer.invite
const { promisify } = require('util')

const App = require('../../../index')

function isObject (o) {
  return typeof o === 'object' && o !== null
}

async function getCurrentIdentity (request) {
  const response = await request.get('/api/identities/current')
  return response.body
}

function byName (a, b) {
  return a.name > b.name ? 1 : -1
}

describe('api', (context) => {
  let app
  let request
  let factory

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
    factory = app.api.get('factory.spaces')
    const payload = { password: 'my-super-secret-password' }
    await register(request, payload)
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/spaces - lists spaces', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const proms = []
    // force create one under a random non-existent identity
    proms.push(factory.createSpace({
      identity: { publicKey: hex(crypto.randomBytes(32)) },
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32))
    }))
    // create three under the current identity
    const spaces = []
    for (let i = 0; i < 3; ++i) {
      const space = {
        name: randomWords(1).pop(),
        address: hex(crypto.randomBytes(32))
      }
      proms.push(factory.createSpace({ identity, ...space }))
      spaces.push(space)
    }
    await Promise.all(proms)
    const response = await request.get('/api/spaces')
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    const responseData = response.body.map((entry) => pick(entry, ['name', 'address']))
    assert.same(spaces.sort(byName), responseData.sort(byName), 'returns the correct space metadata')
    next()
  })

  context('GET /api/spaces - forbidden without authentication', async (assert, next) => {
    await logout(request)
    const response = await request.get('/api/spaces')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/spaces - create a new space using name, address and encryption key', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }

    let response = await request
      .post('/api/spaces')
      .send(params)

    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new space's metadata`
    )

    response = await request.get('/api/spaces')
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.body.length, 1, 'persists the space')
    next()
  })

  context('POST /api/spaces - create a new space using an invite code', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const space = { name: 'magma', address: hex(crypto.randomBytes(32)) }
    const inviteParams = Object.assign({}, { encryptionKey: hex(crypto.randomBytes(32)) }, identity, space)
    const packInvite = app.api.get('services.invites.pack')
    const code = hex(packInvite(inviteParams))

    let response = await request
      .post('/api/spaces')
      .send({ code })

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(inviteParams, ['name', 'address']),
      `returns the new space's metadata`
    )

    response = await request.get('/api/spaces')
    assert.same(response.body.length, 1, 'persists the space')
    next()
  })

  context('POST /api/spaces - forbidden without authentication', async (assert, next) => {
    const params = {
      name: 'magma',
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }
    await logout(request)
    const response = await request.post('/api/spaces').send(params)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('DELETE /api/spaces/:id - deletes an existing space by address', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    await factory.createSpace({
      identity,
      ...params
    })
    // delete by address via api
    let response = await request
      .delete(`/api/spaces/${address}`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(
      pick(response.body, ['name', 'address']),
      pick(params, ['name', 'address']),
      `returns the new space's metadata`
    )

    response = await request.get('/api/spaces')
    assert.same(response.body.length, 0, 'deletes the space')
    next()
  })

  context('DELETE /api/spaces/:id - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    await logout(request)
    const response = await request.delete(`/api/spaces/${address}`).send(params)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/spaces/:id/peers - list the peers in a space', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    await factory.createSpace({ identity, ...params })
    // delete by address via api
    let response = await request
      .get(`/api/spaces/${address}/peers`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(isObject(response.body), 'returns an object')
    assert.same(Object.entries(response.body).length, 1, 'returns a single peer')
    const peer = Object.values(response.body)[0]
    assert.same(peer.type, 'peer/about', 'returns a peer/about message')
    assert.same(peer.author, identity.publicKey, 'returns the author')
    next()
  })

  context('GET /api/spaces/:id/peers - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.get(`/api/spaces/${address}/peers`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/spaces/:id/invites - lists the invites in a space', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space and an invite on the backend
    const space = await factory.createSpace({ identity, ...params })
    // generate a peer id to invite
    const publicKey = hex(crypto.randomBytes(32))
    // publish invite message to hypercore
    await space.log.publish({
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content: { publicKey: hex(publicKey) }
    }, { valueEncoding: PeerInvite })

    let response = await request
      .get(`/api/spaces/${address}/invites`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(Array.isArray(response.body), 'returns a list')
    assert.same(response.body.length, 1, 'returns a single invite')
    assert.same(response.body[0].data.type, 'peer/invite', 'returns a peer/invite message')
    assert.same(response.body[0].data.content.publicKey, publicKey, 'returns the public key')
    next()
  })

  context('GET /api/spaces/:id/invites - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.get(`/api/spaces/${address}/invites`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/spaces/:id/invites - creates an invite', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    await factory.createSpace({ identity, ...params })
    // generate a peer id to invite
    const publicKey = hex(crypto.randomBytes(32))
    const name = randomWords(1).pop()
    let response = await request
      .post(`/api/spaces/${address}/invites`)
      .send({ name, publicKey })

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.same(response.body.type, 'peer/invite', 'returns an invite message')
    assert.same(response.body.content.publicKey, publicKey, 'returns the public key of the invitee')
    assert.ok(response.body.content.code, 'returns an invite code')
    next()
  })

  context('POST /api/spaces/:id/invites - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/spaces/${address}/invites`).send({ address })
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('POST /api/spaces/:id/connections - starts swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    const space = await factory.createSpace({ identity, ...params })
    // stub out the swarm function, we dont want any DHT activity
    space.swarm = sinon.stub().resolves()

    let response = await request
      .post(`/api/spaces/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(space.swarm.calledOnce, 'starts swarming')
    next()
  })

  context('POST /api/spaces/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/spaces/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('DELETE /api/spaces/:id/connections - stops swarming', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    // seed a space on the backend
    const space = await factory.createSpace({ identity, ...params })
    // stub out the unswarm function, we dont want any DHT activity
    space.unswarm = sinon.stub().resolves()

    let response = await request
      .delete(`/api/spaces/${address}/connections`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.headers['content-type'].substr(0, 16), 'application/json', 'accepts json')
    assert.ok(space.unswarm.calledOnce, 'stops swarming')
    next()
  })

  context('DELETE /api/spaces/:id/connections - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.delete(`/api/spaces/${address}/connections`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/spaces/:id/drive/history - returns the drive state history', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    const space = await factory.createSpace({ identity, ...params })

    const check = []
    const proms = []
    for (let i = 0; i < 3; ++i) {
      proms.push(new Promise((resolve, reject) => {
        const filename = [randomWords(1).pop(), 'txt'].join('.')
        space.drive.writeFile(filename, randomWords(5).join(' '), (err) => {
          if (err) return reject(err)
          check.push(filename)
          resolve()
        })
      }))
    }
    await Promise.all(proms)
    await new Promise((resolve, reject) => {
      const filename = check[0]
      space.drive.writeFile(filename, randomWords(5).join(' '), (err) => {
        if (err) return reject(err)
        check.push(filename)
        resolve()
      })
    })

    const response = await request
      .get(`/api/spaces/${address}/drive/history`)

    const history = response.body.map((msg) => msg.value.filename)
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(check, history, 'returns historical changes to the drive')
    next()
  })

  context('GET /api/spaces/:id/drive/history - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/spaces/${address}/drive/history`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/spaces/:id/drive/readdir - lists all files in a drive', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    const space = await factory.createSpace({ identity, ...params })

    const check = []
    const proms = []
    for (let i = 0; i < 3; ++i) {
      proms.push(new Promise((resolve, reject) => {
        const filename = [randomWords(1).pop(), 'txt'].join('.')
        space.drive.writeFile(filename, randomWords(5).join(' '), (err) => {
          if (err) return reject(err)
          check.push(filename)
          resolve()
        })
      }))
    }
    await Promise.all(proms)
    await new Promise((resolve, reject) => {
      const filename = check[0]
      space.drive.writeFile(filename, randomWords(5).join(' '), (err) => {
        if (err) return reject(err)
        resolve()
      })
    })

    const response = await request
      .get(`/api/spaces/${address}/drive/readdir`)

    const files = Object.keys(response.body)
    const sort = (a, b) => a > b ? 1 : -1
    assert.same(response.statusCode, 200, 'is ok')
    assert.same(check.sort(sort), files.sort(sort), 'returns historical changes to the drive')
    next()
  })

  context('GET /api/spaces/:id/drive/readdir - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/spaces/${address}/drive/readdir`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })

  context('GET /api/spaces/:id/drive/stat - stat of the drive', async (assert, next) => {
    const identity = await getCurrentIdentity(request)
    const address = hex(crypto.randomBytes(32))
    const params = { name: randomWords(1).pop(), address }
    const space = await factory.createSpace({ identity, ...params })

    await new Promise((resolve, reject) => {
      const filename = 'hello.txt'
      space.drive.writeFile(filename, 'world', (err) => {
        if (err) return reject(err)
        resolve()
      })
    })

    const response = await request
      .get(`/api/spaces/${address}/drive/stat`)

    assert.same(response.statusCode, 200, 'is ok')
    assert.same(response.body.size, 390, 'returns the summed number of bytes of all cores')
    next()
  })

  context('GET /api/spaces/:id/drive/stat - forbidden without authentication', async (assert, next) => {
    const address = hex(crypto.randomBytes(32))
    await logout(request)
    const response = await request.post(`/api/spaces/${address}/drive/stat`)
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })
})
