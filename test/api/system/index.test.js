const { describe } = require('tape-plus')
const sinon = require('sinon')
const supertest = require('supertest')
const randomWords = require('random-words')
const { pick } = require('../../../util')
const { register, logout, tmp, cleanup } = require('../../spec-helper')

const App = require('../../../index')

describe('api', (context) => {
  let app
  let request

  context.beforeEach(async (c) => {
    app = App({ storage: tmp() })
    request = supertest.agent(app.app)
    await app.setup()
  })

  context.afterEach(async (c) => {
    await app.teardown()
    await cleanup(app.settings.storage)
  })

  context('GET /api/system - displays the app info', async (assert, next) => {
    const details = pick(require('../../../package.json'), ['name', 'description', 'version', 'author', 'license', 'bugs'])
    const response = await request.get('/api/system')
    assert.same(details, response.body, 'returns the app info')
    next()
  })

  context('GET /api/system/stop - calls the stop function', async (assert, next) => {
    const stop = sinon.stub()
    app.api.set('app.stop', stop)
    await request.get('/api/system/stop')
    assert.ok(stop.calledOnce, 'calls the stop function')
    next()
  })

  // TODO: for some wierd reason, we've got a duplicate /api/system/logs route?!
  // context('GET /api/system/routes - returns a full set of API routes', async (assert, next) => {
  //   const response = await request.get('/api/system/routes')
  //   assert.same(response.body, [
  //     { path: '/api/system/', method: 'get' },
  //     { path: '/api/system/stop', method: 'get' },
  //     { path: '/api/system/logs', method: 'get' },
  //     { path: '/api/system/routes', method: 'get' },
  //     { path: '/api/auth/', method: 'post' },
  //     { path: '/api/auth/', method: 'post' },
  //     { path: '/api/auth/', method: 'delete' },
  //     { path: '/api/register/', method: 'post' },
  //     { path: '/api/register/', method: 'post' },
  //     { path: '/api/admin/', method: 'get' },
  //     { path: '/api/admin/', method: 'post' },
  //     { path: '/api/admin/', method: 'post' },
  //     { path: '/api/admin/:id', method: 'delete' },
  //     { path: '/api/admin/:id/peers', method: 'get' },
  //     { path: '/api/admin/:id/connections/', method: 'post' },
  //     { path: '/api/admin/:id/connections/', method: 'delete' },
  //     { path: '/api/admin/connections', method: 'post' },
  //     { path: '/api/admin/connections', method: 'delete' },
  //     { path: '/api/admin/invites/accept', method: 'get' },
  //     { path: '/api/admin/invites/accept', method: 'get' },
  //     { path: '/api/admin/:id/invites', method: 'get' },
  //     { path: '/api/admin/:id/invites/', method: 'post' },
  //     { path: '/api/admin/:id/invites/', method: 'post' },
  //     { path: '/api/admin/:id/commands/replicates', method: 'get' },
  //     { path: '/api/admin/:id/commands/replicate', method: 'post' },
  //     { path: '/api/admin/:id/commands/replicate', method: 'post' },
  //     { path: '/api/admin/:id/commands/replicate', method: 'post' },
  //     { path: '/api/admin/:id/commands/unreplicate', method: 'post' },
  //     { path: '/api/admin/:id/commands/unreplicate', method: 'post' },
  //     { path: '/api/admin/:id/commands/unreplicate', method: 'post' },
  //     { path: '/api/export/', method: 'get' },
  //     { path: '/api/identities/', method: 'get' },
  //     { path: '/api/identities/', method: 'post' },
  //     { path: '/api/identities/', method: 'post' },
  //     { path: '/api/identities/current', method: 'get' },
  //     { path: '/api/identities/:id/change', method: 'get' },
  //     { path: '/api/identities/:id', method: 'patch' },
  //     { path: '/api/identities/:id', method: 'patch' },
  //     { path: '/api/import/', method: 'post' },
  //     { path: '/api/import/', method: 'post' },
  //     { path: '/api/replicators/', method: 'get' },
  //     { path: '/api/replicators/', method: 'post' },
  //     { path: '/api/replicators/', method: 'post' },
  //     { path: '/api/replicators/', method: 'post' },
  //     { path: '/api/replicators/:id', method: 'delete' },
  //     { path: '/api/replicators/:id/connections', method: 'post' },
  //     { path: '/api/replicators/:id/connections', method: 'delete' },
  //     { path: '/api/replicators/connections', method: 'post' },
  //     { path: '/api/replicators/connections', method: 'delete' },
  //     { path: '/api/spaces/', method: 'get' },
  //     { path: '/api/spaces/', method: 'post' },
  //     { path: '/api/spaces/', method: 'post' },
  //     { path: '/api/spaces/:id', method: 'delete' },
  //     { path: '/api/spaces/:id/peers', method: 'get' },
  //     { path: '/api/spaces/:id/connections', method: 'post' },
  //     { path: '/api/spaces/:id/connections', method: 'delete' },
  //     { path: '/api/spaces/connections', method: 'post' },
  //     { path: '/api/spaces/connections', method: 'delete' },
  //     { path: '/api/spaces/invites/accept', method: 'get' },
  //     { path: '/api/spaces/invites/accept', method: 'get' },
  //     { path: '/api/spaces/:id/invites', method: 'get' },
  //     { path: '/api/spaces/:id/invites', method: 'post' },
  //     { path: '/api/spaces/:id/invites', method: 'post' },
  //     { path: '/api/spaces/mounts', method: 'get' },
  //     { path: '/api/spaces/:id/mounts', method: 'post' },
  //     { path: '/api/spaces/:id/mounts', method: 'post' },
  //     { path: '/api/spaces/:id/mounts', method: 'delete' },
  //     { path: '/api/spaces/mounts', method: 'post' },
  //     { path: '/api/spaces/mounts', method: 'post' },
  //     { path: '/api/spaces/mounts', method: 'delete' },
  //     { path: '/api/spaces/:id/drive', method: 'get' },
  //     { path: '/api/spaces/:id/drive/history', method: 'get' },
  //     { path: '/api/spaces/:id/drive/history', method: 'get' },
  //     { path: '/api/spaces/:id/drive/stat', method: 'get' },
  //     { path: '/api/spaces/:id/drive/readdir', method: 'get' }
  //   ], 'returns a list of the APIs routes')
  //   next()
  // })

  context('GET /api/system/logs - sends the main logfile', async (assert, next) => {
    await register(request, { password: randomWords(1).pop() })
    const response = await request.get('/api/system/logs')
    assert.ok(response.buffered, 'returns a buffer stream')
    assert.same(response.headers['transfer-encoding'], 'chunked', 'encoding is chunked')
    assert.same(response.headers['content-disposition'], 'attachment; filename="cobox.log"', 'sends cobox.log file as attachment')
    next()
  })

  context('GET /api/system/logs - forbidden without authentication', async (assert, next) => {
    const response = await request.get('/api/system/logs')
    assert.same(response.statusCode, 401, 'is forbidden')
    next()
  })
})
