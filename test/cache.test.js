const { describe } = require('tape-plus')
const crypto = require('crypto')
const AsyncMap = require('../lib/cache')
const sinon = require('sinon')

describe('cache', (context) => {
  let cache

  context.beforeEach((c) => {
    cache = new AsyncMap(async (id, entry) => {
      await entry.ready()
    })
  })

  context('calls the callback', async (assert) => {
    const spy = sinon.spy()
    const entry1 = { id: 123456, ready: readySpy(spy, 100) }
    const entry2 = { id: 234567, ready: readySpy(spy, 100) }

    try {
      await cache.get(entry1.id)
    } catch (err) {
      assert.ok(err, 'throws error')
      assert.equal(err.message, 'not found: key does not exist')
    }

    cache.set(entry1.id, entry1)
    cache.set(entry1.id, entry2)
    let entry = await cache.get(entry1.id)

    assert.equal(entry, entry2, 'returns the changed entry')
    assert.ok(spy.calledOnce, 'ready called once')
  })
})

function sleep (timeout) {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout)
  })
}

function readySpy (spy, timeout) {
  return function () {
    return new Promise((res) => {
      setTimeout(() => {
        spy()
        res()
      }, timeout)
    })
  }
}

// const assert = require('assert')
// const crypto = require('crypto')
// const AsyncMap = require('./lib/cache')
// const sinon = require('sinon')
// const { on } = require('events')

// async function test () {
//   const map = new AsyncMap(async (id, entry) => {
//     await entry.ready()
//   })

//   const id1 = hex(crypto.randomBytes(4))
//   const spy1 = sinon.spy()
//   const entry1 = {
//     ready: () => new Promise((res) => {
//       spy1()
//       setTimeout(res, 1000)
//     }),
//     name: hex(crypto.randomBytes(4))
//   }

//   const id2 = hex(crypto.randomBytes(4))
//   const spy2 = sinon.spy()
//   const entry2 = {
//     ready: () => new Promise((res) => {
//       spy2()
//       setTimeout(res, 700)
//     }),
//     name: hex(crypto.randomBytes(4))
//   }

//   const id3 = hex(crypto.randomBytes(4))
//   const spy3 = sinon.spy()
//   const entry3 = {
//     ready: () => new Promise((res) => {
//       spy3()
//       setTimeout(res, 1000)
//     }),
//     name: hex(crypto.randomBytes(4))
//   }

//   try {
//     await map.get(id1)
//   } catch (err) {
//     assert.ok(err, 'failed to raise error')
//   }

//   let addCount = 0
//   map.on('add', (id, entry) => {
//     addCount += 1
//   })

//   let updateCount = 0
//   map.on('update', (id, entry) => {
//     updateCount += 1
//   })

//   let queuedCount = 0
//   map.on('queued', (id, entry) => {
//     queuedCount += 1
//   })

//   map.set(id1, entry1)
//   map.set(id2, entry2)
//   map.set(id1, entry1)

//   await sleep(2000)

//   assert.ok(spy1.calledTwice, 'not called twice')
//   assert.ok(spy2.calledOnce, 'not called once')

//   try {
//     map.set(id1, entry3)
//   }
//   catch (err) {
//     assert(!err, 'no set 2 error')
//   }

//   await sleep(1000)

//   assert.ok(spy3.calledOnce, 'not called once')
//   assert.equal(updateCount, 2, 'not two updates')
//   assert.equal(queuedCount, 4, 'not four queues')
//   assert.equal(addCount, 2, 'not two adds')

//   try {
//     const entry = await map.get(id1)
//     assert(entry === entry3, 'not equal')
//     await map.get(id1)
//     await map.get(id1)
//     await map.get(id1)
//   }
//   catch (err) {
//     assert.equal(!err, 'no error')
//   }

//   await sleep(1000)

//   assert.ok(spy3.calledOnce, 'not called once')

//   const keys = []
//   const values = []
//   for await (const [key, value] of map) {
//     keys.push(key)
//     values.push(value)
//   }
//   assert.deepEqual(keys, [id2, id1], 'not two entries')
//   assert.deepEqual(values, [entry2, entry3], 'not two entries')
// }

// function ready (timeout) {
//   return new Promise((res) => {
//     setTimeout(res, timeout)
//   })
// }

// function sleep (timeout) {
//   return new Promise((resolve) => {
//     setTimeout(resolve, timeout)
//   })
// }

// function hex (buf) {
//   if (Buffer.isBuffer(buf)) return buf.toString('hex')
//   return buf 
// }

// test()
