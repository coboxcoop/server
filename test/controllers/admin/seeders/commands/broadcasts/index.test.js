const { describe } = require('../../../../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const { hex } = require('../../../../../../util')
const NotFound = require('../../../../../../app/errors/not-found')
const { getReference } = require('../../../../../spec-helper')
const BroadcastCommandsController = require('../../../../../../app/controllers/admin/seeders/commands/broadcasts')

describe('controllers: admin/seeders/commands/broadcasts', (context) => {
  let api
  let cache
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    api.set('cache.seeders', cache)
    controller = new BroadcastCommandsController(api)
  })

  context('create: calls the start broadcast service', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const command = {
      type: 'command/announce',
      timestamp: Date.now(),
      version: '1.0.0',
      author: hex(crypto.randomBytes(32)),
      content: params
    }

    const startBroadcast = sinon.stub().resolves(command)
    api.set('commands.broadcast.start', startBroadcast)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { publish: sinon.stub().resolves(true) }
    }

    cache.set(getReference(session.identity.publicKey, seeder.address), seeder)

    const opts = { id: seeder.address, session }
    await controller.ready()
    const data = await controller.create(params, opts)
    assert.ok(startBroadcast.calledOnce, 'calls the start broadcast serivce')
    assert.same(data, command, 'returns the command')
    next()

    // // TOOD: move to service test
    // const command = {
    //   type: 'command/announce',
    //   timestamp: Date.now(),
    //   version: '1.0.0',
    //   author: hex(crypto.randomBytes(32)),
    //   content: params
    // }
    // assert.ok(seeder.log.publish.calledWith(command), 'publishes the command to the seeders log')
    // assert.same(data, command, 'returns the published command')
  })

  context('create: throws error when not found', async (assert, next) => {
    const startBroadcast = sinon.stub()
    api.set('commands.broadcast.start', startBroadcast)

    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { publish: sinon.stub().resolves(true) }
    }

    const opts = { id: seeder.address, session }
    await controller.ready()
    const create = async () => controller.create(params, opts)
    await assert.rejects(create, NotFound, 'not found error thrown')
    assert.ok(startBroadcast.notCalled, 'doesnt call start broadcast service')
    next()

    // TODO: move to service test
    // assert.ok(seeder.log.publish.notCalled, 'doesnt publish to seeder log')
  })

  context('destroy: calls the stop broadcast service', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const command = {
      type: 'command/hide',
      timestamp: Date.now(),
      version: '1.0.0',
      author: hex(crypto.randomBytes(32)),
      content: params
    }

    const startBroadcast = sinon.stub().resolves(command)
    api.set('commands.broadcast.start', startBroadcast)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }

    cache.set(getReference(session.identity.publicKey, seeder.address), seeder)

    const opts = { id: seeder.address, session }
    await controller.ready()
    const data = await controller.create(params, opts)
    assert.same(data, command, 'returns the command')
    assert.ok(startBroadcast.calledOnce, 'calls the start broadcast serivce')
    next()

    // // TOOD: move to service test
    // const command = {
    //   type: 'command/announce',
    //   timestamp: Date.now(),
    //   version: '1.0.0',
    //   author: hex(crypto.randomBytes(32)),
    //   content: params
    // }
    // assert.ok(seeder.log.publish.calledWith(command), 'publishes the command to the seeders log')
    // assert.same(data, command, 'returns the published command')
  })

  context('destroy: throws error when not found', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const stopBroadcast = sinon.stub()
    api.set('commands.broadcast.stop', stopBroadcast)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }

    const opts = { id: seeder.address, session }
    await controller.ready()
    const destroy = async () => controller.destroy(params, opts)
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    assert.ok(stopBroadcast.notCalled, 'doesnt call start broadcast service')
    next()

    // TODO: move to service test
    // assert.ok(seeder.log.publish.notCalled, 'doesnt publish to seeder log')
  })
})
