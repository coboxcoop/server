const { describe } = require('../../../../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const { hex } = require('../../../../../../util')
const NotFound = require('../../../../../../app/errors/not-found')
const { getReference } = require('../../../../../spec-helper')
const ReplicateCommandsController = require('../../../../../../app/controllers/admin/seeders/commands/replicates')

describe('controllers: admin/seeders/commands/replicates', (context) => {
  let api
  let cache
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    api.set('cache.seeders', cache)
    controller = new ReplicateCommandsController(api)
  })

  context('create: calls the start replicate service', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const command = {
      type: 'command/replicate',
      timestamp: Date.now(),
      version: '1.0.0',
      author: hex(crypto.randomBytes(32)),
      content: params
    }

    const startReplicate = sinon.stub().resolves(command)
    api.set('commands.replicate.start', startReplicate)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { publish: sinon.stub().resolves(true) }
    }

    cache.set(getReference(session.identity.publicKey, seeder.address), seeder)

    const opts = { id: seeder.address, session }
    await controller.ready()
    const data = await controller.create(params, opts)
    assert.ok(startReplicate.calledOnce, 'calls the start replicate serivce')
    assert.same(data, command, 'returns the command')
    next()

    // // TOOD: move to service test
    // const command = {
    //   type: 'command/replicate',
    //   timestamp: Date.now(),
    //   version: '1.0.0',
    //   author: hex(crypto.randomBytes(32)),
    //   content: params
    // }
    // assert.ok(seeder.log.publish.calledWith(command), 'publishes the command to the seeders log')
    // assert.same(data, command, 'returns the published command')
  })

  context('create: throws error when not found', async (assert, next) => {
    const startReplicate = sinon.stub()
    api.set('commands.replicate.start', startReplicate)

    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { publish: sinon.stub().resolves(true) }
    }

    const opts = { id: seeder.address, session }
    await controller.ready()
    const create = async () => controller.create(params, opts)
    await assert.rejects(create, NotFound, 'not found error thrown')
    assert.ok(startReplicate.notCalled, 'doesnt call start replicate service')
    next()

    // TODO: move to service test
    // assert.ok(seeder.log.publish.notCalled, 'doesnt publish to seeder log')
  })

  context('destroy: calls the stop replicate service', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const command = {
      type: 'command/unreplicate',
      timestamp: Date.now(),
      version: '1.0.0',
      author: hex(crypto.randomBytes(32)),
      content: params
    }

    const startReplicate = sinon.stub().resolves(command)
    api.set('commands.replicate.start', startReplicate)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }

    cache.set(getReference(session.identity.publicKey, seeder.address), seeder)

    const opts = { id: seeder.address, session }
    await controller.ready()
    const data = await controller.create(params, opts)
    assert.same(data, command, 'returns the command')
    assert.ok(startReplicate.calledOnce, 'calls the start replicate serivce')
    next()

    // // TOOD: move to service test
    // const command = {
    //   type: 'command/replicate',
    //   timestamp: Date.now(),
    //   version: '1.0.0',
    //   author: hex(crypto.randomBytes(32)),
    //   content: params
    // }
    // assert.ok(seeder.log.publish.calledWith(command), 'publishes the command to the seeders log')
    // assert.same(data, command, 'returns the published command')
  })

  context('destroy: throws error when not found', async (assert, next) => {
    const params = {
      name: 'the-chicken-coop',
      address: hex(crypto.randomBytes(32))
    }

    const stopReplicate = sinon.stub()
    api.set('commands.replicate.stop', stopReplicate)

    const seeder = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }

    const opts = { id: seeder.address, session }
    await controller.ready()
    const destroy = async () => controller.destroy(params, opts)
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    assert.ok(stopReplicate.notCalled, 'doesnt call start replicate service')
    next()

    // TODO: move to service test
    // assert.ok(seeder.log.publish.notCalled, 'doesnt publish to seeder log')
  })
})
