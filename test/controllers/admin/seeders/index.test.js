const { describe } = require('../../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const AdminSeedersController = require('../../../../app/controllers/admin/seeders')
const { setResources } = require('../../../spec-helper')
const { hex } = require('../../../../util')
const NotFound = require('../../../../app/errors/not-found')

describe('controllers: admin/seeders', (context) => {
  let api
  let cache
  let factory
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    setResources(cache, session.identity.publicKey)
    api.set('cache.seeders', cache)
    controller = new AdminSeedersController(api)
  })

  context('ready: loads dependencies from api', async (assert, next) => {
    await controller.ready()
    assert.same(controller.seeders, api.get('cache.seeders'), 'has seeders cache')
    assert.same(controller.factory, api.get('factory.seeders'), 'has seeders factory')
    next()
  })

  context('list: lists all seeders', async (assert, next) => {
    await controller.ready()
    const seeders = await controller.list({}, { session })
    assert.same(seeders, Array.from(cache.values()), 'gets all seeders')
    next()
  })

  context('create: creates a new seeder', async (assert, next) => {
    const params = { name: randomWords(1).pop(), address: hex(crypto.address()) }
    factory = { createSeeder: sinon.stub().resolves(params) }
    api.set('factory.seeders', factory)
    await controller.ready()
    const seeder = await controller.create(params, { session })
    assert.ok(factory.createSeeder.calledOnce, 'makes a call to seeder factory create')
    assert.same(params, seeder)
    next()
  })

  context('destroy: removes a seeder when found', async (assert, next) => {
    const params = { id: cache.values().next().value.address }
    factory = { deleteSeeder: sinon.stub().resolves(params) }
    api.set('factory.seeders', factory)
    await controller.ready()
    await controller.destroy(params, { session })
    assert.ok(factory.deleteSeeder.calledOnce, 'makes a call to seeder factory delete')
    next()
  })

  context('destroy: throws error when not found', async (assert, next) => {
    const params = { id: crypto.address() }
    factory = { deleteSeeder: sinon.stub().resolves(params) }
    api.set('factory.seeders', factory)
    await controller.ready()
    const destroy = () => controller.destroy(params, { session })
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    next()
  })
})
