const { describe } = require('../../../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const { setResources } = require('../../../../spec-helper')
const { hex } = require('../../../../../util')
const AdminSeederInvitesController = require('../../../../../app/controllers/admin/seeders/invites')
const NotFound = require('../../../../../app/errors/not-found')

describe('controllers: seeders/invites', (context) => {
  let api
  let cache
  let session
  let controller

  context.beforeEach(async (c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    setResources(cache, session.identity.publicKey)
    api.set('cache.seeders', cache)
    controller = new AdminSeederInvitesController(api)
  })

  context('create: create an invite', async (assert, next) => {
    const createInvite = sinon.stub().resolves({ type: 'peer/invite' })
    api.set('services.invites.create', createInvite)
    await controller.ready()
    const opts = { id: cache.values().next().value.address, session }
    const publicKey = hex(crypto.randomBytes(32))
    await controller.create({ publicKey }, opts)
    assert.ok(createInvite.calledOnce, 'calls create invite service')
    next()
  })

  context('create: throws an error when not found', async (assert, next) => {
    const createInvite = sinon.stub().resolves({ type: 'peer/invite' })
    api.set('services.invites.create', createInvite)
    await controller.ready()
    const opts = { id: hex(crypto.address()), session }
    const publicKey = hex(crypto.randomBytes(32))
    const create = async () => controller.create({ publicKey }, opts)
    await assert.rejects(create, NotFound, 'not found error thrown')
    assert.ok(createInvite.notCalled, 'doesnt call create invite service')
    next()
  })

  context('accept: creates a seeder', async (assert, next) => {
    const seeder = {
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32)),
      swarm: sinon.stub().resolves()
    }
    const accept = sinon.stub().resolves({ name: seeder.name, address: seeder.address })
    const createSeeder = sinon.stub().resolves(seeder)
    api.set('factory.seeders', { createSeeder })
    api.set('services.invites.accept', accept)
    await controller.ready()

    const code = `
dc9ec850387bd699ace4d2d6c00f9a36f
63fb19bf917c3797cb5595ece38e3bc81
3ef2c43e21529641d21e14d05ab71ae90
aaab925a853592e5f456a8a8034b9615c
b79d70a19dd05e08da1a03457c17e9963
eb1793e28a9c89bf65d229abb4d4aae0e
9270d3921d1645e1f68763aa3ece1f880
4c461df6829d23f465f23de457b857280
ce298c174a08c65d8d
`.replace(/\r?\n|\r/g, '')

    const data = await controller.accept({ code }, { session })

    assert.ok(accept.calledWith(code, session.identity), 'makes a call to the accept service')
    assert.ok(createSeeder.calledOnce, 'makes a call to seeder factory create')
    assert.ok(seeder.swarm.calledOnce, 'makes a call to start swarming')
    assert.equal(seeder, data, 'returns the seeder')

    next()
  })
})
