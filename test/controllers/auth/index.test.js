const { describe } = require('../../tape')
const { promises: fs } = require('fs')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const AuthenticationController = require('../../../app/controllers/auth')
const { hex } = require('../../../util')
const Forbidden = require('../../../app/errors/forbidden')

describe('controllers: authentication', (context) => {
  let api
  let controller
  let lock
  let unlock
  let authenticate
  let identity

  context.beforeEach((c) => {
    api = new Map()
    identity = crypto.keyPair()
    lock = sinon.stub()
    unlock = sinon.stub().resolves(identity)
    authenticate = sinon.stub()
    api.set('app.lock', lock)
    api.set('app.unlock', unlock)
    api.set('services.authenticate', authenticate)
    controller = new AuthenticationController(api)
  })

  context('ready: loads dependencies from api', async (assert, next) => {
    await controller.ready()
    assert.same(controller.unlock, api.get('app.unlock'), 'has unlock function')
    assert.same(controller.lock, api.get('app.lock'), 'has lock function')
    assert.same(controller.authenticate, api.get('services.authenticate'), 'has authenticate function')
    next()
  })

  context('create: correct password, authenticates, assigns a session and unlocks the api', async (assert, next) => {
    const opts = { session: {} }
    const params = { password: 'the-real-password' }
    await controller.ready()
    await controller.create(params, opts)
    assert.ok(unlock.calledOnce, 'calls the unlock function')
    assert.same(opts.session.identity, identity, 'assigns an identity to the session object')
    next()
  })

  context('create: incorrect password, throws a forbidden error', async (assert, next) => {
    authenticate = sinon.stub().throws(new Forbidden('incorrect password'))
    api.set('services.authenticate', authenticate)
    const params = { password: 'incorrect-password' }
    await controller.ready()
    const create = async () => controller.create(params)
    await assert.rejects(create, Forbidden, 'forbidden error thrown')
    assert.ok(unlock.notCalled, 'unlock not called')
    next()
  })

  context('destroy: destroys session and locks the api', async (assert, next) => {
    const opts = { session: { identity: { publicKey: hex(crypto.randomBytes(4)) } } }
    const params = { password: 'incorrect-password' }
    await controller.ready()
    await controller.destroy(params, opts)
    assert.ok(lock.calledOnce, 'calls the lock function')
    assert.notok(opts.session.identity, 'closes the session')
    next()
  })
})
