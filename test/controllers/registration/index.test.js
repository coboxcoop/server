const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const RegistrationController = require('../../../app/controllers/registration')
const Forbidden = require('../../../app/errors/forbidden')

describe('controllers: registration', (context) => {
  let api
  let controller
  let unlock
  let identity
  let passwordHashExists
  let savePasswordHash
  let saveEncryptedParentKey

  context.beforeEach((c) => {
    api = new Map()
    identity = crypto.keyPair()
    unlock = sinon.stub().resolves(identity)
    passwordHashExists = sinon.stub().resolves(false)
    savePasswordHash = sinon.stub()
    saveEncryptedParentKey = sinon.stub()
    api.set('setup.unlock', unlock)
    api.set('services.authentication.password-exists', passwordHashExists)
    api.set('services.authentication.save-password', savePasswordHash)
    api.set('services.encryption.save-parent-key', saveEncryptedParentKey)
    controller = new RegistrationController(api)
  })

  context('ready: loads dependencies from api', async (assert, next) => {
    await controller.ready()
    assert.same(controller.unlock, api.get('setup.unlock'), 'has an unlock function')
    assert.same(controller.passwordHashExists, api.get('services.authentication.password-exists'), 'has a passwordHashExists function')
    assert.same(controller.savePasswordHash, api.get('services.authentication.save-password'), 'has a savePasswordHash function')
    assert.same(controller.saveEncryptedParentKey, api.get('services.encryption.save-parent-key'), 'has a saveEncryptedParentKey function')
    next()
  })

  context('create: authenticates, assigns a session and unlocks the api', async (assert, next) => {
    const opts = { session: {} }
    const params = { password: 'my-super-secret-password' }
    await controller.ready()
    await controller.create(params, opts)
    assert.ok(passwordHashExists.calledOnce, 'calls the password exists service')
    assert.ok(savePasswordHash.calledOnce, 'calls the savePasswordHash service')
    assert.ok(saveEncryptedParentKey.calledOnce, 'calls the saveEncryptedParentKey service')
    assert.ok(unlock.calledOnce, 'calls the unlock service')
    assert.same(opts.session.identity, identity, 'assigns an identity to the session object')
    next()
  })

  context('create: password hash already exists, throws a forbidden error', async (assert, next) => {
    passwordHashExists = sinon.stub().resolves(true)
    api.set('services.authentication.password-exists', passwordHashExists)
    const opts = { session: {} }
    const params = { password: 'my-super-secret-password' }
    await controller.ready()
    const create = async () => controller.create(params, opts)
    await assert.rejects(create, Forbidden, 'forbidden error thrown')
    assert.ok(passwordHashExists.calledOnce, 'calls the password exists service')
    assert.ok(savePasswordHash.notCalled, 'savePasswordHash not called')
    assert.ok(saveEncryptedParentKey.notCalled, 'saveEncryptedParentKey not called')
    assert.same(opts.session, {}, 'session isnt set')
    next()
  })
})
