const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const ReplicatorsController = require('../../../app/controllers/replicators')
const { setResources } = require('../../spec-helper')
const { hex } = require('../../../util')
const NotFound = require('../../../app/errors/not-found')

describe('controllers: replicators', (context) => {
  let api
  let cache
  let factory
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    setResources(cache, session.identity.publicKey)
    api.set('cache.replicators', cache)
    controller = new ReplicatorsController(api)
  })

  context('list: lists all replicators', async (assert, next) => {
    await controller.ready()
    const replicators = await controller.list({}, { session })
    assert.same(replicators, Array.from(cache.values()), 'gets all replicators')
    next()
  })

  context('create: creates a new replicator', async (assert, next) => {
    const params = { name: randomWords(1).pop(), address: hex(crypto.address()) }
    factory = { createReplicator: sinon.stub().resolves(params) }
    api.set('factory.replicators', factory)
    await controller.ready()
    const replicator = await controller.create(params, { session })
    assert.ok(factory.createReplicator.calledOnce, 'makes a call to replicator factory create')
    assert.same(params, replicator)
    next()
  })

  context('destroy: removes a replicator when found', async (assert, next) => {
    const params = { id: cache.values().next().value.address }
    factory = { deleteReplicator: sinon.stub().resolves(params) }
    api.set('factory.replicators', factory)
    await controller.ready()
    await controller.destroy(params, { session })
    assert.ok(factory.deleteReplicator.calledOnce, 'makes a call to replicator factory delete')
    next()
  })

  context('destroy: throws error when not found', async (assert, next) => {
    const params = { id: crypto.address() }
    factory = { deleteReplicator: sinon.stub().resolves(params) }
    api.set('factory.replicators', factory)
    await controller.ready()
    const destroy = () => controller.destroy(params, { session })
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    next()
  })
})
