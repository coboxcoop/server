const { describe } = require('../../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const { setResources } = require('../../../spec-helper')
const { hex } = require('../../../../util')
const SpaceConnectionsController = require('../../../../app/controllers/spaces/connections')
const NotFound = require('../../../../app/errors/not-found')

describe('controllers: spaces/connections', (context) => {
  let api
  let cache
  let stubs
  let session
  let controller

  context.beforeEach(async (c) => {
    api = new Map()
    cache = new Map()
    stubs = {
      swarm: sinon.stub().returns(true),
      unswarm: sinon.stub().returns(true)
    }
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    setResources(cache, session.identity.publicKey, stubs)
    api.set('cache.spaces', cache)
    controller = new SpaceConnectionsController(api)
    await controller.ready()
  })

  context('create: start swarming', async (assert, next) => {
    const params = { id: cache.values().next().value.address }
    await controller.create(params, { session })
    assert.ok(stubs.swarm.calledOnce, 'calls swarm')
    next()
  })

  context('create: throws an error when not found', async (assert, next) => {
    const params = { id: hex(crypto.address()) }
    const create = async () => controller.create(params, { session })
    await assert.rejects(create, NotFound, 'not found error thrown')
    next()
  })

  context('destroy: stop swarming', async (assert, next) => {
    const params = { id: cache.values().next().value.address }
    await controller.destroy(params, { session })
    assert.ok(stubs.unswarm.calledOnce, 'calls unswarm')
    next()
  })

  context('destroy: throws an error when not found', async (assert, next) => {
    const params = { id: hex(crypto.address()) }
    const destroy = async () => controller.destroy(params, { session })
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    next()
  })
})
