const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const SpacesController = require('../../../app/controllers/spaces')
const { setResources } = require('../../spec-helper')
const { hex } = require('../../../util')
const NotFound = require('../../../app/errors/not-found')

describe('controllers: spaces', (context) => {
  let api
  let cache
  let factory
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    setResources(cache, session.identity.publicKey)
    api.set('cache.spaces', cache)
    controller = new SpacesController(api)
  })

  context('ready: loads dependencies from api', async (assert, next) => {
    await controller.ready()
    assert.same(controller.spaces, api.get('cache.spaces'), 'has spaces cache')
    assert.same(controller.factory, api.get('factory.spaces'), 'has spaces factory')
    next()
  })

  context('list: lists all spaces', async (assert, next) => {
    await controller.ready()
    const spaces = await controller.list({}, { session })
    assert.same(spaces, Array.from(cache.values()), 'gets all spaces')
    next()
  })

  context('create: creates a new space', async (assert, next) => {
    const params = { name: randomWords(1).pop(), address: hex(crypto.address()) }
    factory = { createSpace: sinon.stub().resolves(params) }
    api.set('factory.spaces', factory)
    await controller.ready()
    const space = await controller.create(params, { session })
    assert.ok(factory.createSpace.calledOnce, 'makes a call to space factory create')
    assert.same(params, space)
    next()
  })

  context('destroy: removes a space when found', async (assert, next) => {
    const params = { id: cache.values().next().value.address }
    factory = { deleteSpace: sinon.stub().resolves(params) }
    api.set('factory.spaces', factory)
    await controller.ready()
    await controller.destroy(params, { session })
    assert.ok(factory.deleteSpace.calledOnce, 'makes a call to space factory delete')
    next()
  })

  context('destroy: throws error when not found', async (assert, next) => {
    const params = { id: crypto.address() }
    factory = { deleteSpace: sinon.stub().resolves(params) }
    api.set('factory.spaces', factory)
    await controller.ready()
    const destroy = () => controller.destroy(params, { session })
    await assert.rejects(destroy, NotFound, 'not found error thrown')
    next()
  })
})
