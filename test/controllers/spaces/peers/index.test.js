const { describe } = require('../../../tape')
const crypto = require('@coboxcoop/crypto')
const randomWords = require('random-words')
const SpacesController = require('../../../../app/controllers/spaces/peers')
const { hex } = require('../../../../util')
const NotFound = require('../../../../app/errors/not-found')
const { mockReadStream, getReference } = require('../../../spec-helper')

describe('controllers: spaces', (context) => {
  let api
  let cache
  let session
  let controller

  context.beforeEach((c) => {
    api = new Map()
    cache = new Map()
    session = { identity: { publicKey: hex(crypto.randomBytes(4)) } }
    api.set('cache.spaces', cache)
    controller = new SpacesController(api)
  })

  context('list: lists all peers in a space log', async (assert, next) => {
    const peerAbouts = [{
      type: 'peer/about',
      timestamp: Date.now(),
      version: '1.0.0',
      author: hex(crypto.randomBytes(32)),
      content: { name: 'pagmie' }
    }]

    const space = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { read: mockReadStream(peerAbouts) }
    }

    cache.set(getReference(session.identity.publicKey, space.address), space)

    const params = { id: space.address }
    await controller.ready()
    const data = await controller.list(params, { session })
    assert.same(data, peerAbouts, 'gets all space peers')
    next()
  })

  context('list: throws error when not found', async (assert, next) => {
    const space = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop(),
      log: { read: mockReadStream([]) }
    }

    const params = { id: space.address }
    await controller.ready()
    const list = () => controller.list(params, { session })
    await assert.rejects(list, NotFound, 'not found error thrown')
    next()
  })
})
