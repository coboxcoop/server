const { describe } = require('../../tape')
const sinon = require('sinon')
const path = require('path')
const { promises: fs } = require('fs')
const SystemController = require('../../../app/controllers/system')
const { pick } = require('../../../util')
const { cleanup, tmp } = require('../../spec-helper')

const details = pick(require('../../../package.json'), ['name', 'description', 'version', 'author', 'license', 'bugs'])

describe('controllers: system', (context) => {
  let api
  let controller
  let routes

  context.beforeEach(async (c) => {
    api = new Map()
    controller = new SystemController(api)
    routes = [{ path: '/api/routes', method: 'get' }]
    api.set('controllers.system', controller)
    api.set('app.settings', { logsPath: tmp() })
    api.set('setup.fetch-routes', sinon.stub().resolves(routes))
    api.set('app.details', details)
    await controller.ready()
  })

  context('show: gets system information', async (assert, next) => {
    const data = await controller.show()
    assert.deepEqual(data, details, 'returns system information')
    next()
  })

  context('routes: gets system routes', async (assert, next) => {
    const data = await controller.routes()
    assert.deepEqual(data, routes, 'returns system routes')
    next()
  })

  context('logs: returns logs path', async (assert, next) => {
    const settings = api.get('app.settings')
    await fs.writeFile(path.join(settings.logsPath, 'main.log'), 'testing')
    const stream = await controller.logs()
    const data = []
    for await (const chunk of stream) {
      data.push(chunk)
    }
    assert.same(data, [Buffer.from('testing')], 'returns the logs path')
    await cleanup(settings.logsPath)
    next()
  })
})
