const { describe } = require('../tape')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const memdb = require('level-mem')
const randomWords = require('random-words')
const { shortHex, hex } = require('../../util')
const IdentityFactory = require('../../app/factories/identity')

describe('factories', (context) => {
  context('create an identity', async (assert, next) => {
    const db = memdb({ valueEncoding: 'json' })
    const cache = new Map()
    const keyPair = crypto.keyPair()
    const name = randomWords(1).pop()
    const api = new Map()
    const deriveKeyPair = sinon.stub().returns(keyPair)

    api.set('db.identities', db)
    api.set('cache.identities', cache)
    api.set('setup.encryption-key-pair', deriveKeyPair)

    const seed = hex(crypto.randomBytes(8))
    const factory = new IdentityFactory(api)
    await factory.ready()

    const identity = await factory.createIdentity({ seed, name })
    assert.ok(deriveKeyPair.calledOnce, 'derives a keypair')
    assert.same(identity.name, name, 'returns a name')
    assert.same(identity.publicKey, hex(keyPair.publicKey), 'returns a public key hex string')
    assert.same(identity.secretKey, hex(keyPair.secretKey), 'returns a secret key hex string')
    assert.same(await db.get(identity.publicKey), { name, seed: hex(seed) }, 'persists name and seed to database')
    assert.same(cache.get(shortHex(identity.publicKey)), identity)
    next()
  })

  context('update an identity', async (assert, next) => {
    const db = memdb({ valueEncoding: 'json' })
    const cache = new Map()
    const keyPair = crypto.keyPair()
    const oldName = randomWords(1).pop()
    const newName = randomWords(1).pop()
    const seed = hex(crypto.randomBytes(4))
    const api = new Map()

    api.set('db.identities', db)
    api.set('cache.identities', cache)

    await db.put(hex(keyPair.publicKey), {
      seed,
      name: oldName
    })

    await cache.set(shortHex(keyPair.publicKey), {
      publicKey: hex(keyPair.publicKey),
      secretKey: hex(keyPair.secretKey),
      name: oldName
    })

    const factory = new IdentityFactory(api)
    await factory.ready()

    const identity = await factory.updateIdentity({
      name: newName,
      identity: {
        publicKey: hex(keyPair.publicKey),
        secretKey: hex(keyPair.secretKey)
      }
    })

    assert.same(identity.name, newName, 'returns a name')
    assert.same(identity.publicKey, hex(keyPair.publicKey), 'returns a public key hex string')
    assert.same(identity.secretKey, hex(keyPair.secretKey), 'returns a secret key hex string')

    assert.same(await db.get(identity.publicKey), {
      name: newName,
      seed: hex(seed)
    }, 'persists new name and existing seed to database')

    assert.same(cache.get(shortHex(identity.publicKey)), {
      ...identity,
      name: newName
    })

    next()
  })

  context('delete an identity', async (assert, next) => {
    const db = memdb({ valueEncoding: 'json' })
    const cache = new Map()
    const keyPair = crypto.keyPair()
    const api = new Map()
    const spaceFactory = { deleteSpace: sinon.stub() }
    const replFactory = { deleteReplicator: sinon.stub() }
    const seederFactory = { deleteSeeder: sinon.stub() }

    api.set('db.identities', db)
    api.set('cache.identities', cache)
    api.set('cache.spaces', new Map())
    api.set('cache.replicators', new Map())
    api.set('cache.seeders', new Map())
    api.set('factory.spaces', spaceFactory)
    api.set('factory.replicators', replFactory)
    api.set('factory.seeder', seederFactory)

    const factory = new IdentityFactory(api)
    await factory.ready()

    const identity = await factory.deleteIdentity({ identity: keyPair })
    assert.notok(cache.get(shortHex(identity.publicKey)), 'identity removed from cache')
    assert.rejects(() => db.get(hex(identity.publicKey)), 'identity was deleted from database')
    next()
  })
})
