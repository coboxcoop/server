const { describe } = require('../tape')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const memdb = require('level-mem')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const randomWords = require('random-words')
const { pick, shortHex, hex, hexify } = require('../../util')
const { getReference, tmp, cleanup } = require('../spec-helper')
const SpaceFactory = require('../../app/factories/space')

describe('factories', (context) => {
  context('create a space', async (assert, next) => {
    const storage = tmp()
    const api = new Map()
    const corestore = new Corestore(RAM)
    const db = memdb({ valueEncoding: 'json' })
    const cache = new Map()
    const keyPair = crypto.keyPair()
    const identity = hexify(keyPair)
    const deriveKeyPair = sinon.stub().returns(keyPair)

    api.set('app.settings', { storage })
    api.set('db.spaces', db)
    api.set('cache.spaces', cache)
    api.set('dat.store', corestore)
    api.set('setup.signing-key-pair', deriveKeyPair)

    const factory = new SpaceFactory(api)
    await factory.ready()

    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }
    const space = await factory.createSpace({ ...params, identity })

    assert.same(deriveKeyPair.callCount, 3, 'derives keypairs')
    assert.same(space.name, params.name, 'returns a name')
    assert.same(hex(space.address), params.address, 'returns an address')
    const ref = getReference(identity.publicKey, space.address)
    assert.same(await db.get(ref), pick(params, ['name', 'address']), 'persists space detils to database')
    assert.same(cache.get(ref), space, 'caches the space')
    await cleanup(storage)
    next()
  })

  context('destroy a space', async (assert, next) => {
    const storage = tmp()
    const api = new Map()
    const corestore = new Corestore(RAM)
    const db = memdb({ valueEncoding: 'json' })
    const cache = new Map()
    const keyPair = crypto.keyPair()
    const identity = hexify(keyPair)
    const deriveKeyPair = sinon.stub().returns(keyPair)

    api.set('app.settings', { storage })
    api.set('db.spaces', db)
    api.set('cache.spaces', cache)
    api.set('dat.store', corestore)
    api.set('setup.signing-key-pair', deriveKeyPair)

    const factory = new SpaceFactory(api)
    await factory.ready()

    const params = {
      name: randomWords(1).pop(),
      address: hex(crypto.randomBytes(32)),
      encryptionKey: hex(crypto.randomBytes(32))
    }
    const space = await factory.createSpace({ ...params, identity })
    const ref = getReference(identity.publicKey, space.address)

    await factory.deleteSpace(space, identity)

    assert.rejects(() => db.get(ref), 'deletes space details from database')
    assert.notok(cache.get(ref), 'removes the space from the cache')

    await cleanup(storage)
    next()
  })
})
