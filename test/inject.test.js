const { describe } = require('./tape')
const express = require('express')
const inject = require('../app/inject')
const { pick } = require('../util')
const { tmp, cleanup } = require('./spec-helper')

const details = pick(require('../package.json'), ['name', 'description', 'version', 'author', 'license', 'bugs'])

describe('api: inject', (context) => {
  context('has required dependencies', async (assert, next) => {
    const app = {
      app: express(),
      settings: {
        storage: tmp(),
        mount: tmp(),
        port: 1234,
        tmp: tmp()
      }
    }

    const api = inject(app)

    assert.ok(api.get('db.main'), 'db.main exists')
    assert.ok(api.get('db.spaces'), 'db.spaces exists')
    assert.ok(api.get('db.replicators'), 'db.replicators exists')
    assert.ok(api.get('db.seeders'), 'db.seeders exists')
    assert.ok(api.get('db.identities'), 'db.identities exists')
    assert.notok(api.get('dat.store'), 'dat.store is null')
    assert.notok(api.get('dat.networker'), 'dat.networker is null')
    assert.ok(api.get('factory.spaces'), 'factory.space exists')
    assert.ok(api.get('factory.replicators'), 'factory.replicator exists')
    assert.ok(api.get('factory.seeders'), 'factory.seeder exists')
    assert.ok(api.get('factory.identities'), 'factory.identity exists')
    assert.ok(api.get('app.express'), 'app.express exists')
    assert.ok(api.get('app.router'), 'app.router exists')
    assert.same(api.get('app.settings'), app.settings, 'app.settings are passed settings')
    assert.same(api.get('app.details'), details, 'app.details selects details from package.json')
    assert.ok(api.get('app.events'), 'app.events exists')
    assert.ok(api.get('app.start'), 'app.start function exists')
    assert.ok(api.get('app.stop'), 'app.stop function exists')
    assert.ok(api.get('app.unlock'), 'app.unlock function exists')
    assert.ok(api.get('app.lock'), 'app.lock function exists')
    assert.ok(api.get('setup.events'), 'setup.events exists')
    assert.ok(api.get('setup.fetch-routes'), 'setup.fetch-routes function exists')
    assert.ok(api.get('services.backup.pack'), 'services.backup.pack function exists')
    assert.ok(api.get('services.backup.unpack'), 'services.backup.unpack function exists')
    assert.ok(api.get('services.invites.pack'), 'services.invites.pack function exists')
    assert.ok(api.get('services.invites.unpack'), 'services.invites.unpack function exists')
    assert.ok(api.get('services.invites.seal'), 'services.invites.seal function exists')
    assert.ok(api.get('services.invites.open'), 'services.invites.open function exists')
    assert.ok(api.get('services.authenticate'), 'services.authenticate function exists')
    assert.ok(api.get('services.authentication.password-exists'), 'services.authentication.password-exists function exists')
    assert.ok(api.get('services.authentication.save-password'), 'services.authentication.save-password function exists')
    assert.ok(api.get('services.encryption.save-parent-key'), 'services.encryption.save-parent-key function exists')
    assert.ok(api.get('services.encryption.get-parent-key'), 'services.encryption.get-parent-key function exists')
    assert.ok(api.get('commands.replicate.start'), 'commands.replicate.start function exists')
    assert.ok(api.get('commands.replicate.stop'), 'commands.replicate.stop function exists')
    assert.ok(api.get('commands.broadcast.start'), 'commands.broadcast.start function exists')
    assert.ok(api.get('commands.broadcast.stop'), 'commands.broadcast.start function exists')
    assert.ok(api.get('cache.identities'), 'cache.identities exists')
    assert.same(api.get('cache.identities').constructor.name, 'Map', 'cache.identities is a Map')
    assert.ok(api.get('cache.spaces'), 'cache.spaces exists')
    assert.same(api.get('cache.spaces').constructor.name, 'AsyncMap', 'cache.spaces is an AsyncMap')
    assert.ok(api.get('cache.replicators'), 'cache.replicators exists')
    assert.same(api.get('cache.replicators').constructor.name, 'AsyncMap', 'cache.replicators is an AsyncMap')
    assert.ok(api.get('cache.seeders'), 'cache.seeders exists')
    assert.same(api.get('cache.seeders').constructor.name, 'AsyncMap', 'cache.seeders is an AsyncMap')
    assert.ok(api.get('controllers.admin.seeders'), 'controllers.admin.seeders exists')
    assert.ok(api.get('controllers.admin.seeders.invites'), 'controllers.admin.seeders.invites exists')
    assert.ok(api.get('controllers.admin.seeders.connections'), 'controllers.admin.seeders.connections exists')
    assert.ok(api.get('controllers.admin.seeders.peers'), 'controllers.admin.seeders.peers exists')
    assert.ok(api.get('controllers.auth'), 'controllers.auth exists')
    assert.ok(api.get('controllers.commands.broadcast'), 'controllers.commands.broadcast exists')
    assert.ok(api.get('controllers.commands.replicate'), 'controllers.commands.replicate exists')
    assert.ok(api.get('controllers.drive'), 'controllers.drive exists')
    assert.ok(api.get('controllers.export'), 'controllers.expost exists')
    assert.ok(api.get('controllers.identities'), 'controllers.identities exists')
    assert.ok(api.get('controllers.import'), 'controllers.import exists')
    assert.ok(api.get('controllers.events'), 'controllers.events exists')
    assert.ok(api.get('controllers.mounts'), 'controllers.mounts exists')
    assert.ok(api.get('controllers.registration'), 'controllers.registration exists')
    assert.ok(api.get('controllers.replicators'), 'controllers.replicators exists')
    assert.ok(api.get('controllers.replicators.connections'), 'controllers.replicators.connections exists')
    assert.ok(api.get('controllers.spaces'), 'controllers.spaces exists')
    assert.ok(api.get('controllers.spaces.invites'), 'controllers.spaces.invites exists')
    assert.ok(api.get('controllers.spaces.connections'), 'controllers.spaces.connections exists')
    assert.ok(api.get('controllers.spaces.peers'), 'controllers.spaces.peers exists')
    assert.ok(api.get('controllers.system'), 'controllers.system exists')
    assert.ok(api.get('helpers.upload'), 'helpers.upload exists')

    await cleanup([
      app.settings.storage,
      app.settings.mount,
      app.settings.tmp
    ])

    next()
  })
})
