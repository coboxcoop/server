const bcrypt = require('bcrypt')
const { promises: fs } = require('fs')
const path = require('path')
const { describe } = require('../../tape')
const { tmp, cleanup } = require('../../spec-helper')
const setupSavePasswordHash = require('../../../app/services/authentication/save-password-hash')

describe('service: authentication/save-password-hash', (context) => {
  context('saves the password hash when one doesnt yet exist', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const password = 'my-super-secret-password'
    const service = setupSavePasswordHash(api)
    await service(password)
    const passwordHash = await fs.readFile(path.join(storage, 'password_hash'), 'utf-8')
    await bcrypt.compare(password, passwordHash)
    await cleanup(storage)
    next()
  })

  context('throws an error when password hash already exists', async (assert, next) => {
    // TODO
    next()
  })
})
