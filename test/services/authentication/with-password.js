const bcrypt = require('bcrypt')
const path = require('path')
const { describe } = require('../../tape')
const { promises: fs } = require('fs')
const { tmp, cleanup } = require('../../spec-helper')
const NotFound = require('../../../app/errors/not-found')
const Forbidden = require('../../../app/errors/forbidden')
const setupAuthenticateWithPassword = require('../../../app/services/authentication/with-password')

describe('serivces: authentication/with-password', (context) => {
  context('correct password does not throw', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const service = setupAuthenticateWithPassword(api)
    const password = 'my-super-secret-password'
    await fs.writeFile(path.join(storage, 'password_hash'), await bcrypt.hash(password, 10),'utf-8')
    const result = await service(password)
    assert.ok(result, 'authenticates successfully')

    await cleanup(storage)
    next()
  })

  context('missing password throws not found', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const service = setupAuthenticateWithPassword(api)
    await assert.rejects(service, NotFound, 'throws when password is empty')
    await cleanup(storage)
    next()
  })

  context('missing password hash throws not found', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const service = setupAuthenticateWithPassword(api)
    const password = 'my-super-secret-password'
    const fail = async () => service(password)
    await assert.rejects(fail, NotFound, 'throws when password hash is not saved')
    await cleanup(storage)
    next()
  })

  context('wrong password throws forbidden', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const service = setupAuthenticateWithPassword(api)
    const password = 'my-super-secret-password'
    const passwordAttempt = 'my-super-wrong-secret-password'
    await fs.writeFile(path.join(storage, 'password_hash'), await bcrypt.hash(password, 10))
    const fail = async () => service(passwordAttempt)
    await assert.rejects(fail, Forbidden, 'throws when incorrect password')
    await cleanup(storage)
    next()
  })
})
