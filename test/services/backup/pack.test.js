const { describe } = require('../../tape')
const crypto = require('@coboxcoop/crypto')
const concat = require('concat-stream')
const hCrypto = require('hypercore-crypto')
const pumpify = require('pumpify')
const tar = require('tar-stream')
const zlib = require('zlib')
const { promises: fs } = require('fs')
const path = require('path')
const randomWords = require('random-words')

const setupPack = require('../../../app/services/backup/pack')
const setupGetParentKey = require('../../../app/services/encryption/get-parent-key')
const setupSaveParentKey = require('../../../app/services/encryption/save-parent-key')
const AsyncMap = require('../../../lib/cache')
const { tmp, cleanup, getReference } = require('../../spec-helper')
const { hex, shortHex } = require('../../../util')

describe('services: backup/pack', (context) => {
  context('returns a tarball containing the necessary data for a backup', async (assert, done) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })
    const identities = new Map()
    const spaces = new AsyncMap()
    const replicators = new AsyncMap()
    const seeders = new AsyncMap()
    api.set('cache.identities', identities)
    api.set('cache.spaces', spaces)
    api.set('cache.replicators', replicators)
    api.set('cache.seeders', seeders)
    api.set('services.encryption.get-parent-key', setupGetParentKey(api))
    // seed an identity keypair
    let identity = hCrypto.keyPair()
    identity.publicKey = hex(identity.publicKey)
    identity.secretKey = hex(identity.secretKey)
    identity.name = randomWords(1).pop()
    identities.set(shortHex(identity.publicKey), identity)
    // seed a space
    let address = hex(crypto.address())
    const spaceEncryptionKey = crypto.encryptionKey()
    const space = { address: address, name: randomWords(1).pop(), storage: path.join(storage, address) }
    await fs.mkdir(space.storage, { recursive: true })
    await fs.writeFile(path.join(space.storage, 'encryption_key'), spaceEncryptionKey)
    await spaces.set(getReference(identity.publicKey, space.address), space)
    // and a seeder
    address = hex(crypto.address())
    const seederEncryptionKey = crypto.encryptionKey()
    const seeder = { address: address, name: randomWords(1).pop(), storage: path.join(storage, address) }
    await fs.mkdir(seeder.storage, { recursive: true })
    await fs.writeFile(path.join(seeder.storage, 'encryption_key'), seederEncryptionKey)
    await seeders.set(getReference(identity.publicKey, seeder.address), seeder)
    // and a replicator
    address = hex(crypto.address())
    const replicator = { address: address, name: randomWords(1).pop() }
    await replicators.set(getReference(identity.publicKey, replicator.address), replicator)
    // generate a parent key
    const password = 'my-super-secret-password'
    const saveParentKey = setupSaveParentKey(api)
    await saveParentKey(crypto.masterKey(), password)
    // call the service
    const service = setupPack(api)
    const backup = await service({ password })
    // now unpack the result and recompile into something checkable
    const extract = tar.extract()
    const files = {}
    extract.on('entry', onEntry)
    extract.on('finish', check)
    pumpify(backup, zlib.createUnzip(), extract)

    function onEntry (header, stream, next) {
      stream.pipe(concat((contents) => {
        files[header.name] = JSON.parse(contents.toString('utf-8'))
        return next()
      }))
    }

    async function check () {
      assert.same(files['VERSION'], 2, 'contains a version file')
      assert.same(files['backup.cobox.json'], {
        settings: api.get('app.settings'),
        parentKey: hex(api.get('services.encryption.get-parent-key')(password)),
        spaces: {
          [getReference(identity.publicKey, space.address)]: {
            name: space.name,
            address: space.address,
            encryptionKey: hex(spaceEncryptionKey)
          }
        },
        replicators: {
          [getReference(identity.publicKey, replicator.address)]: replicator
        },
        seeders: {
          [getReference(identity.publicKey, seeder.address)]: {
            name: seeder.name,
            address: seeder.address,
            encryptionKey: hex(seederEncryptionKey)
          }
        },
        identities: {
          [shortHex(identity.publicKey)]: identity
        }
      }, 'contains a backup.cobox.json file')
      await cleanup(storage)
      done()
    }
  })
})
