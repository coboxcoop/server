const { AssertionError } = require('assert')
const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const randomWords = require('random-words')
const { hex } = require('../../../util')
const { encodings } = require('@coboxcoop/schemas')
const setupStartReplicate = require('../../../app/services/commands/start-replicate')
const Replicate = encodings.command.replicate

describe('services: commands/start-replicate', (context) => {
  let clock

  context.beforeEach((c) => {
    clock = sinon.useFakeTimers((new Date()).getTime())
  })

  context.afterEach((c) => {
    clock.restore()
  })

  context('it publishes a command/replicate message to the seeder log', async (assert, next) => {
    const seeder = { log: { publish: sinon.stub() } }
    const params = {
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }
    const identity = { publicKey: hex(crypto.randomBytes(32)) }
    const service = setupStartReplicate()
    const data = await service(seeder, params, identity)
    const command = {
      type: 'command/replicate',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content: params
    }
    assert.ok(seeder.log.publish.calledOnceWith(command, { valueEncoding: Replicate }), 'calls publish with args')
    assert.same(data, command, 'returns the published command')
    next()
  })

  context('it throws when params are invalid', async (assert, next) => {
    const seeder = { log: { publish: sinon.stub() } }
    const params = { fakeyfakefake: 'this is gonna throw an error' }
    const identity = { publicKey: hex(crypto.randomBytes(32)) }
    const service = setupStartReplicate()
    const fail = async () => service(seeder, params, identity)
    await assert.rejects(fail, AssertionError, 'throws an assertion error')
    assert.ok(seeder.log.publish.notCalled, 'publish isnt called')
    next()
  })

  context('it throws when identity.publicKey is missing', async (assert, next) => {
    const seeder = { log: { publish: sinon.stub() } }
    const params = { address: hex(crypto.randomBytes(32)), name: randomWords(1).pop() }
    const identity = {}
    const service = setupStartReplicate()
    const fail = async () => service(seeder, params, identity)
    await assert.rejects(fail, AssertionError, 'throws an assertion error')
    assert.ok(seeder.log.publish.notCalled, 'publish isnt called')
    next()
  })
})
