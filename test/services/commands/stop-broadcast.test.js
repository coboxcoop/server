const { AssertionError } = require('assert')
const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('hypercore-crypto')
const { hex } = require('../../../util')
const { encodings } = require('@coboxcoop/schemas')
const setupStartBroadcast = require('../../../app/services/commands/stop-broadcast')
const Hide = encodings.command.hide

describe('services: commands/stop-broadcast', (context) => {
  let clock

  context.beforeEach((c) => {
    clock = sinon.useFakeTimers((new Date()).getTime())
  })

  context.afterEach((c) => {
    clock.restore()
  })

  context('it publishes a command/hide message to the seeder log', async (assert, next) => {
    const seeder = { log: { publish: sinon.stub() } }
    const identity = { publicKey: hex(crypto.randomBytes(32)) }
    const service = setupStartBroadcast()
    const data = await service(seeder, {}, identity)
    const command = {
      type: 'command/hide',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey
    }
    assert.ok(seeder.log.publish.calledOnceWith(command, { valueEncoding: Hide }), 'calls publish with args')
    assert.same(data, command, 'returns the published command')
    next()
  })

  context('it throws when identity.publicKey is missing', async (assert, next) => {
    const seeder = { log: { publish: sinon.stub() } }
    const identity = {}
    const service = setupStartBroadcast()
    const fail = async () => service(seeder, {}, identity)
    await assert.rejects(fail, AssertionError, 'throws an assertion error')
    assert.ok(seeder.log.publish.notCalled, 'publish isnt called')
    next()
  })
})
