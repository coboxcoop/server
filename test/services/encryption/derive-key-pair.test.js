const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const setupDeriveKeyPair = require('../../../app/services/encryption/derive-key-pair')

describe('service: encryption/derive-key-pair', (context) => {
  context('derives a key pair using get-parent-key service', (assert, next) => {
    const api = new Map()
    const getParentKey = sinon.stub().returns(crypto.masterKey())
    api.set('services.encryption.get-parent-key', getParentKey)

    const password = 'my-super-secret-password'
    const service = setupDeriveKeyPair(api)
    const deriveKeyPair = service(crypto.keyPair, password)

    assert.same(typeof deriveKeyPair, 'function', 'returns a deriveKeyPair function')

    const keyPair = deriveKeyPair(0, 'some context')

    assert.ok(getParentKey.calledOnceWith(password), 'calls getParentKey')
    assert.ok(keyPair.publicKey, 'has a public key')
    assert.ok(keyPair.secretKey, 'has a secret key')

    next()
  })
})
