const { describe } = require('../../tape')
const { tmp, cleanup } = require('../../spec-helper')
const sodium = require('sodium-native')
const crypto = require('@coboxcoop/crypto')
const path = require('path')
const { promises: fs } = require('fs')
const setupGetParentKey = require('../../../app/services/encryption/get-parent-key')

describe('service: encryption/get-parent-key', (context) => {
  context('it fetches the parent key from storage', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })

    const parentKey = crypto.masterKey()
    const password = Buffer.from('my-super-secret-password')
    const passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, password)
    const encParentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    const nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    sodium.crypto_stream_xor(encParentKey, parentKey, nonce, passwordHash)
    await fs.writeFile(path.join(storage, 'encrypted_parent_key'), encParentKey)

    const service = setupGetParentKey(api)
    const result = service(password)

    assert.same(result, parentKey, 'fetches and decrypts the parent key')

    await cleanup(storage)
    next()
  })

  context('it fails with an incorrect password', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })

    const parentKey = crypto.masterKey()
    const password = Buffer.from('my-super-secret-password')
    const passwordAttempt = Buffer.from('my-super-wrong-secret-password')
    const passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, password)
    const encParentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    const nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    sodium.crypto_stream_xor(encParentKey, parentKey, nonce, passwordHash)
    await fs.writeFile(path.join(storage, 'encrypted_parent_key'), encParentKey)

    const service = setupGetParentKey(api)
    const result = service(passwordAttempt)

    assert.notSame(result, parentKey, 'fetches and decrypts the parent key')

    await cleanup(storage)
    next()
  })
})
