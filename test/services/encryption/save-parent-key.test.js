
const { describe } = require('../../tape')
const { tmp, cleanup } = require('../../spec-helper')
const sodium = require('sodium-native')
const crypto = require('@coboxcoop/crypto')
const path = require('path')
const { promises: fs } = require('fs')
const setupSaveParentKey = require('../../../app/services/encryption/save-parent-key')

describe('service: encryption/save-parent-key', (context) => {
  context('it saves an encrypted_parent_key to storage', async (assert, next) => {
    const api = new Map()
    const storage = tmp()
    api.set('app.settings', { storage })

    const parentKey = crypto.masterKey()
    const pk = Buffer.from(parentKey)
    const password = 'my-super-secret-password'
    const service = setupSaveParentKey(api)
    await service(parentKey, password)

    const encParentKey = await fs.readFile(path.join(storage, 'encrypted_parent_key'))
    assert.ok(encParentKey && encParentKey.length, 'encrypted_parent_key is persisted to storage')
    assert.same(Buffer.alloc(32), parentKey, 'zeroes the parentKey buffer')

    let passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, Buffer.from(password))
    let decParentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    let nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    sodium.crypto_stream_xor(decParentKey, encParentKey, nonce, passwordHash)
    assert.same(decParentKey, pk, 'encrypted_parent_key is decryptable with the correct password')

    passwordHash = Buffer.alloc(sodium.crypto_stream_KEYBYTES)
    sodium.crypto_generichash(passwordHash, Buffer.from('this-is-the-wrong-password'))
    decParentKey = sodium.sodium_malloc(sodium.crypto_kdf_KEYBYTES)
    nonce = sodium.sodium_malloc(sodium.crypto_stream_NONCEBYTES)
    sodium.crypto_stream_xor(decParentKey, encParentKey, nonce, passwordHash)
    assert.notSame(decParentKey, pk, 'encrypted_parent_key fails to decrypt with the wrong password')

    await cleanup(storage)
    next()
  })
})
