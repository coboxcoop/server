const { describe } = require('../../tape')
const crypto = require('@coboxcoop/crypto')
const sinon = require('sinon')
const { hex } = require('../../../util')
const setupCreateInvite = require('../../../app/services/invites/create')

describe('service: invites/create', (context) => {
  let clock

  context.beforeEach((c) => {
    clock = sinon.useFakeTimers((new Date()).getTime())
  })

  context.afterEach((c) => {
    clock.restore()
  })

  context('generates a code and publishes an invite record to the resource', async (assert, next) => {
    const api = new Map()
    const pack = sinon.stub().returns('the-invite-code')
    const space = { log: { publish: sinon.stub() } }
    api.set('services.invites.pack', pack)

    const service = setupCreateInvite(api)
    const params = { publicKey: crypto.randomBytes(32) }
    const identity = { publicKey: crypto.randomBytes(32) }
    const result = await service(space, params, identity)

    const invite = {
      type: 'peer/invite',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey,
      content: {
        publicKey: hex(params.publicKey)
      }
    }

    assert.ok(pack.calledOnceWith(params), 'calls pack with the correct parameters')
    assert.ok(space.log.publish.calledOnce, 'calls publish on the space log')
    invite.content.code = 'the-invite-code'
    assert.same(invite, result, 'returns an invite record')

    next()
  })
})
