const { describe } = require('../../tape')
const crypto = require('@coboxcoop/crypto')
const sinon = require('sinon')
const Forbidden = require('../../../app/errors/forbidden')
const setupOpenInvite = require('../../../app/services/invites/open')

describe('service: invites/open', (context) => {
  context('returns an unpacked code', async (assert, next) => {
    const api = new Map()
    const unpack = sinon.stub().returns('the-unpacked-space-stuff')
    api.set('services.invites.unpack', unpack)

    const service = setupOpenInvite(api)
    const identity = { publicKey: crypto.randomBytes(32) }
    const code = 'this-is-the-code'
    const result = await service(code, identity)

    assert.ok(unpack.calledOnce, 'calls unpack with the correct parameters')
    assert.same(result, 'the-unpacked-space-stuff', 'returns an invite record')

    next()
  })

  context('returns an unpacked code', async (assert, next) => {
    const api = new Map()
    const unpack = sinon.stub().throws(new Error('failed to decrypt code'))
    api.set('services.invites.unpack', unpack)

    const service = setupOpenInvite(api)
    const identity = { publicKey: crypto.randomBytes(32) }
    const code = 'this-is-the-code'
    const fail = async () => service(code, identity)
    await assert.rejects(fail, Forbidden, 'throws a forbidden error')
    assert.ok(unpack.calledOnce, 'calls unpack with incorrect parameters')

    next()
  })
})
