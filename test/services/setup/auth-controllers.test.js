const { describe } = require('../../tape')
const sinon = require('sinon')
const setupAuthenicatedControllers = require('../../../app/services/setup/auth-controllers')

describe('services: setup/auth-controllers', (context) => {
  context('it calls ready on all protected controllers', async (assert, next) => {
    const api = new Map()
    const ready = sinon.stub()

    api.set('controllers.admin.seeders', { ready })
    api.set('controllers.admin.seeders.invites', { ready })
    api.set('controllers.admin.seeders.connections', { ready })
    api.set('controllers.admin.seeders.peers', { ready })
    api.set('controllers.commands.broadcast', { ready })
    api.set('controllers.commands.replicate', { ready })
    api.set('controllers.drive', { ready })
    api.set('controllers.events', { ready })
    api.set('controllers.export', { ready })
    api.set('controllers.identities', { ready })
    api.set('controllers.import', { ready })
    api.set('controllers.mounts', { ready })
    api.set('controllers.replicators', { ready })
    api.set('controllers.replicators.connections', { ready })
    api.set('controllers.spaces', { ready })
    api.set('controllers.spaces.invites', { ready })
    api.set('controllers.spaces.connections', { ready })
    api.set('controllers.spaces.peers', { ready })

    const service = setupAuthenicatedControllers(api)

    await service()

    assert.same(ready.callCount, 18, 'makes all controllers ready')
    next()
  })
})
