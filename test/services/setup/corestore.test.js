const { describe } = require('../../tape')
const crypto = require('@coboxcoop/crypto')
const { tmp, cleanup } = require('../../spec-helper')
const setupCorestore = require('../../../app/services/setup/corestore')

describe('services: setup/corestore', (context) => {
  context('it initializes a corestore and networker instance', async (assert, next) => {
    const storage = tmp()
    const api = new Map()
    api.set('app.settings', { storage })

    const service = setupCorestore(api)
    const parentKey = crypto.masterKey()
    await service(parentKey)

    const corestore = api.get('dat.store')
    const networker = api.get('dat.networker')

    assert.ok(corestore, 'sets dat.store')
    assert.ok(networker, 'sets dat.networker')
    assert.same(corestore.constructor.name, 'Corestore', 'is a corestore instance')
    assert.same(corestore.inner._masterKey, parentKey, 'uses the parent key')
    assert.ok(networker.constructor.name, 'CoBoxNetworker', 'is a cobox networker instance')

    await cleanup(storage)
    next()
  })
})
