const { describe } = require('../../tape')
const sinon = require('sinon')
const AsyncMap = require('../../../lib/cache')
const setupEvents = require('../../../app/services/setup/events')
const { sleep } = require('../../spec-helper')

describe('services: event-store', (context) => {
  let api
  let service
  let eventStream
  let spaces
  let seeders

  context.beforeEach(async (c) => {
    api = new Map()
    eventStream = { add: sinon.stub() }
    service = setupEvents(api)
    api.set('setup.events', service)
    api.set('app.events', eventStream)
    spaces = new AsyncMap()
    seeders = new AsyncMap()
    api.set('cache.spaces', spaces)
    api.set('cache.seeders', seeders)
  })

  context('it adds new sources to the event stream sink', async (assert, next) => {
    const space = { createLogStream: sinon.stub() }
    const seeder = { createLogStream: sinon.stub() }

    service()

    spaces.set(1, space)
    seeders.set(1, seeder)

    await sleep(100)

    assert.ok(eventStream.add.calledTwice, 'adds two streams to the store')
    assert.ok(space.createLogStream.calledOnce, 'calls createLogStream on space')
    assert.ok(seeder.createLogStream.calledOnce, 'calls createLogStream on seeder')
    next()
  })
})
