const { describe } = require('../../tape')
const crypto = require('@coboxcoop/crypto')
const memdb = require('level-mem')
const sinon = require('sinon')
const { hex, shortHex } = require('../../../util')
const setupIdentities = require('../../../app/services/setup/identities')

describe('services: setup/identities', (context) => {
  context('it loads identity references from leveldb and passes to the identity factory', async (assert, next) => {
    const api = new Map()
    const cache = new Map()
    const factory = { createIdentity: sinon.stub(), ready: sinon.stub() }
    const identitydb = memdb({ keyEncoding: 'utf-8', valueEncoding: 'json' })

    api.set('db.identities', identitydb)
    api.set('factory.identities', factory)
    api.set('cache.identities', cache)

    const id1 = {
      publicKey: hex(crypto.randomBytes(32)),
      secretKey: hex(crypto.randomBytes(64))
    }

    const id2 = {
      publicKey: hex(crypto.randomBytes(32)),
      secretKey: hex(crypto.randomBytes(64))
    }

    // add some to the cache, so we don't create a new one
    cache.set(shortHex(id1.publicKey), id1)
    cache.set(shortHex(id2.publicKey), id2)

    const service = setupIdentities(api)

    await identitydb.batch()
      .put(shortHex(id1.publicKey), id1)
      .put(shortHex(id2.publicKey), id2)
      .write()

    await service()

    assert.ok(factory.ready.calledOnce, 'ready called on factory')
    console.log(factory.createIdentity.callCount)
    assert.ok(factory.createIdentity.calledTwice, 'factory called for each identity reference')
    next()
  })

  context('it creates an identity if one didnt exist', async (assert, next) => {
    const api = new Map()
    const identities = new Map()
    const factory = { createIdentity: sinon.stub(), ready: sinon.stub() }
    const identitydb = memdb()
    api.set('db.identities', identitydb)
    api.set('factory.identities', factory)
    api.set('cache.identities', identities)

    const service = setupIdentities(api)
    await service()

    assert.ok(factory.ready.calledOnce, 'ready called on factory')
    assert.ok(factory.createIdentity.calledOnce, 'factory called to create new identity')
    next()
  })
})
