const { describe } = require('../../tape')
const crypto = require('hypercore-crypto')
const memdb = require('level-mem')
const sinon = require('sinon')
const { shortHex } = require('../../../util')
const setupReplicators = require('../../../app/services/setup/replicators')

describe('services: setup/replicators', (context) => {
  context('it loads replicator references from leveldb and passes to the replicator factory', async (assert, next) => {
    const api = new Map()
    const identities = new Map()
    const keyPair = crypto.keyPair()
    const factory = { createReplicator: sinon.stub(), ready: sinon.stub() }
    const replicators = memdb()
    const shortPublicKey = shortHex(keyPair.publicKey)
    identities.set(shortPublicKey, keyPair)
    api.set('db.replicators', replicators)
    api.set('factory.replicators', factory)
    api.set('cache.identities', identities)

    const service = setupReplicators(api)

    const batch = replicators.batch()
    const replicator1 = { address: crypto.randomBytes(32) }
    const replicator2 = { address: crypto.randomBytes(32) }

    await batch
      .put([shortPublicKey, shortHex(replicator1.address)].join('/'), replicator1)
      .put([shortPublicKey, shortHex(replicator2.address)].join('/'), replicator2)
      .write()

    await service()

    assert.ok(factory.ready.calledOnce, 'ready called on factory')
    assert.ok(factory.createReplicator.calledTwice, 'factory called for each replicator reference')
    next()
  })
})
