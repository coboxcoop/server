const { describe } = require('../../tape')
const crypto = require('hypercore-crypto')
const memdb = require('level-mem')
const sinon = require('sinon')
const { shortHex } = require('../../../util')
const setupSeeders = require('../../../app/services/setup/seeders')

describe('services: setup/seeders', (context) => {
  context('it loads seeder references from leveldb and passes to the seeder factory', async (assert, next) => {
    const api = new Map()
    const identities = new Map()
    const keyPair = crypto.keyPair()
    const factory = { createSeeder: sinon.stub(), ready: sinon.stub() }
    const seeders = memdb()
    const shortPublicKey = shortHex(keyPair.publicKey)
    identities.set(shortPublicKey, keyPair)
    api.set('db.seeders', seeders)
    api.set('factory.seeders', factory)
    api.set('cache.identities', identities)

    const service = setupSeeders(api)

    const batch = seeders.batch()
    const seeder1 = { address: crypto.randomBytes(32) }
    const seeder2 = { address: crypto.randomBytes(32) }

    await batch
      .put([shortPublicKey, shortHex(seeder1.address)].join('/'), seeder1)
      .put([shortPublicKey, shortHex(seeder2.address)].join('/'), seeder2)
      .write()

    await service()

    assert.ok(factory.ready.calledOnce, 'ready called on factory')
    assert.ok(factory.createSeeder.calledTwice, 'factory called for each seeder reference')
    next()
  })
})
