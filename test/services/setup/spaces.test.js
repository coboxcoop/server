const { describe } = require('../../tape')
const crypto = require('hypercore-crypto')
const memdb = require('level-mem')
const sinon = require('sinon')
const { shortHex } = require('../../../util')
const setupSpaces = require('../../../app/services/setup/spaces')

describe('services: setup/spaces', (context) => {
  context('it loads space references from leveldb and passes to the space factory', async (assert, next) => {
    const api = new Map()
    const identities = new Map()
    const keyPair = crypto.keyPair()
    const factory = { createSpace: sinon.stub(), ready: sinon.stub() }
    const spaces = memdb()
    const shortPublicKey = shortHex(keyPair.publicKey)
    identities.set(shortPublicKey, keyPair)
    api.set('db.spaces', spaces)
    api.set('factory.spaces', factory)
    api.set('cache.identities', identities)

    const service = setupSpaces(api)

    const batch = spaces.batch()
    const space1 = { address: crypto.randomBytes(32) }
    const space2 = { address: crypto.randomBytes(32) }

    await batch
      .put([shortPublicKey, shortHex(space1.address)].join('/'), space1)
      .put([shortPublicKey, shortHex(space2.address)].join('/'), space2)
      .write()

    await service()

    assert.ok(factory.ready.calledOnce, 'ready called on factory')
    assert.ok(factory.createSpace.calledTwice, 'factory called for each space reference')
    next()
  })
})
