const { describe } = require('../../tape')
const sinon = require('sinon')
const setupUnauthenicatedControllers = require('../../../app/services/setup/unauth-controllers')

describe('services: setup/unauth-controllers', (context) => {
  context('it calls ready on all unprotected controllers', async (assert, next) => {
    const api = new Map()
    const ready = sinon.stub()

    api.set('controllers.system', { ready })
    api.set('controllers.registration', { ready })
    api.set('controllers.auth', { ready })

    const service = setupUnauthenicatedControllers(api)

    await service()

    assert.same(ready.callCount, 3, 'makes all controllers ready')
    next()
  })
})
