const { describe } = require('../../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const teardownCorestore = require('../../../app/services/teardown/corestore')
const { hex } = require('../../../util')

describe('services: teardown/corestore', (context) => {
  context('it removes the existing corestore and networker instance', async (assert, next) => {
    const api = new Map()
    const corestore = { close: sinon.stub(), inner: { _masterKey: crypto.randomBytes(32) } }
    const networker = { close: sinon.stub() }
    api.set('dat.store', corestore)
    api.set('dat.networker', networker)

    const service = teardownCorestore(api)
    await service()

    assert.ok(corestore.close.calledOnce, 'calls close on corestore')
    assert.ok(networker.close.calledOnce, 'calls close on networker')
    assert.same(hex(corestore.inner._masterKey), hex(Buffer.alloc(32)), `it zeros corestore's master key`)
    assert.notok(api.get('dat.store'), 'unsets dat.store')
    assert.notok(api.get('dat.networker'), 'unsets dat.networker')
    next()
  })
})
