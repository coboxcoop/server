const { describe } = require('../../tape')
const { tmp, cleanup } = require('../../spec-helper')
const { promises: fs } = require('fs')
const setupTeardownMount = require('../../../app/services/teardown/mount')

describe('services: teardown/mount', (context) => {
  context('removes the mount directory', async (assert, next) => {
    const api = new Map()
    const mount = tmp()
    api.set('app.settings', { mount })

    const service = setupTeardownMount(api)
    await service()
    const exists = async () => fs.access(mount)
    assert.rejects(exists, 'mount directory is removed')

    await cleanup(mount)
    next()
  })
})
