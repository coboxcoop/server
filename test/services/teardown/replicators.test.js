const { describe } = require('../../tape')
const sinon = require('sinon')
const setupTeardownReplicators = require('../../../app/services/teardown/replicators')

describe('services: teardown/replicators', (context) => {
  context('closes all replicators, clears cache and nulls factory', async (assert, next) => {
    const api = new Map()
    const replicators = new Map()
    api.set('cache.replicators', replicators)
    api.set('factory.replicators', 'testing')
    const repl1 = { close: sinon.stub() }
    const repl2 = { close: sinon.stub() }
    replicators.set(1, repl1)
    replicators.set(2, repl2)

    const service = setupTeardownReplicators(api)
    await service()

    assert.notok(api.get('factory.replicators'), 'nullifies factory.replicators')
    assert.notok(replicators.size, 'clears the replicators cache')
    assert.ok(repl1.close.calledOnce, 'calls close on repl1')
    assert.ok(repl2.close.calledOnce, 'calls close on repl2')

    next()
  })
})
