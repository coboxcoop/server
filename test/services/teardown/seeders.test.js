const { describe } = require('../../tape')
const sinon = require('sinon')
const setupTeardownSeeders = require('../../../app/services/teardown/seeders')

describe('services: teardown/seeders', (context) => {
  context('closes all seeders, clears cache and nulls factory', async (assert, next) => {
    const api = new Map()
    const seeders = new Map()
    api.set('cache.seeders', seeders)
    api.set('factory.seeders', 'testing')
    const seeder1 = { close: sinon.stub() }
    const seeder2 = { close: sinon.stub() }
    seeders.set(1, seeder1)
    seeders.set(2, seeder2)

    const service = setupTeardownSeeders(api)
    await service()

    assert.notok(api.get('factory.seeders'), 'nullifies factory.seeders')
    assert.notok(seeders.size, 'clears the seeders cache')
    assert.ok(seeder1.close.calledOnce, 'calls close on seeder1')
    assert.ok(seeder2.close.calledOnce, 'calls close on seeder2')

    next()
  })
})
