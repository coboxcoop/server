const { describe } = require('../../tape')
const sinon = require('sinon')
const setupTeardownSpaces = require('../../../app/services/teardown/spaces')

describe('services: teardown/spaces', (context) => {
  context('closes all spaces, clears cache and nulls factory', async (assert, next) => {
    const api = new Map()
    const spaces = new Map()
    api.set('cache.spaces', spaces)
    api.set('factory.spaces', 'testing')
    const space1 = { close: sinon.stub() }
    const space2 = { close: sinon.stub() }
    spaces.set(1, space1)
    spaces.set(2, space2)

    const service = setupTeardownSpaces(api)
    await service()

    assert.notok(api.get('factory.spaces'), 'nullifies factory.spaces')
    assert.notok(spaces.size, 'clears the spaces cache')
    assert.ok(space1.close.calledOnce, 'calls close on space1')
    assert.ok(space2.close.calledOnce, 'calls close on space2')

    next()
  })
})
