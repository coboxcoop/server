const { describe } = require('../tape')
const sinon = require('sinon')
const crypto = require('@coboxcoop/crypto')
const setupUnlock = require('../../app/services/unlock')
const { tmp, cleanup } = require('../spec-helper')
const { hex } = require('../../util')
const { promises: fs } = require('fs')

describe('services: unlock', (context) => {
  context('it unlocks encrypted resources', async (assert, next) => {
    const api = new Map()
    const app = new Map()
    const mount = tmp()
    const getParentKey = sinon.stub().returns(crypto.masterKey())
    const getKeyPairDeriver = sinon.stub().returns(crypto.masterKey())
    const setupCorestore = sinon.stub()
    const identity = {
      publicKey: hex(crypto.randomBytes(32)),
      secretKey: hex(crypto.randomBytes(64))
    }
    const loadIdentities = sinon.stub().resolves(identity)
    const loadSpaces = sinon.stub()
    const loadReplicators = sinon.stub()
    const loadSeeders = sinon.stub()
    const setupAuthenticatedControllers = sinon.stub()

    api.set('app.express', app)
    api.set('app.settings', { mount })
    api.set('services.encryption.get-parent-key', getParentKey)
    api.set('services.encryption.derive-key-pair', getKeyPairDeriver)
    api.set('setup.corestore', setupCorestore)
    api.set('setup.identities', loadIdentities)
    api.set('setup.spaces', loadSpaces)
    api.set('setup.replicators', loadReplicators)
    api.set('setup.seeders', loadSeeders)
    api.set('setup.auth-controllers', setupAuthenticatedControllers)

    const password = 'my-super-secret-password'
    const unlock = setupUnlock(api)
    const id = await unlock(password)

    assert.ok(await fs.readdir(mount), 'mount directory is created')
    assert.ok(getParentKey.calledOnceWith(password), 'calls getParentKey once with password')
    assert.ok(setupCorestore.calledOnce, 'calls setupCorestore once')
    assert.ok(getKeyPairDeriver.calledOnceWith(crypto.keyPair, password), 'calls getKeyPairDeriver once with correct args')
    assert.ok(loadIdentities.calledOnce, 'calls loadIdentities once')
    assert.ok(loadSpaces.calledOnce, 'calls loadSpaces once')
    assert.ok(loadReplicators.calledOnce, 'calls loadReplicators once')
    assert.ok(loadSeeders.calledOnce, 'calls loadSeeders once')
    assert.ok(setupAuthenticatedControllers.calledOnce, 'calls setupAuthenticatedControllers once')

    assert.notok(app.get('lock'), 'boolean lock is off')

    assert.same(identity, id, 'returns the current identity')
    await cleanup([mount])
    next()
  })
})
