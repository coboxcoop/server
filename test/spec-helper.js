const rimraf = require('rimraf')
const { dirSync: tmpdir } = require('tmp')
const mkdirp = require('mkdirp')
const duplexify = require('duplexify')
const crypto = require('crypto')
const LiveStream = require('level-live-stream')
const { Readable } = require('stream')
const memdb = require('level-mem')
const randomWords = require('random-words')
const sinon = require('sinon')
const { hex, shortHex } = require('../util')

/**
 * supertest registration helper function
 * @param {...} request
 * @param {string} password - the encryption password
 */
async function register (request, payload) {
  return request
    .post('/api/register')
    .send(payload)
}

/**
 * supertest login helper function
 * @param {...} request
 * @param {string} password - the encryption password
 */
async function login (request, payload) {
  return request
    .post('/api/auth')
    .send(payload)
}

/**
 * supertest logout helper function
 * @param {...} request
 */
async function logout (request) {
  return request.delete('/api/auth')
}

/*
 * promisified setTimeout
 * @param {number} ms - number of milliseconds sleep
 */
function sleep (ms) {
  return new Promise((resolve, reject) => setTimeout(resolve, ms))
}

/*
 * a mock that returns a readable stream
 * @param {Array} data - a list of messages to stream
 * @param {Object} opts - readable stream opts
 */
function mockReadStream (data, opts = {}) {
  const readable = new Readable({ objectMode: true, ...opts })
  readable.finish = readable.end = readable.close
  for (const item of data) {
    readable.push(item)
  }
  readable.push(null)
  return sinon.stub().returns(readable)
}

/**
 * seed some resource entries into the cache
 * @param {Map} cache
 * @param {buffer|string} publicKey
 */
function setResources (cache, publicKey, stubs) {
  const integer = Math.ceil(Math.random() * 10)
  for (let i = 0; i < integer; ++i) {
    const entry = {
      ...stubs,
      address: hex(crypto.randomBytes(32)),
      name: randomWords(1).pop()
    }
    const reference = getReference(publicKey, entry.address)
    cache.set(reference, entry)
  }
}

/**
 * return a cache reference
 * @returns {string}
 */
function getReference (pubKey, address) {
  return [shortHex(pubKey), shortHex(address)].join('/')
}

/**
 * create a mocked seeder store
 * @returns {Level}
 */
function mockSeederStore () {
  const db = memdb()
  LiveStream.install(db)

  return {
    source: duplexify(),
    store: db,
    stream: db.createLiveStream()
  }
}

function cleanup (dirs) {
  if (!Array.isArray(dirs)) dirs = [dirs]

  return new Promise((resolve, reject) => {
    var pending = 1

    function next (n) {
      var dir = dirs[n]
      if (!dir) return done()
      ++pending
      process.nextTick(next, n + 1)

      rimraf(dir, (err) => {
        if (err) return done(err)
        done()
      })
    }

    function done (err) {
      if (err) {
        pending = Infinity
        return reject(err)
      }
      if (!--pending) return resolve()
    }

    next(0)
  })
}

function tmp () {
  var path = tmpdir().name
  mkdirp.sync(path)
  return path
}

module.exports = {
  register,
  login,
  logout,
  sleep,
  getReference,
  setResources,
  mockSeederStore,
  mockReadStream,
  cleanup,
  tmp
}
