const _tape = require('tape')
const _proms = require('tape-promise').default(_tape)
const _plus = require('tape-plus/wrap')(_proms)
const tape = _plus

module.exports = tape
