const os = require('os')
const path = require('path')

/**
 * randomly selects one of the four colorizers
 * @param {string} text
 */
function colourize (code) {
  code = code || Math.floor(Math.random() * 255)
  return function (text) {
    return ['\x1b[38;5;', code, 'm', text, '\x1b[0m'].join('')
  }
}

/**
 * replicates '~' with the path of home directory
 * @param {string} str
 * @returns {string}
 */
function untilde (str) {
  if (str[0] === '~') return path.join(os.homedir(), str.slice(1))
  return str
}

/**
 * transform a buffer into a hex string
 * @param {Buffer} buf
 * @returns {string}
 */
function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

/**
 * transform a buffer or string into a shortened hex string
 * @param {buffer} buf
 * @returns {string}
 */
function shortHex (buf) {
  return hex(buf).substr(0, 8)
}

/**
 * transform all values in an object recursively into hex strings
 * @param {Buffer} buf
 * @returns {string}
 */
function hexify (obj) {
  return Object.keys(obj).reduce((acc, key) => {
    !Buffer.isBuffer(obj[key]) && obj[key] != null
      ? acc[key] = hexify(obj[key])
      : acc[key] = hex(obj[key])
    return acc
  }, {})
}
/**
 * check truthy accepting true bool or 'true' string
 * @param {string|boolean} t
 * @returns {boolean}
 */
function isTruthy (t) {
  return t && (t === 'true' || t === true)
}

/**
 * check if object is empty
 * @param {object} obj
 * @returns {boolean}
 */
function isEmpty (obj) {
  return Object.entries(obj).length === 0
}

/**
 * check if variable is a string
 * @param {object} obj
 * @returns {boolean}
 */
function isString (str) {
  return str && typeof str === 'string'
}

/**
 * check if variable is a valid hex string
 * @param {object} obj
 * @returns {boolean}
 */
function isHexString (str, length) {
  if (!isString(str)) return false
  if (length && (str.length !== length)) return false
  return RegExp('[0-9a-fA-F]+').test(str)
}

/**
 * recursively remove undefined keys from an object
 * @param {Object} obj - the object to trim
 * @returns {Array}
 */
function trim (obj) {
  return Object
    .keys(obj)
    .filter(key => obj[key] != null)
    .reduce((acc, key) => {
      return !Array.isArray(obj[key]) && typeof obj[key] === 'object'
        ? { ...acc, [key]: trim(obj[key]) }
        : { ...acc, [key]: obj[key] }
    }, {})
}

/**
 * flatten an object into a list, each key is followed by its value
 * prepends keys with '--' for command line parsability
 * @param {Object} obj - the object to flatten
 * @returns {Array}
 */
function flatten (obj) {
  return Object
    .keys(obj)
    .reduce((acc, key) => {
      acc.push(argKey(key))
      if (obj[key] !== true) acc.push(obj[key])
      return acc
    }, [])

  function argKey (key) {
    return '--' + key.replace(
      /[\w]([A-Z])/g,
      m => m[0] + '-' + m[1]
    ).toLowerCase()
  }
}

/**
 * select the specified list of keys from an object
 * @param {Object} obj - the object to pick from
 * @param {Array} keys - the keys to pick
 * @returns {Object}
 */
function pick (obj, keys) {
  const result = {}
  for (const key of keys) {
    if (obj[key] !== undefined) result[key] = obj[key]
  }
  return result
}

/**
 * omit the specified list of keys from an object
 * @param {Object} obj - the object to omit from
 * @param {Array} keys - the keys to omit
 * @returns {Object}
 */
function omit (obj, keys) {
  const result = {}
  for (const key of Object.keys(obj)) {
    if (keys.includes(key)) continue
    result[key] = obj[key]
  }
  return result
}

/*
 * check if environment is production
 * @returns {boolean}
 */
function isProduction () {
  return process.env.NODE_ENV === 'production'
}

/*
 * check if environment is development
 * @returns {boolean}
 */
function isDevelopment () {
  return process.env.NODE_ENV === 'development'
}

module.exports = {
  isDevelopment,
  isProduction,
  colourize,
  untilde,
  hex,
  shortHex,
  hexify,
  isHexString,
  isString,
  isTruthy,
  isEmpty,
  trim,
  flatten,
  pick,
  omit
}
